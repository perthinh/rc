using Core.Timing;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace Core.Web.Kendo
{
    public static class DatePickerExtensions
    {
        public static DatePickerBuilderBase<DatePicker, DatePickerBuilder> FutureDate(this DatePickerBuilderBase<DatePicker, DatePickerBuilder> builder, bool futureDate)
        {
            return futureDate ? builder.Min(Clock.Now.AddDays(1)) : builder;
        }

        public static DatePickerBuilderBase<DateTimePicker, DateTimePickerBuilder> FutureDate(this DatePickerBuilderBase<DateTimePicker, DateTimePickerBuilder> builder, bool futureDate)
        {
            return futureDate ? builder.Min(Clock.Now.AddDays(1)) : builder;
        }

        public static DatePickerBuilderBase<DatePicker, DatePickerBuilder> FormatDate(this DatePickerBuilderBase<DatePicker, DatePickerBuilder> builder)
        {
            return builder.Format("{0:yyyy/MM/dd}");
        }

        public static DatePickerBuilderBase<DateTimePicker, DateTimePickerBuilder> FormatDateTime(this DatePickerBuilderBase<DateTimePicker, DateTimePickerBuilder> builder)
        {
            return builder.Format("{0:yyyy/MM/dd HH:mm:ss}");
        }


    }
}