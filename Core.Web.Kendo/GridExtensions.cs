using Kendo.Mvc.UI.Fluent;

namespace Core.Web.Kendo
{
    public static class GridExtensions
    {
        public static DataSourceEventBuilder GridError(this DataSourceEventBuilder builder, string gridId = "grid")
        {
            var function = string.Format("function(args){{KendoHelper.gridError(args,'{0}');}}", gridId);
            return builder.Error(function);
        }

        public static DataSourceEventBuilder GridRequestEnd(this DataSourceEventBuilder builder)
        {
            return builder.RequestEnd("KendoHelper.gridRequestEnd");
        }

        public static GridBoundColumnBuilder<T> FormatDate<T>(this GridBoundColumnBuilder<T> builder) where T : class
        {
            return builder.Format("{0:yyyy-MM-dd}");
        }

        public static GridBoundColumnBuilder<T> FormatDateTime<T>(this GridBoundColumnBuilder<T> builder) where T : class
        {
            return builder.Format("{0:yyyy-MM-dd HH:mm:ss}");
        }

        public static GridBuilder<T> DefaultSettings<T>(this GridBuilder<T> builder, string name = "grid") where T : class
        {
            return builder
                .Name(name)
                .Pageable(x => x.PageSizes(new[] { 5, 15, 25, 50, 100 }).Refresh(true))
                .Filterable();
        }

        public static AjaxDataSourceBuilder<T> DefaultSettings<T>(this AjaxDataSourceBuilder<T> builder, bool withSync = false) where T : class
        {
            return builder
                .PageSize(15)
                .Events(e =>
                {
                    e.GridRequestEnd().GridError();
                    if (withSync)
                    {
                        e.Sync("kendoGrid.sync");
                    }
                });
        }

        public static AjaxDataSourceBuilder<T> DefaultSettingsWithSync<T>(this AjaxDataSourceBuilder<T> builder) where T : class
        {
            return builder.DefaultSettings(withSync: true);
        }
    }
}