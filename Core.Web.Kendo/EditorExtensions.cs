﻿using Kendo.Mvc.UI.Fluent;

namespace Core.Web.Kendo
{
    public static class EditorExtensions
    {
        public static EditorToolFactory AllTools(this EditorToolFactory tools)
        {
            return tools.Clear()
                          .Bold().Italic().Underline().Strikethrough()
                          .JustifyLeft().JustifyCenter().JustifyRight().JustifyFull()
                          .InsertUnorderedList().InsertOrderedList()
                          .Outdent().Indent()
                          .CreateLink().Unlink()
                          .InsertFile().InsertImage()
                          .SubScript()
                          .SuperScript()
                          .TableEditing()
                          .Formatting()
                          .CleanFormatting()
                          .FontName()
                          .FontSize()
                          .FontColor().BackColor()
                          .Print();
        }
    }
}
