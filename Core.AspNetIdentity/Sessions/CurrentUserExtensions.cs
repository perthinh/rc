﻿namespace Core.AspNetIdentity.Sessions
{
    public static class CurrentUserExtensions
    {
        public static string FullName(this ICurrentUser current)
        {
            return current?.Info == null ? "Anonymous" : current.Info.FullName;
        }
    }
}