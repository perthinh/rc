﻿using System.Collections.Generic;
using System.Security.Principal;
using Core.AspNetIdentity.Entities;

namespace Core.AspNetIdentity.Sessions
{
    public interface ICurrentUser
    {
        User Info { get; }
        IList<string> Roles { get; }
        bool IsInRole(params ERole[] roles);
        bool IsAuthenticated { get; }
        IIdentity Identity { get; set; }
        void Refresh();
    }
}