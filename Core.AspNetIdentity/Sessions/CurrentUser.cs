using System.Web;
using Core.AspNetIdentity.Managers;
using Core.Web.Caching;

namespace Core.AspNetIdentity.Sessions
{
    public class CurrentUser : UserBase, ICurrentUser
    {
        private readonly ISessionCache _sessionCache;
        private const string CurrentUserKey = "__CurrentUser__";

        public CurrentUser(UserManager userManager, ISessionCache sessionCache)
            : base(userManager)
        {
            _sessionCache = sessionCache;
            Identity = HttpContext.Current?.User?.Identity;
        }

        public void Refresh()
        {
            _sessionCache.Remove(CurrentUserKey);
        }

        protected override LoggedInUser LoggedInUser
        {
            get
            {
                return _sessionCache.GetOrAdd(CurrentUserKey, () => CreateLoggedInUser());
            }
            set
            {
                _sessionCache.Set(CurrentUserKey, value);
            }
        }
    }
}