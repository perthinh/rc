using System.Collections.Generic;
using Core.AspNetIdentity.Entities;

namespace Core.AspNetIdentity.Sessions
{
    public class LoggedInUser
    {
        public User Info { get; set; }
        public IList<string> Roles { get; set; }

        public LoggedInUser()
        {
            Roles = new List<string>();
        }
    }
}