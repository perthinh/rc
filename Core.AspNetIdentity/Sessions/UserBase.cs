using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Core.AspNetIdentity.Entities;
using Core.AspNetIdentity.Managers;
using Microsoft.AspNet.Identity;

namespace Core.AspNetIdentity.Sessions
{
    public abstract class UserBase
    {
        protected UserManager UserManager;

        public IIdentity Identity { get; set; }

        public User Info => LoggedInUser.Info;

        public IList<string> Roles => LoggedInUser.Roles;

        public bool IsAuthenticated => Identity.IsAuthenticated;

        protected abstract LoggedInUser LoggedInUser { get; set; }

        protected UserBase(UserManager userManager)
        {
            UserManager = userManager;
        }

        public bool IsInRole(params ERole[] roles)
        {
            return roles.Any(role => LoggedInUser.Roles.Contains(role.ToString()));
        }

        protected LoggedInUser CreateLoggedInUser(string userName = null)
        {
            var user = UserManager.FindByName(userName ?? Identity.Name);

            if (user != null)
            {
                var roles = UserManager.GetRoles(user.Id);
                return new LoggedInUser
                {
                    Info = user,
                    Roles = roles
                };
            }
            return new LoggedInUser();
        }
    }
}