﻿using System.Security.Claims;
using System.Threading.Tasks;
using Core.AspNetIdentity.Managers;
using Core.DataAccess.Identity;
using Microsoft.AspNet.Identity;

namespace Core.AspNetIdentity.Entities
{
    public class User : AspNetUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{LastName} {FirstName}";

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager manager, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}