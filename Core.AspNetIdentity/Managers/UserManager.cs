﻿using Core.AspNetIdentity.Entities;
using Microsoft.AspNet.Identity;

namespace Core.AspNetIdentity.Managers
{
    public class UserManager : UserManager<User, int>
    {
        public UserManager(IUserStore<User, int> store)
            : base(store)
        {
        }


    }
}