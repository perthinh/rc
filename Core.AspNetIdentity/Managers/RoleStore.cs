using Core.AspNetIdentity.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace Core.AspNetIdentity.Managers
{
    public class RoleStore : AspNetRoleStore<Role>
    {
        public RoleStore(IDataContext context)
            : base(context)
        {
        }
    }
}