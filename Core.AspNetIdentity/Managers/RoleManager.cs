﻿using Core.AspNetIdentity.Entities;
using Microsoft.AspNet.Identity;

namespace Core.AspNetIdentity.Managers
{
    public class RoleManager : RoleManager<Role, int>
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }

        public void CreateIfNotExists(string roleName)
        {
            if (!this.RoleExists(roleName))
            {
                this.Create(new Role
                {
                    Name = roleName
                });
            }
        }
    }
}