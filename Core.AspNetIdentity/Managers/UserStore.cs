using Core.AspNetIdentity.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace Core.AspNetIdentity.Managers
{
    public class UserStore : AspNetUserStore<User, Role>
    {
        public UserStore(IDataContext context)
            : base(context)
        {
        }
    }
}