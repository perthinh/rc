﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace ResourceCenter.WebCore.Extensions
{
    public static class Objects
    {
        public static string ToUniqueFileName(this HttpPostedFileBase file)
        {
            return Guid.NewGuid().ToString("N") + Path.GetExtension(file.FileName);
        }

        public static string ToAbsolutePath(this string path)
        {
            return HostingEnvironment.MapPath(path);
        }

        public static string Translate<T>(this IEnumerable<T> list, Func<T, string> func)
        {
            var first = list.FirstOrDefault();
            return (first == null ? "[N/A]" : func(first)) ?? string.Empty;
        }
    }
}
