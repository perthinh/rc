﻿using System;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Extensions
{
    public static class FormExtensions
    {
        public static int MaxLength(this EFieldDataType dataType)
        {
            switch (dataType)
            {
                case EFieldDataType.Text:
                    return 200;
                case EFieldDataType.MultilineText:
                    return 400;
                case EFieldDataType.DropDownList:
                    return 100;
                case EFieldDataType.CheckBox:
                    return 5;
                default:
                    return 50;
            }
        }
    }
}
