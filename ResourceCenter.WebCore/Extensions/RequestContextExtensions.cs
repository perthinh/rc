﻿using System.Linq;
using System.Web;
using System.Web.Routing;
using Core.Extensions;

namespace ResourceCenter.WebCore.Extensions
{
    public static class RequestContextExtensions
    {
        public static string UserLanguage(this HttpRequestBase request)
        {
            return request.UserLanguages?.FirstOrDefault() ?? "en";
        }

        public static int? CategoryId(this RequestContext context)
        {
            return context.HttpContext.Request["CategoryId"].ChangeTypeTo<int?>();
        }

        public static int AssetId(this RequestContext context)
        {
            return context.HttpContext.Request["AssetId"].ChangeTypeTo<int>();
        }

        public static object TenantId(this RequestContext context)
        {
            return context.RouteData.Values["TenantId"];
        }

        public static object ResellerId(this RequestContext context)
        {
            return context.RouteData.Values["ResellerId"];
        }

        public static object LanguageCode(this RequestContext context)
        {
            return context.RouteData.Values["Lang"];
        }

        public static RouteValueDictionary ToAreaRouteValues(this RequestContext context)
        {
            return new RouteValueDictionary(new
            {
                TenantId = context.TenantId(),
                ResellerId = context.ResellerId(),
                Lang = context.LanguageCode()
            });
        }

        public static RouteValueDictionary ToCategoryRouteValues(this RequestContext context, int? categoryId)
        {
            return context.ToAreaRouteValues()
                           .Set("CategoryId", categoryId);
        }

        public static RouteValueDictionary ToPagedAssetsRouteValues(this RequestContext context, int page)
        {
            return context.ToCategoryRouteValues(context.CategoryId())
                            .Set("Page", page);
        }

        public static RouteValueDictionary ToAssetDetailsRouteValues(this RequestContext context, int assetId)
        {
            return context.ToAreaRouteValues()
                            .Set("AssetId", assetId);
        }

        public static RouteValueDictionary Set(this RouteValueDictionary dictionary, string key, object value)
        {
            if (value != null)
            {
                dictionary[key] = value;
            }
            return dictionary;
        }
    }
}