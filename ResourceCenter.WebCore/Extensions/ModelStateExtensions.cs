﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ResourceCenter.WebCore.Extensions
{
    public static class ModelStateExtensions
    {
        public static void AddIdentityResultErrors(this ModelStateDictionary modelState, IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                modelState.AddModelError("", error);
            }
        }
    }
}
