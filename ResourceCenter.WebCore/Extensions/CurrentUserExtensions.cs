﻿using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.AspNetIdentity;

namespace ResourceCenter.WebCore.Extensions
{
    public static class CurrentUserExtensions
    {
        public static bool IsHost(this ICurrentUser user)
        {
            return user.IsInRole(ERole.Host);
        }

        public static bool IsReseller(this ICurrentUser user)
        {
            return user.IsInRole(ERole.Reseller);
        }

        public static bool IsDistributor(this ICurrentUser user)
        {
            return user.IsInRole(ERole.Distributor, ERole.User);
        }
    }
}
