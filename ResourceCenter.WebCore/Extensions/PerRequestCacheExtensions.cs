﻿using Core.Web.Caching;

namespace ResourceCenter.WebCore.Extensions
{
    public static class PerRequestCacheExtensions
    {
        private const string KeyRequestPartnerTypeId = "__RequestPartnerTypeId";

        public static int RequestPartnerTypeId(this IPerRequestCache cache)
        {
            return cache.Get<int>(KeyRequestPartnerTypeId);
        }

        public static void SetRequestPartnerTypeId(this IPerRequestCache cache, int id)
        {
            cache.Set(KeyRequestPartnerTypeId, id);
        }
    }
}
