﻿using Core.Configuration;

namespace ResourceCenter.WebCore.Extensions
{
    public static class SettingManagerExtensions
    {
        public static string WebDomain(this ISettingManager manager)
        {
            return manager.GetNotEmptyValue("Web.Domain");
        }

        public static string ConfirmEmailUrl(this ISettingManager manager)
        {
            return string.Format("{0}{1}", manager.WebDomain(), manager.GetNotEmptyValue("Web.ResetPasswordUrl"));
        }
    }
}