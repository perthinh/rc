﻿using System;
using System.Threading.Tasks;
using System.Web;
using Core.AutoMapper;
using Core.Configuration.Startup;
using Core.DataAccess.Uow;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.AspNetIdentity.Extensions;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Account.Models;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Account.Services
{
    public interface IAccountService
    {
        Task<SignInStatus> Login(LoginModel model);
        Task<IdentityResult> Register(CreateDistributorModel model, string languageCode);
        Task<IdentityResult> RegisterReseller(CreateResellerModel model, string languageCode);
        Task<IdentityResult> ConfirmEmail(int userId, string code);
        Task<User> ForgotPassword(ForgotPasswordModel model);
        Task<IdentityResult> ResetPassword(ResetPasswordModel model);
    }

    public class AccountService : IAccountService
    {
        private readonly SignInManager _signInManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISiteConfig _siteConfig;

        private readonly ITemplateEmailSender _emailSender;
        private readonly UserManager _userManager;
        private TenantRepository TenantRepo => _unitOfWork.Repo<TenantRepository>();
        private LanguageRepository LangRepo => _unitOfWork.Repo<LanguageRepository>();

        public AccountService(
            IUnitOfWork unitOfWork,
            ISiteConfig siteConfig,
            ITemplateEmailSender emailSender,
            UserManager userManager,
            SignInManager signInManager)
        {
            _unitOfWork = unitOfWork;
            _siteConfig = siteConfig;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        public Task<SignInStatus> Login(LoginModel model)
        {
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            return _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);
        }

        public async Task<IdentityResult> Register(CreateDistributorModel model, string languageCode)
        {
            var user = model.MapTo<User>();
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, ERole.Distributor);
                await TenantRepo.UpdateOwner(user.TenantId, user.Id);
                await InsertDefaultLanguage(user, languageCode);

                await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }

            return result;
        }

        public async Task<IdentityResult> RegisterReseller(CreateResellerModel model, string languageCode)
        {
            var user = model.MapTo<User>();
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, ERole.Reseller);
                await InsertDefaultLanguage(user, languageCode);

                await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }

            return result;
        }

        public async Task<IdentityResult> ConfirmEmail(int userId, string code)
        {
            return await _userManager.ConfirmEmailAsync(userId, code);
        }

        public async Task<User> ForgotPassword(ForgotPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null) return null;

            var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);

            // Set request domain to web app domain
            var requestUrl = $"{_siteConfig.Settings.ConfirmEmailUrl()}?userId={user.Id}&code={HttpUtility.UrlEncode(code)}";
            await _emailSender.SendForgotPassword(user.Email, new Uri(requestUrl).AbsoluteUri);

            return user;
        }

        public async Task<IdentityResult> ResetPassword(ResetPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return new IdentityResult("The email does not exist");
            }

            return await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
        }

        private Task InsertDefaultLanguage(User createdUser, string languageCode)
        {
            var language = new Language
            {
                IsDefault = true,
                LanguageCode = languageCode,
                CreatedBy = createdUser.Id,
                TenantId = createdUser.TenantId
            };

            LangRepo.Insert(language);
            return _unitOfWork.SaveChangesAsync();
        }

    }
}
