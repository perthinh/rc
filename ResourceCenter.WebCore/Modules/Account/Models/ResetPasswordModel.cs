﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Modules.Account.Models
{
    public class ResetPasswordModel
    {
        [HiddenInput]
        public string Code { get; set; }

        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
