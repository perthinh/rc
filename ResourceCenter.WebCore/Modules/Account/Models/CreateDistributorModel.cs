﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Account.Models
{
    public class CreateDistributorModel : ICustomMappings
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        public bool AgreeTerms { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateDistributorModel, User>()
                .ForMember(x => x.Tenant, opt => opt.MapFrom(x => new Tenant
                {
                    Name = $"{x.UserName}",
                    IsActive = true
                }));
        }
    }
}