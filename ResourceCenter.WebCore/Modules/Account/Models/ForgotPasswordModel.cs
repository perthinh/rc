﻿using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.Account.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }
    }
}
