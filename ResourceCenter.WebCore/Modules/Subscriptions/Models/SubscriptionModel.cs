﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Subscriptions.Models
{
    public class SubscriptionModel : ICustomMappings
    {
        public int Id { get; set; }
        public string Subscriber { get; set; }

        [UIHint("App/TenantPartnerTypes")]
        [UIOptions(Deferred = false)]
        public int? PartnerTypeId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        [UIHint("App/SubscriptionStatus"), UIOptions(Deferred = false)]
        public ESubscriptionStatus Status { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Subscription, SubscriptionModel>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.Subscriber, opt => opt.MapFrom(x => x.Subscriber.UserName))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName))
                ;

            configuration.CreateMap<SubscriptionModel, Subscription>()
                .ForMember(x => x.Subscriber, opt => opt.Ignore())
                .ForMember(x => x.CreatedDate, opt => opt.Ignore());
        }
    }
}
