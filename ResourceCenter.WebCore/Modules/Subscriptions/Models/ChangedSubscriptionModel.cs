using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Subscriptions.Models
{
    public class ChangedSubscriptionModel : ICustomMappings
    {
        public int? PartnerTypeId { get; set; }
        public ESubscriptionStatus Status { get; set; }
        public Subscription Entity { get; set; }
        public string EmailTo => Entity?.Subscriber.Email;
        public string Subscriber => Entity?.Subscriber.UserName;
        public string NewPartnerType => Entity?.PartnerType?.Name;
        public string NewStatus => Entity?.Status.ToString();

        public bool HasChanges => Entity.Status != Status || Entity.PartnerTypeId != PartnerTypeId;

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Subscription, ChangedSubscriptionModel>()
                .ForMember(x => x.Entity, opt => opt.MapFrom(x => x));
        }
    }
}