﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.Infrastructure.Objects;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Subscriptions.Models;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Subscriptions.Services
{
    public interface ISubscriptionService
    {
        Task<List<PairInt>> GetTenantPartnerTypes();
        IQueryable<SubscriptionModel> GetList();
        Task<Subscription> Update(SubscriptionModel model);
    }

    public class SubscriptionService : ISubscriptionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUser _currentUser;
        private readonly ITemplateEmailSender _emailSender;
        private SubscriptionRepository SubscriptionRepo => _unitOfWork.Repo<SubscriptionRepository>();

        public SubscriptionService(IUnitOfWork unitOfWork, ICurrentUser currentUser, ITemplateEmailSender emailSender)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
            _emailSender = emailSender;
        }

        public async Task<List<PairInt>> GetTenantPartnerTypes()
        {
            return await _unitOfWork.Repo<PartnerTypeRepository>()
                            .GetByTenantId(_currentUser.Info.TenantId);
        }

        public IQueryable<SubscriptionModel> GetList()
        {
            return SubscriptionRepo.Queryable()
                         .Include(x => x.Subscriber)
                         .Include(x => x.UpdatedUser)
                         .Where(x => x.TenantId == _currentUser.Info.TenantId)
                         .ProjectTo<SubscriptionModel>();
        }

        public async Task<Subscription> Update(SubscriptionModel model)
        {
            var entity = await SubscriptionRepo.GetAsync(model.Id);

            var changedModel = entity.MapTo<ChangedSubscriptionModel>();

            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            await SendEmailIfAnyChanges(changedModel);

            entity.MapTo(model);
            return entity;
        }

        private Task SendEmailIfAnyChanges(ChangedSubscriptionModel model)
        {
            return model.HasChanges ? _emailSender.SendSubscriptionChanged(model) : Task.FromResult(0);
        }
    }
}