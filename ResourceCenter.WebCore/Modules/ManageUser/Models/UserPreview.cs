﻿using System;
using AutoMapper;
using Core.AutoMapper;
using Core.Timing;
using Core.Web.MetadataModel.MetadataAware;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.ManageUser.Models
{
    public class UserPreview : IMapping<User>
    {
        public int Id { get; set; }

        public bool IsLocked => LockoutEnabled && LockoutEndDateUtc.HasValue && LockoutEndDateUtc.Value.Date > Clock.Now.Date;

        public string Email { get; set; }

        public string UserName { get; set; }

        [NotRender]
        public bool LockoutEnabled { get; set; }

        [NotRender]
        public DateTime? LockoutEndDateUtc { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
    }
}
