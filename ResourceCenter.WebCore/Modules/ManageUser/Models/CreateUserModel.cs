﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.ManageUser.Models
{
    public class CreateUserModel : ICustomMappings
    {
        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(255)]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public bool IsLocked { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateUserModel, User>()
                .AfterMap((source, dest) =>
                {
                    dest.SetLockState(source.IsLocked);
                });
            configuration.CreateMap<User, CreateUserModel>();
        }
    }
}
