﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Timing;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.ManageUser.Models
{
    public class EditUserModel : ICustomMappings
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(255)]
        public string UserName { get; set; }

        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        [DependencyRequired("Password")]
        public string ConfirmPassword { get; set; }

        public bool IsLocked { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<EditUserModel, User>()
                .AfterMap((source, dest) =>
                {
                    dest.SetLockState(source.IsLocked);
                });
            configuration.CreateMap<User, EditUserModel>()
                .ForMember(x => x.IsLocked, opt => opt.ResolveUsing(source =>
                    source.LockoutEnabled &&
                    source.LockoutEndDateUtc.HasValue &&
                    source.LockoutEndDateUtc.Value.Date > Clock.Now.Date));
        }
    }
}
