﻿using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.Runtime.Session;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.AspNetIdentity.Extensions;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.ManageUser.Models;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.ManageUser.Services
{
    public interface IManageUserService : IUserService
    {
        IQueryable<UserPreview> GetList();
        Task<IdentityResult> Create(CreateUserModel model);
        Task<IdentityResult> Update(EditUserModel model);
        Task<IdentityResult> Delete(UserPreview model);
    }
    
    public class ManageUserService : UserService, IManageUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleService _rolserService;

        private UserRepository UserRepo => _unitOfWork.Repo<UserRepository>();

        public ManageUserService(
            IUnitOfWork unitOfWork,
            ITemplateEmailSender emailSender,
            IRoleService rolserService,
            UserManager userManager) : base(emailSender, userManager)
        {
            _unitOfWork = unitOfWork;
            _rolserService = rolserService;
        }

        public IQueryable<UserPreview> GetList()
        {
            var roleId = _rolserService.GetRoleId(ERole.User);
            var tenantId = UserSessions.TenantId.GetValueOrDefault();
            return UserRepo.Queryable().Where(x => x.Roles.Any(y => y.RoleId == roleId) && x.TenantId == tenantId).ProjectTo<UserPreview>();
        }

        public async Task<IdentityResult> Create(CreateUserModel model)
        {
            var user = model.MapTo<User>();
            user.TenantId = UserSessions.TenantId.GetValueOrDefault();

            // Create user
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await UserManager.AddToRoleAsync(user, ERole.User);

                // Send mail
                await EmailSender.SendUserCreated(model.Email, model.UserName, model.Password);
            }

            user.MapTo(model);
            return result;
        }

        public async Task<IdentityResult> Update(EditUserModel model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            model.MapTo(user);

            // Check if password changed
            if (!string.IsNullOrEmpty(model.Password))
            {
                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
            }

            var result = await UserManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                // Send mail
                await EmailSender.SendUserEdited(model);
            }

            user.MapTo(model);
            return result;
        }

        public async Task<IdentityResult> Delete(UserPreview model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            var result = await UserManager.DeleteAsync(user);

            return result;
        }
    }
}
