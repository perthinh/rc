﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;

namespace ResourceCenter.WebCore.Modules.Invitations.Models
{
    public class CreateInvitationModel : ICustomMappings
    {
        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string EmailTo { get; set; }

        [DataType(DataType.Date)]
        [UIOptions(FutureDate = true)]
        public DateTime? ExpiredDate { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateInvitationModel, DataAccess.Entities.Invitation>()
                .ForMember(x => x.InvitationCode, opt => opt.UseValue(Guid.NewGuid()));
        }
    }
}
