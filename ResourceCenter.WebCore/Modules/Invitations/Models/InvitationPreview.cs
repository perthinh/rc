using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.MetadataAware;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Invitations.Models
{
    public class InvitationPreview : ICustomMappings
    {
        [HiddenInput]
        public int Id { get; set; }

        [UIHint("ReadOnly")]
        public string EmailTo { get; set; }

        [UIHint("ReadOnly")]
        public Guid InvitationCode { get; set; }

        [NotRender]
        public EInvitationStatus Status { get; set; }

        [NotRender]
        public string SentBy { get; set; }

        [NotRender]
        public string RegisteredBy { get; set; }

        [DataType(DataType.Date)]
        [UIOptions(Deferred = false, Disabled = true)]
        public DateTime CreatedDate { get; set; }

        [UIOptions(Deferred = false, FutureDate = true)]
        [DataType(DataType.Date)]
        public DateTime? ExpiredDate { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Invitation, InvitationPreview>()
                .ForMember(x => x.SentBy, opt => opt.MapFrom(x => x.CreatedUser.UserName))
                .ForMember(x => x.RegisteredBy, opt => opt.MapFrom(x => x.RegisteredUser.UserName));
        }
    }
}