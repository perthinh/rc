﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Invitations.Models;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Invitations.Services
{
    public interface IInvitationService
    {
        IQueryable<InvitationPreview> GetList();
        Task Create(CreateInvitationModel model);
        Task Update(InvitationPreview model);
        Task ResendEmail(int id);
    }

    public class InvitationService : IInvitationService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ITemplateEmailSender _emailSender;
        private InvitationRepository InvitationRepo => _unitOfWork.Repo<InvitationRepository>();

        public InvitationService(IUnitOfWork unitOfWork, ITemplateEmailSender emailSender)
        {
            _unitOfWork = unitOfWork;
            _emailSender = emailSender;
        }

        public IQueryable<InvitationPreview> GetList()
        {
            return InvitationRepo.Queryable()
                        .Include(x => x.CreatedUser)
                        .Include(x => x.RegisteredUser)
                        .ProjectTo<InvitationPreview>();
        }

        public async Task Create(CreateInvitationModel model)
        {
            var entity = model.MapTo<Invitation>();
            await _emailSender.SendResellerInvitation(model.EmailTo, entity.InvitationCode);
            InvitationRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task Update(InvitationPreview model)
        {
            var entity = await InvitationRepo.GetAsync(model.Id);
            entity.ExpiredDate = model.ExpiredDate;
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task ResendEmail(int id)
        {
            var entity = await InvitationRepo.GetAsync(id);
            await _emailSender.SendResellerInvitation(entity.EmailTo, entity.InvitationCode);
        }
    }
}
