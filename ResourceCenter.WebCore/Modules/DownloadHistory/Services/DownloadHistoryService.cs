﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Helpers;
using ResourceCenter.WebCore.Modules.DownloadHistory.Models;

namespace ResourceCenter.WebCore.Modules.DownloadHistory.Services
{
    public interface IDownloadHistoryService
    {
        IQueryable<DownloadHistoryPreview> GetList();
        Task<Dictionary<string, string>> GetDetail(int id);
    }

    public class DownloadHistoryService : IDownloadHistoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        private AssetDownloadRepository AssetDownloadRepo => _unitOfWork.Repo<AssetDownloadRepository>();

        public DownloadHistoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<DownloadHistoryPreview> GetList()
        {
            return AssetDownloadRepo.Queryable()
                .Include(x => x.Asset)
                .Include(x => x.Subscriber)
                .Where(x => x.IsLead)
                .ProjectTo<DownloadHistoryPreview>();
        }

        public async Task<Dictionary<string, string>> GetDetail(int id)
        {
            var entity = await AssetDownloadRepo.GetAsync(id);
            return AssetLeadHelper.Deserialize(entity);
        }
    }
}
