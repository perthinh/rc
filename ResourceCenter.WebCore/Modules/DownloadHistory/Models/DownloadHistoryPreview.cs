﻿using System;
using AutoMapper;
using Core.AutoMapper;

namespace ResourceCenter.WebCore.Modules.DownloadHistory.Models
{
    public class DownloadHistoryPreview : ICustomMappings
    {
        public int Id { get; set; }
        public string AssetName { get; set; }
        public string SubscriberName { get; set; }
        public DateTime DownloadedDate { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DataAccess.Entities.AssetDownload, DownloadHistoryPreview>()
                .ForMember(x => x.AssetName, opt => opt.MapFrom(x => x.Asset.Alias))
                .ForMember(x => x.SubscriberName, opt => opt.MapFrom(x => x.Subscriber.UserName))
                .ForMember(x => x.DownloadedDate, opt => opt.MapFrom(x => x.CreatedDate));
        }
    }
}
