﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Extensions;
using Core.DataAccess.Uow;
using Core.Extensions;
using Core.Infrastructure.Paging;
using Core.Timing;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Dashboard.Models;
using ResourceCenter.WebCore.Services;

namespace ResourceCenter.WebCore.Modules.Dashboard.Services
{
    public interface IDashboardService
    {
        Task<int> GetDownloadCount(bool forToday, bool leadOnly);
        IPagedList<LatestResellerModel> GetLatestResellers();
        IPagedList<LatestAssetModel> GetLatestAssets();
        IPagedList<LatestDistributorModel> GetLatestDistributors();
    }

    public class DashboardService : IDashboardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUser _currentUser;
        private readonly IRoleService _roleService;
        private AssetDownloadRepository AssetDownloadRepo => _unitOfWork.Repo<AssetDownloadRepository>();
        private SubscriptionRepository SubscriptionRepo => _unitOfWork.Repo<SubscriptionRepository>();
        private AssetRepository AssetRepo => _unitOfWork.Repo<AssetRepository>();
        private UserRepository UserRepo => _unitOfWork.Repo<UserRepository>();

        public DashboardService(IUnitOfWork unitOfWork, ICurrentUser currentUser, IRoleService roleService)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
            _roleService = roleService;
        }

        public Task<int> GetDownloadCount(bool forToday, bool leadOnly)
        {
            var query = AssetDownloadRepo.AsNoFilter();

            query = query.WhereIf(leadOnly, x => x.IsLead)
                         .WhereIf(_currentUser.IsReseller(), x => x.SubscriberId == _currentUser.Id)
                         .WhereIf(_currentUser.IsDistributor(), x => x.TenantId == _currentUser.Info.TenantId);

            if (forToday)
            {
                query = query.Where(x => DbFunctions.TruncateTime(x.CreatedDate) == DbFunctions.TruncateTime(Clock.Now));
            }
            else
            {
                var start = Clock.Now.BeginOfTheMonth();
                var end = Clock.Now.EndOfTheMonth();
                query = query.Where(x => DbFunctions.TruncateTime(x.CreatedDate) >= DbFunctions.TruncateTime(start) &&
                                         DbFunctions.TruncateTime(x.CreatedDate) <= DbFunctions.TruncateTime(end));
            }

            return query.CountAsync();
        }

        public IPagedList<LatestResellerModel> GetLatestResellers()
        {
            var list = SubscriptionRepo.Queryable()
                .Include(x => x.Subscriber)
                .Include(x => x.PartnerType)
                .Where(x => x.TenantId == _currentUser.Info.TenantId)
                .OrderByDescending(x => x.UpdatedDate)
                .PageBy(1, 10, x => x.MapToList<LatestResellerModel>());

            return list;
        }

        public IPagedList<LatestDistributorModel> GetLatestDistributors()
        {
            var role = _roleService.GetRoleId(ERole.Distributor);

            var list = UserRepo.Queryable()
                .Where(x => x.Roles.Any(a => a.RoleId == role))
                .OrderByDescending(x => x.Id)
                .PageBy(1, 10, x => x.MapToList<LatestDistributorModel>());

            return list;
        }

        public IPagedList<LatestAssetModel> GetLatestAssets()
        {
            var list = AssetRepo.AsNoFilter()
                .Include(x => x.UpdatedUser)
                .WhereIf(_currentUser.IsDistributor(), x => x.TenantId == _currentUser.Info.TenantId)
                .OrderByDescending(x => x.UpdatedDate)
                .PageBy(1, 10, x => x.MapToList<LatestAssetModel>());

            return list;
        }
    }
}
