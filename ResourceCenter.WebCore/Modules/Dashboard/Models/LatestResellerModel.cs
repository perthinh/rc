﻿using System;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Dashboard.Models
{
    public class LatestResellerModel : ICustomMappings
    {
        public string UserName { get; set; }
        public string PartnerTypeName { get; set; }
        public DateTime UpdatedDate { get; set; }
        public ESubscriptionStatus Status { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Subscription, LatestResellerModel>()
                .ForMember(x => x.UserName, opt => opt.MapFrom(x => x.CreatedUser.UserName));
        }
    }
}
