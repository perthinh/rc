﻿using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Dashboard.Models
{
    public class LatestDistributorModel : ICustomMappings
    {
        public string UserName { get; set; }
        public string Email { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<User, LatestDistributorModel>();
        }
    }
}