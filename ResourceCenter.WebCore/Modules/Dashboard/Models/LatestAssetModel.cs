﻿using System;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Dashboard.Models
{
    public class LatestAssetModel : ICustomMappings
    {
        public string Alias { get; set; }
        public bool Published { get; set; }
        public bool IsCobranded { get; set; }
        public bool IsGated { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Asset, LatestAssetModel>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName));

        }
    }
}
