﻿using System.Data.Entity;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.PartnerTypes.Models;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceCenter.WebCore.Modules.PartnerTypes.Services
{
    public interface IPartnerTypeService
    {
        IQueryable<PartnerTypeModel> GetList();
        Task<PartnerType> Create(PartnerTypeModel model);
        Task<PartnerType> Update(PartnerTypeModel model);
        Task<PartnerType> Delete(PartnerTypeModel model);
    }

    public class PartnerTypeService : IPartnerTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private PartnerTypeRepository Repo => _unitOfWork.Repo<PartnerTypeRepository>();

        public PartnerTypeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<PartnerTypeModel> GetList()
        {
            return Repo.Queryable()
                    .Include(x => x.UpdatedUser)
                    .ProjectTo<PartnerTypeModel>();
        }

        public async Task<PartnerType> Create(PartnerTypeModel model)
        {
            var entity = model.MapTo<PartnerType>();
            Repo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<PartnerType> Update(PartnerTypeModel model)
        {
            var entity = await Repo.GetAsync(model.Id);
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<PartnerType> Delete(PartnerTypeModel model)
        {
            var entity = await Repo.GetAsync(model.Id);
            Repo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }
    }
}