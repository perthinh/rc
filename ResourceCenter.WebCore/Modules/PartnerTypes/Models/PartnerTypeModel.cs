﻿using System;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using System.ComponentModel.DataAnnotations;
using AutoMapper;

namespace ResourceCenter.WebCore.Modules.PartnerTypes.Models
{
    public class PartnerTypeModel : ICustomMappings
    {
        public int? Id { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<PartnerTypeModel, PartnerType>();
            configuration.CreateMap<PartnerType, PartnerTypeModel>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName));
        }
    }
}
