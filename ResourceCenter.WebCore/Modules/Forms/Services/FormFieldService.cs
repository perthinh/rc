using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Forms.Models;

namespace ResourceCenter.WebCore.Modules.Forms.Services
{
    public interface IFormFieldService
    {
        IQueryable<FormFieldPreview> GetList(int formId);
        Task<FormField> Get(int id);
        Task<FormField> Create(CreateFieldModel model);
        Task<FormField> Update(EditFieldModel model);
        Task<FormField> Delete(FormFieldPreview model);
    }

    public class FormFieldService : IFormFieldService
    {
        private readonly IUnitOfWork _unitOfWork;
        private FormFieldRepository FieldRepo => _unitOfWork.Repo<FormFieldRepository>();

        public FormFieldService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<FormFieldPreview> GetList(int formId)
        {
            return FieldRepo.Queryable()
                .Where(x => x.FormId == formId)
                .ProjectTo<FormFieldPreview>();
        }

        public async Task<FormField> Get(int id)
        {
            return await FieldRepo.GetAsync(id);
        }

        public async Task<FormField> Create(CreateFieldModel model)
        {
            var entity = model.MapTo<FormField>();
            FieldRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<FormField> Update(EditFieldModel model)
        {
            var entity = await Get(model.Id);
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<FormField> Delete(FormFieldPreview model)
        {
            var entity = await FieldRepo.GetAsync(model.Id);
            FieldRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }
    }
}