﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Forms.Models;
using Z.EntityFramework.Plus;

namespace ResourceCenter.WebCore.Modules.Forms.Services
{
    public interface IFormService
    {
        IQueryable<FormModel> GetList();
        Task<Form> Create(FormModel model);
        Task<Form> Update(FormModel model);
        Task<Form> Delete(FormModel model);
        Task<Form> Get(int id);
        Task<LeadFormModel> GetLeadForm(int formId, string languageCode);
    }

    public class FormService : IFormService
    {
        private readonly IUnitOfWork _unitOfWork;
        private FormRepository FormRepo => _unitOfWork.Repo<FormRepository>();
        private FormFieldRepository FieldRepo => _unitOfWork.Repo<FormFieldRepository>();

        public FormService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<FormModel> GetList()
        {
            return FormRepo.Queryable()
                        .Include(x => x.UpdatedUser)
                        .ProjectTo<FormModel>();
        }

        public async Task<Form> Get(int id)
        {
            return await FormRepo.GetAsync(id);
        }

        public async Task<Form> Create(FormModel model)
        {
            var entity = model.MapTo<Form>();
            FormRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<Form> Update(FormModel model)
        {
            var entity = await Get(model.Id.GetValueOrDefault());
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<Form> Delete(FormModel model)
        {
            var entity = await Get(model.Id.GetValueOrDefault());
            FormRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }

        public async Task<LeadFormModel> GetLeadForm(int formId, string languageCode)
        {
            var form = await FormRepo
                .AsNoFilter()
                .Where(x => x.Id == formId)
                .FirstOrDefaultAsync();

            var fields = await FieldRepo
                .AsNoFilter()
                .IncludeFilter(x => x.Translations.Where(a => a.LanguageCode == languageCode))
                .Where(x => x.FormId == formId)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            var result = form.MapTo<LeadFormModel>();
            result.Fields = fields.MapToList<LeadFieldModel>();
            return result;
        }
    }
}
