﻿using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class FormViewModel : ICustomMappings
    {
        public int FormId { get; set; }
        public string Name { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Form, FormViewModel>()
                .ForMember(x => x.FormId, a => a.MapFrom(x => x.Id));
        }
    }
}
