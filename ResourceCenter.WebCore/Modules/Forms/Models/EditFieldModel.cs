﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class EditFieldModel : IMapping<FormField>
    {
        [HiddenInput]
        public int Id { get; set; }

        [HiddenInput]
        [Required]
        public int FormId { get; set; }

        [Required]
        [StringLength(100)]
        [RegexPattern("^[a-zA-Z_0-9]+$")]
        public string FieldName { get; set; }
        public bool IsRequired { get; set; }

        public EFieldDataType DataType { get; set; }

        [Required]
        public int SortOrder { get; set; }
    }
}