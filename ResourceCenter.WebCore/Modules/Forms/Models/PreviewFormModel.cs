﻿using System.ComponentModel.DataAnnotations;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.MetadataAware;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class PreviewFormModel
    {
        [Required]
        [UIHint("App/UserLanguages")]
        [UIOptions(OptionLabel = "")]
        public string Language { get; set; }

        [NotRender]
        public LeadFormModel PreviewForm { get; set; }
    }
}
