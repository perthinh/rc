﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.MetadataAware;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class FormModel : ICustomMappings
    {
        [HiddenInput]
        public int? Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [NotRender]
        public DateTime ModifiedDate { get; set; }

        [NotRender]
        public string ModifiedBy { get; set; }

        [NotRender]
        public int FieldCount { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Form, FormModel>()
                .ForMember(x => x.FieldCount, opt => opt.MapFrom(x => x.FormFields.Count()))
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName));

            configuration.CreateMap<FormModel, Form>()
                .ForMember(x => x.CreatedDate, x => x.Ignore())
                .ForMember(x => x.UpdatedDate, x => x.Ignore());
        }
    }
}
