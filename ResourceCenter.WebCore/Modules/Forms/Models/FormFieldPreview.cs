﻿using System.ComponentModel.DataAnnotations;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class FormFieldPreview : IMapping<FormField>
    {
        public int Id { get; set; }
        public int FormId { get; set; }
        public string FieldName { get; set; }
        public EFieldDataType DataType { get; set; }
        public bool IsRequired { get; set; }

        [Required]
        public int SortOrder { get; set; }
        public int TenantId { get; set; }
    }
}