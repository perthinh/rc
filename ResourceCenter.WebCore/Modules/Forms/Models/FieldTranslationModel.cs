﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class FieldTranslationModel : IMapping<FormFieldTranslation>
    {
        [HiddenInput]
        public int? Id { get; set; }

        [Required]
        [StringLength(5)]
        [UIHint("App/UserLanguages")]
        [UIOptions(Deferred = false)]
        public string LanguageCode { get; set; }

        [Required]
        [StringLength(100)]
        public string Label { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        [UIOptions(HelpText = "Items are separated using newline as a delimiter")]
        public string DataSource { get; set; }

        [StringLength(255)]
        public string DefaultValue { get; set; }
    }
}
