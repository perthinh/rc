﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Resolvers;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class CreateFieldModel : ICustomMappings
    {
        [HiddenInput]
        [Required]
        public int FormId { get; set; }

        [Required]
        [StringLength(100)]
        [RegexPattern("^[a-zA-Z_0-9]+$")]
        public string FieldName { get; set; }

        [Required]
        [StringLength(100)]
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public EFieldDataType DataType { get; set; }

        [StringLength(255)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.MultilineText)]
        [HtmlAttr("disabled", "disabled")]
        [UIOptions(HelpText = "Items are separated using newline as a delimiter")]
        public string DataSource { get; set; }

        [StringLength(255)]
        public string DefaultValue { get; set; }

        [Required]
        public int SortOrder { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateFieldModel, FormField>()
                 .AfterMap((source, dest) =>
                 {
                     dest.Translations.Add(source.MapTo<FormFieldTranslation>());
                 });

            configuration.CreateMap<CreateFieldModel, FormFieldTranslation>()
                .ForMember(x => x.LanguageCode, opt => opt.ResolveUsing<DefaultLanguageResolver>());
        }
    }
}