﻿using AutoMapper;
using Core.AutoMapper;
using Core.Extensions;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Extensions;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class LeadFieldModel : ICustomMappings
    {
        public string FieldName { get; set; }
        public string Label { get; set; }
        public EFieldDataType DataType { get; set; }
        public string DefaultValue { get; set; }
        public string DataSource { get; set; }
        public bool IsRequired { get; set; }

        public string RequiredLabel => $"{Label}{(IsRequired ? " *" : "")}";
 
        public string[] ParsedDataSource => string.IsNullOrWhiteSpace(DataSource)
                                            ? new string[0]
                                            : DataSource.Split('\n');
        public T GetDefaultValue<T>()
        {
            return DefaultValue.ChangeTypeTo<T>(default(T));
        }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<FormField, LeadFieldModel>()
                .ForMember(x => x.Label, opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.Label)))
                .ForMember(x => x.DefaultValue,
                    opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.DefaultValue)))
                .ForMember(x => x.DataSource, opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.DataSource)));
        }

     
    }
}
