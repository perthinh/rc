﻿using System.Collections.Generic;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Forms.Models
{
    public class LeadFormModel : IMapFrom<Form>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AssetId { get; set; }
        public IList<LeadFieldModel> Fields { get; set; }

        public LeadFormModel()
        {
            Fields = new List<LeadFieldModel>();
        }
    }
}