﻿using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Categories.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using ResourceCenter.WebCore.Helpers;

namespace ResourceCenter.WebCore.Modules.Categories.Services
{
    public interface ICategoryService
    {
        Task<IList<CategoryPreview>> GetList();
        Task<Category> Get(int id);
        Task<Category> Create(CreateCategoryModel model);
        Task<Category> Update(EditCategoryModel model);
        Task Delete(int id);
    }

    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private CategoryRepository CategoryRepo => _unitOfWork.Repo<CategoryRepository>();

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IList<CategoryPreview>> GetList()
        {
            var list = await CategoryRepo.GetAllAsync();
            return TreeBuilder.BuildFlat(list.MapToList<CategoryPreview>());
        }

        public async Task<Category> Get(int id)
        {
            return await CategoryRepo.GetAsync(id);
        }

        public async Task<Category> Create(CreateCategoryModel model)
        {
            var entity = model.MapTo<Category>();
            CategoryRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<Category> Update(EditCategoryModel model)
        {
            var entity = await Get(model.Id);
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }

        public async Task Delete(int id)
        {
            var entity = await Get(id);
            CategoryRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
