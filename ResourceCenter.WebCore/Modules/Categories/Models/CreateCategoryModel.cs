﻿using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Resolvers;
using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.Categories.Models
{
    public class CreateCategoryModel : ICustomMappings
    {
        [UIHint("App/UserCategories")]
        public int? ParentId { get; set; }

        [Required, StringLength(100)]
        public string Alias { get; set; }

        [StringLength(100)]
        [DataType(DataType.MultilineText)]
        [HtmlAttr("rows", 3)]
        public string Description { get; set; }

        [Required]
        public int SortOrder { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateCategoryModel, Category>()
                .AfterMap((source, dest) =>
                {
                    dest.Translations.Add(source.MapTo<CategoryTranslation>());
                });

            configuration.CreateMap<CreateCategoryModel, CategoryTranslation>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Alias))
                .ForMember(x => x.LanguageCode, opt => opt.ResolveUsing<DefaultLanguageResolver>());
        }
    }
}
