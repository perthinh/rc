﻿using Core.AutoMapper;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Modules.Categories.Models
{
    public class EditCategoryModel : IMapping<Category>
    {
        [HiddenInput]
        public int Id { get; set; }

        [UIHint("App/UserCategories")]
        [SelfReference("Id")]
        public int? ParentId { get; set; }

        [Required, StringLength(100)]
        public string Alias { get; set; }

        public int SortOrder { get; set; }
    }
}