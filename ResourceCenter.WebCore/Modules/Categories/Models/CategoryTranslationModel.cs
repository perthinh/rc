﻿using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using System.ComponentModel.DataAnnotations;
using Core.Web.MetadataModel.Attributes;

namespace ResourceCenter.WebCore.Modules.Categories.Models
{
    public class CategoryTranslationModel : IMapping<CategoryTranslation>
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        [StringLength(5)]
        [UIHint("App/UserLanguages")]
        [UIOptions(Deferred = false)]
        public string LanguageCode { get; set; }
    }
}
