﻿using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Models;

namespace ResourceCenter.WebCore.Modules.Categories.Models
{
    public class CategoryPreview : ICustomMappings, IFlatNode
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Text { get; set; }
        public bool HasChildren { get; set; }
        public int SortOrder { get; set; }
        public int Level { get; set; }
        public int Padding => Level * 20;

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Category, CategoryPreview>()
                .ForMember(x => x.Text, opt => opt.MapFrom(x => x.Alias));
        }
    }
}