﻿using System.Data.Entity.Spatial;
using AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Modules.Location.Models;

namespace ResourceCenter.WebCore.Modules.Location.Resolvers
{
    public class LocationResolver : IValueResolver<UserLocationModel, UserLocation, DbGeography>
    {
        public DbGeography Resolve(UserLocationModel source, UserLocation destination, DbGeography destMember,
            ResolutionContext context)
        {
            var location = source.Location.Split(',');

            if (location.Length != 2) return null;

            var point = string.Format("POINT({1} {0})", location[0], location[1]);
            return DbGeography.FromText(point);
        }
    }
}
