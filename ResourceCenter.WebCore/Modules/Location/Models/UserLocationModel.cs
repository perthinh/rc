﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Modules.Location.Resolvers;

namespace ResourceCenter.WebCore.Modules.Location.Models
{
    public class UserLocationModel : ICustomMappings
    {
        [HiddenInput]
        public int? Id { get; set; }

        [Required]
        [StringLength(100)]
        [HiddenInput]
        public string Description { get; set; }

        [Required]
        [UIHint("App/Geography")]
        public string Location { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<UserLocation, UserLocationModel>()
                .ForMember(x => x.Location, opt => opt.ResolveUsing(x => $"{x.Location.Latitude},{x.Location.Longitude}"));
            configuration.CreateMap<UserLocationModel, UserLocation>()
                .ForMember(x => x.Location, opt => opt.ResolveUsing<LocationResolver>());
        }
    }
}
