﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Location.Models;

namespace ResourceCenter.WebCore.Modules.Location.Services
{
    public interface IUserLocationService
    {
        IQueryable<UserLocationPreview> GetList();
        Task<UserLocation> Create(UserLocationModel model);
        Task<UserLocation> Update(UserLocationModel model);
        Task<UserLocation> Delete(UserLocationPreview model);
        Task<UserLocation> GetLocation(int id);
    }

    public class UserLocationService : IUserLocationService
    {
        private readonly IUnitOfWork _unitOfWork;

        private UserLocationRepository UserLocationRepo => _unitOfWork.Repo<UserLocationRepository>();

        public UserLocationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<UserLocationPreview> GetList()
        {
            return UserLocationRepo.Queryable().Include(x => x.UpdatedUser).ProjectTo<UserLocationPreview>();
        }

        public async Task<UserLocation> GetLocation(int id)
        {
            return await UserLocationRepo.GetAsync(id);
        }

        public async Task<UserLocation> Create(UserLocationModel model)
        {
            var entity = model.MapTo<UserLocation>();

            // Create user
            UserLocationRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);

            return entity;
        }

        public async Task<UserLocation> Update(UserLocationModel model)
        {
            var entity = await UserLocationRepo.GetAsync(model.Id);
            model.MapTo(entity);

            UserLocationRepo.Update(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<UserLocation> Delete(UserLocationPreview model)
        {
            var entity = await UserLocationRepo.GetAsync(model.Id);
            UserLocationRepo.Delete(entity);

            await _unitOfWork.SaveChangesAsync();

            return entity;
        }
    }
}
