﻿using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.AssetLanguages.Models
{
    public class LanguageModel : IMapping<Language>
    {
        public int? Id { get; set; }

        [Required, UIHint("App/LanguagePicker")]
        [UIOptions(Deferred = false)]
        public string LanguageCode { get; set; }
        public bool IsDefault { get; set; }
    }
}
