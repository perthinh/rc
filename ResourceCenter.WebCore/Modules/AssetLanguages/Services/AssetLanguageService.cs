﻿using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.AssetLanguages.Models;
using System.Linq;
using System.Threading.Tasks;
using ResourceCenter.WebCore.Services;

namespace ResourceCenter.WebCore.Modules.AssetLanguages.Services
{
    public interface IAssetLanguageService
    {
        IQueryable<LanguageModel> Queryable();
        Task Insert(LanguageModel model);
        Task Update(LanguageModel model);
        Task Delete(LanguageModel model);
    }

    public class AssetLanguageService : IAssetLanguageService
    {
        private readonly ICurrentUser _currentUser;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserLanguage _userLanguage;
        private LanguageRepository LangRepo => _unitOfWork.Repo<LanguageRepository>();

        public AssetLanguageService(ICurrentUser currentUser, IUnitOfWork unitOfWork, IUserLanguage userLanguage)
        {
            _currentUser = currentUser;
            _unitOfWork = unitOfWork;
            _userLanguage = userLanguage;
        }

        public IQueryable<LanguageModel> Queryable()
        {
            var query = LangRepo.Queryable();
            if (_currentUser.Info.TenantId == Tenant.DefaultId)
            {
                query = query.Where(x => x.CreatedBy == _currentUser.Id);
            }
            return query.ProjectTo<LanguageModel>();
        }

        public async Task Insert(LanguageModel model)
        {
            var entity = model.MapTo<Language>();
            LangRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();
            entity.MapTo(model);

            _userLanguage.Refresh();
        }

        public async Task Update(LanguageModel model)
        {
            var entity = await LangRepo.GetAsync(model.Id);
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);

            _userLanguage.Refresh();
        }

        public async Task Delete(LanguageModel model)
        {
            var entity = await LangRepo.GetAsync(model.Id);
            LangRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();

            _userLanguage.Refresh();
        }
    }
}
