﻿using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Profile.Models;

namespace ResourceCenter.WebCore.Modules.Profile.Services
{
    public interface IProfileService
    {
        Task<PersonalInfoModel> GetPersonalInfo();
        Task<CompanyModel> GetCompanyInfo();
        Task<IdentityResult> UpdatePersonalInfo(PersonalInfoModel model);
        Task UpdateCompanyInfo(CompanyModel model);
        Task<IdentityResult> ChangePassword(ChangePasswordModel model);
    }

    public class ProfileService : IProfileService
    {
        private readonly ICurrentUser _currentUser;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager _userManager;

        private CompanyRepository CompanyRepo => _unitOfWork.Repo<CompanyRepository>();

        public ProfileService(
            ICurrentUser currentUser,
            IUnitOfWork unitOfWork,
            UserManager userManager)
        {
            _currentUser = currentUser;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<PersonalInfoModel> GetPersonalInfo()
        {
            return (await _userManager.FindByIdAsync(_currentUser.Id)).MapTo<PersonalInfoModel>();
        }

        public async Task<CompanyModel> GetCompanyInfo()
        {
            var company = await CompanyRepo.GetAsync(_currentUser.Id);
            return company?.MapTo<CompanyModel>();
        }

        public async Task<IdentityResult> UpdatePersonalInfo(PersonalInfoModel model)
        {
            var user = await _userManager.FindByIdAsync(_currentUser.Id);
            model.MapTo(user);

            return await _userManager.UpdateAsync(user);
        }

        public async Task UpdateCompanyInfo(CompanyModel model)
        {
            var company = await CompanyRepo.GetAsync(_currentUser.Id);
            if (company != null)
            {
                model.MapTo(company);
            }
            else
            {
                CompanyRepo.Insert(model.MapTo<Company>());
            }
            await _unitOfWork.SaveChangesAsync();
            company.MapTo(model);
        }

        public async Task<IdentityResult> ChangePassword(ChangePasswordModel model)
        {
            return await _userManager.ChangePasswordAsync(_currentUser.Id, model.OldPassword, model.NewPassword);
        }
    }
}
