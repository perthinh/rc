﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Resolvers;

namespace ResourceCenter.WebCore.Modules.Profile.Models
{
    public class CompanyModel : ICustomMappings
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        [UIHint("App/FilePicker")]
        public string Logo { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string County { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(255)]
        [Url]
        public string WebsiteUrl { get; set; }

        [StringLength(255)]
        public string ExternalCode { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Company, CompanyModel>();
            configuration.CreateMap<CompanyModel, Company>()
                .ForMember(x => x.Id, opt => opt.ResolveUsing<CurrentUserResolver>());
        }
    }
}
