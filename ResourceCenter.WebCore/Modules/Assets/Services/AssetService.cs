﻿using System.Data.Entity;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.IO;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Assets.Models;
using System.Linq;
using System.Threading.Tasks;
using ResourceCenter.WebCore.Services;

namespace ResourceCenter.WebCore.Modules.Assets.Services
{
    public interface IAssetService
    {
        IQueryable<AssetPreview> GetList(int? partnerTypeId);
        Task<Asset> Get(int id);
        Task<Asset> Create(CreateAssetModel model);
        Task<Asset> Update(EditAssetModel model);
        Task<Asset> Delete(AssetPreview model);
    }

    public class AssetService : IAssetService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUploadService _uploadService;
        private AssetRepository AssetRepo => _unitOfWork.Repo<AssetRepository>();

        public AssetService(IUnitOfWork unitOfWork, IUploadService uploadService)
        {
            _unitOfWork = unitOfWork;
            _uploadService = uploadService;
        }

        public IQueryable<AssetPreview> GetList(int? partnerTypeId)
        {
            var query = AssetRepo.Queryable()
                .Include(x => x.Category)
                .Include(x => x.UpdatedUser);

            if (partnerTypeId.HasValue)
            {
                query = query.Where(x => x.PartnerTypes.Any(a => a.Id == partnerTypeId.Value));
            }

            return query.ProjectTo<AssetPreview>();
        }

        public async Task<Asset> Get(int id)
        {
            return await AssetRepo.Queryable()
                   .Include(x => x.PartnerTypes)
                   .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Asset> Create(CreateAssetModel model)
        {
            var entity = model.MapTo<Asset>();
            entity.FilePath = _uploadService.Upload(model.FileUpload);

            AssetRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<Asset> Update(EditAssetModel model)
        {
            var entity = await Get(model.Id);
            model.MapTo(entity);

            if (model.FileUpload != null)
            {
                // Delete old file
                FileHelper.DeleteIfExists(entity.FilePath);

                entity.FilePath = _uploadService.Upload(model.FileUpload);
            }

            await _unitOfWork.SaveChangesAsync();
            return entity;
        }

        public async Task<Asset> Delete(AssetPreview model)
        {
            var entity = await Get(model.Id);
            AssetRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();

            FileHelper.DeleteIfExists(entity.FilePath);
            return entity;
        }
    }
}
