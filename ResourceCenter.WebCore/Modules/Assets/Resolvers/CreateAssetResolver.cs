using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Assets.Models;

namespace ResourceCenter.WebCore.Modules.Assets.Resolvers
{
    public class CreateAssetResolver : IValueResolver<CreateAssetModel, object, IList<PartnerType>>
    {
        public IList<PartnerType> Resolve(CreateAssetModel source, object destination, IList<PartnerType> destMember, ResolutionContext context)
        {
            if (!source.PartnerTypes.Any())
            {
                return destMember;
            }

            var userId = DependencyResolver.Current.GetService<ICurrentUser>().Id;
            return DependencyResolver.Current.GetService<PartnerTypeRepository>()
                        .AsNoFilter()
                        .Where(x => x.CreatedBy == userId && source.PartnerTypes.Contains(x.Id))
                        .ToList();
        }
    }
}