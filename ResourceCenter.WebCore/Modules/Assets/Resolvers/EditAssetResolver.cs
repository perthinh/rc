using AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Assets.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Modules.Assets.Resolvers
{
    public class EditAssetResolver : IValueResolver<EditAssetModel, object, IList<PartnerType>>
    {
        public IList<PartnerType> Resolve(EditAssetModel source, object destination, IList<PartnerType> destMember, ResolutionContext context)
        {
            if (!source.PartnerTypes.Any())
            {
                return null;
            }
            var oldTypes = new HashSet<int>(destMember.Select(x => x.Id));
            var newTypes = new HashSet<int>(source.PartnerTypes);

            var currentUser = DependencyResolver.Current.GetService<ICurrentUser>();
            var allTypes = DependencyResolver.Current.GetService<PartnerTypeRepository>()
                .AsNoFilter()
                .Where(x => x.CreatedBy == currentUser.Id)
                .ToList();

            foreach (var item in allTypes)
            {
                if (newTypes.Contains(item.Id))
                {
                    if (!oldTypes.Contains(item.Id))
                    {
                        destMember.Add(item);
                    }
                }
                else if (oldTypes.Contains(item.Id))
                {
                    destMember.Remove(item);
                }
            }
            return new List<PartnerType>(destMember);
        }
    }
}