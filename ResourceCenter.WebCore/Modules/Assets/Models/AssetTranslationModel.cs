﻿using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Modules.Assets.Models
{
    public class AssetTranslationModel : IMapping<AssetTranslation>
    {
        [HiddenInput]
        public int? Id { get; set; }

        [Required]
        [StringLength(5)]
        [UIHint("App/UserLanguages")]
        [UIOptions(Deferred = false)]
        public string LanguageCode { get; set; }

        [Required, StringLength(255)]
        public string AssetTitle { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        [HtmlAttr("rows", 3)]
        public string ShortDescription { get; set; }

        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        public string LongDescription { get; set; }
    }
}
