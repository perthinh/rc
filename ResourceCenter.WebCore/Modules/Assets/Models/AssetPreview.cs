﻿using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using System;
using AutoMapper;

namespace ResourceCenter.WebCore.Modules.Assets.Models
{
    public class AssetPreview : ICustomMappings
    {
        public int Id { get; set; }
        public string CategoryAlias { get; set; }
        public string Alias { get; set; }
        public string FileName { get; set; }
        public bool Published { get; set; }
        public bool IsGated { get; set; }
        public bool IsCobranded { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Asset, AssetPreview>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName));

            configuration.CreateMap<AssetPreview, Asset>();
        }
    }
}