﻿using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.Assets.Models
{
    public class AssetFilterModel
    {
        [UIHint("App/UserPartnerTypeDropdown")]
        public int? PartnerTypeId { get; set; }
    }
}
