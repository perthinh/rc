﻿using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.MetadataAware;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Modules.Assets.Resolvers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Modules.Assets.Models
{
    public class EditAssetModel : ICustomMappings
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required, UIHint("App/UserCategories")]
        public int CategoryId { get; set; }

        [Required, StringLength(255)]
        public string Alias { get; set; }
        public bool Published { get; set; }
        public bool IsCobranded { get; set; }
        public bool IsGated { get; set; }

        [UIHint("App/UserForms")]
        public int? FormId { get; set; }


        [UIHint("App/UserPartnerTypes")]
        public IList<int> PartnerTypes { get; set; }

        [UIHint("App/FilePicker")]
        public string Thumbnail { get; set; }

        [TextOnly]
        public string FileName { get; set; }

        [FileUpload(IsRequired = false)]
        public HttpPostedFileBase FileUpload { get; set; }

        public EditAssetModel()
        {
            PartnerTypes = new List<int>();
        }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Asset, EditAssetModel>()
                .ForMember(x => x.PartnerTypes, opt => opt.MapFrom(x => x.PartnerTypes.Select(a => a.Id)));
            configuration.CreateMap<EditAssetModel, Asset>()
                .ForMember(x => x.FileName, opt => opt.Ignore())
                .ForMember(x => x.PartnerTypes, x => x.ResolveUsing<EditAssetResolver>())
                .AfterMap((model, entity) =>
                {
                    if (model.FileUpload == null) return;
                    // update new file name if there is new file
                    entity.FileName = model.FileUpload.FileName;
                });
        }

    }
}