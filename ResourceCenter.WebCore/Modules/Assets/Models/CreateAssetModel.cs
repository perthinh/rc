﻿using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.Validations;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Resolvers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using ResourceCenter.WebCore.Modules.Assets.Resolvers;

namespace ResourceCenter.WebCore.Modules.Assets.Models
{
    public class CreateAssetModel : ICustomMappings
    {
        [Required, UIHint("App/UserCategories")]
        public int CategoryId { get; set; }

        [Required, StringLength(255)]
        public string Alias { get; set; }

        [Required, StringLength(255)]
        public string AssetTitle { get; set; }

        [StringLength(255)]
        [DataType(DataType.MultilineText)]
        [HtmlAttr("rows", 3)]
        public string ShortDescription { get; set; }

        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        public string LongDescription { get; set; }

        [UIHint("App/UserPartnerTypes")]
        public IList<int> PartnerTypes { get; set; }
        public bool Published { get; set; }
        public bool IsCobranded { get; set; }
        public bool IsGated { get; set; }

        [UIHint("App/UserForms")]
        public int? FormId { get; set; }

        [UIHint("App/FilePicker")]
        public string Thumbnail { get; set; }

        [FileUpload]
        public HttpPostedFileBase FileUpload { get; set; }

        public CreateAssetModel()
        {
            PartnerTypes = new List<int>();
            Published = true;
        }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateAssetModel, Asset>()
                .ForMember(x => x.FileName, opt => opt.MapFrom(x => x.FileUpload.FileName))
                .ForMember(x => x.PartnerTypes, x => x.ResolveUsing<CreateAssetResolver>())
                .AfterMap((source, dest) =>
                {
                    dest.AssetCounter = new AssetCounter();
                    dest.Translations.Add(source.MapTo<AssetTranslation>());
                }); 

            configuration.CreateMap<CreateAssetModel, AssetTranslation>()
                .ForMember(x => x.LanguageCode, opt => opt.ResolveUsing<DefaultLanguageResolver>());
        }
    }
}
