﻿using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Models;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class TranslatedCategoryModel : IFlatNode, ICustomMappings
    {
        public int Id { get; set; }
        public int SortOrder { get; set; }
        public int Level { get; set; }
        public string Text { get; set; }
        public bool HasChildren { get; set; }
        public int? ParentId { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Category, TranslatedCategoryModel>()
                .ForMember(x => x.Text, opt => opt.MapFrom(x => x.Translations.Translate(a => a.Name)));
        }
    }
}
