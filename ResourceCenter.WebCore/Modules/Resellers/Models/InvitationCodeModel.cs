﻿using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class InvitationCodeModel
    {
        [Required]
        [StringLength(40)]
        public string InvitationCode { get; set; }
    }
}
