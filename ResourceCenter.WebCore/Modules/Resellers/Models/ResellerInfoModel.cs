﻿using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class ResellerInfoModel : ICustomMappings
    {
        public string CompanyName { get; set; }
        public string Logo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string WebsiteUrl { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }


        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Company, ResellerInfoModel>()
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.Owner.Email))
                .ForMember(x => x.PhoneNumber, opt => opt.MapFrom(x => x.Owner.PhoneNumber));
        }
    }
}
