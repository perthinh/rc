using System.ComponentModel.DataAnnotations;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class PreviewAssetModel 
    {
        [Required]
        [UIHint("App/Distributors")]
        public int TenantId { get; set; }

        [Required]
        [UIHint("App/CascadeLanguage")]
        public string LanguageCode { get; set; }
    }
}