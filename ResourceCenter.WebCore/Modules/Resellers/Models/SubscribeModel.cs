﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Resolvers;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class SubscribeModel : ICustomMappings
    {
        [Required]
        [UIHint("App/Distributors")]
        public int TenantId { get; set; }

        [UIHint("App/CascadePartnerType")]
        public int? PartnerTypeId { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<SubscribeModel, Subscription>()
                .ForMember(x => x.Status, opt => opt.UseValue(ESubscriptionStatus.Pending))
                .ForMember(x => x.SubscriberId, opt => opt.ResolveUsing<CurrentUserResolver>());
        }
    }
}
