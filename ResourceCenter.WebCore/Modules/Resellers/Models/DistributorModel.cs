﻿using System;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class DistributorModel : ICustomMappings
    {
        public int Id { get; set; }
        public string Distributor { get; set; }

        public string PartnerTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public ESubscriptionStatus Status { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Subscription, DistributorModel>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.Distributor, opt => opt.MapFrom(x => x.Tenant.Owner.UserName));

            configuration.CreateMap<DistributorModel, Subscription>()
                .ForMember(x => x.CreatedDate, opt => opt.Ignore());
        }
    }
}
