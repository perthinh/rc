﻿using System;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Extensions;

namespace ResourceCenter.WebCore.Modules.Resellers.Models
{
    public class TranslatedAssetModel : ICustomMappings
    {
        public int Id { get; set; }
        public string AssetTitle { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Thumbnail { get; set; }
        public bool IsGated { get; set; }
        public bool IsCobranded { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? FormId { get; set; }
        public int CategoryId { get; set; }
        public int TenantId { get; set; }
        public int? PartnerTypeId { get; set; }
        public int? SubscriberId { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<SubscriptionAsset, TranslatedAssetModel>()
                .ForMember(x => x.AssetTitle,
                    opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.AssetTitle)))
                .ForMember(x => x.ShortDescription,
                    opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.ShortDescription)))
                .ForMember(x => x.LongDescription,
                    opt => opt.ResolveUsing(x => x.Translations.Translate(a => a.ShortDescription)));
        }
    }
}
