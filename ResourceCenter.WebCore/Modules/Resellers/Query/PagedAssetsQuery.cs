
namespace ResourceCenter.WebCore.Modules.Resellers.Query
{
    public class PagedAssetsQuery : ResourceQuery
    {
        public int? CategoryId { get; set; }
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 5;
    }
}