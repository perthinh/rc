﻿namespace ResourceCenter.WebCore.Modules.Resellers.Query
{
    public class AssetDetailQuery : ResourceQuery
    {
        public int AssetId { get; set; }
    }
}
