namespace ResourceCenter.WebCore.Modules.Resellers.Query
{
    public class ResourceQuery
    {
        public int TenantId { get; set; }
        public int ResellerId { get; set; }
        public string Lang { get; set; }
    }
}