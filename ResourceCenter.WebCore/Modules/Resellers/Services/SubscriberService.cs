﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.Exceptions;
using Core.Timing;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Resellers.Models;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Resellers.Services
{
    public interface ISubscriberService
    {
        Task<bool> IsSubscribed(int tenantId);
        Task Subscribe(SubscribeModel model);
        Task Subscribe(InvitationCodeModel model);
        Task<List<Subscription>> GetSubscribedTenants();
    }

    public class SubscriberService : ISubscriberService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUser _currentUser;
        private readonly ITemplateEmailSender _emailSender;

        private SubscriptionRepository SubscriptionRepo => _unitOfWork.Repo<SubscriptionRepository>();
        private InvitationRepository InvitationRepo => _unitOfWork.Repo<InvitationRepository>();
        private TenantRepository TenantRepo => _unitOfWork.Repo<TenantRepository>();

        public SubscriberService(IUnitOfWork unitOfWork, ICurrentUser currentUser, ITemplateEmailSender emailSender)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
            _emailSender = emailSender;
        }

        public Task<bool> IsSubscribed(int tenantId)
        {
            return SubscriptionRepo.AnyAsync(x =>
                           x.SubscriberId == _currentUser.Id &&
                           x.TenantId == tenantId);
        }

        public async Task Subscribe(SubscribeModel model)
        {
            var entity = model.MapTo<Subscription>();
            SubscriptionRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            var emailTo = await TenantRepo.GetTenantEmail(model.TenantId);
            await _emailSender.SendSubscriberNotification(emailTo, _currentUser.Info.UserName);
        }

        public Task<List<Subscription>> GetSubscribedTenants()
        {
            var userId = _currentUser.Id;
            return SubscriptionRepo.AsNoFilter()
                        .Where(x => x.SubscriberId == userId &&
                                    x.Status == ESubscriptionStatus.Active)
                        .ToListAsync();
        }

        public async Task Subscribe(InvitationCodeModel model)
        {
            Guid code;
            if (!Guid.TryParse(model.InvitationCode, out code))
            {
                throw new BusinessException("Invalid invitation code");
            }

            var entity = await InvitationRepo.AsNoFilter()
                                    .SingleOrDefaultAsync(x => x.InvitationCode == code);

            if (entity == null)
            {
                throw new BusinessException("Invalid invitation code");
            }

            if (entity.ExpiredDate < Clock.Now || entity.Status == EInvitationStatus.Accepted)
            {
                throw new BusinessException("Invitation code was expired/used");
            }

            entity.RegisteredUserId = _currentUser.Id;
            entity.Status = EInvitationStatus.Accepted;

            SubscriptionRepo.Insert(new Subscription
            {
                TenantId = entity.TenantId,
                SubscriberId = _currentUser.Id,
                Status = ESubscriptionStatus.Pending
            });

            await _unitOfWork.SaveChangesAsync();
        }

    }
}