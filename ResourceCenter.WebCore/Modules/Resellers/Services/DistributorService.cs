﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Modules.Resellers.Models;

namespace ResourceCenter.WebCore.Modules.Resellers.Services
{
    public interface IDistributorService
    {
        IQueryable<DistributorModel> GetList();
        Task ChangeStatus(int subId, ESubscriptionStatus status);
    }

    public class DistributorService : IDistributorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentUser _currentUser;

        private SubscriptionRepository SubRepo => _unitOfWork.Repo<SubscriptionRepository>();

        public DistributorService(IUnitOfWork unitOfWork, ICurrentUser currentUser)
        {
            _unitOfWork = unitOfWork;
            _currentUser = currentUser;
        }

        public IQueryable<DistributorModel> GetList()
        {
            return SubRepo.Queryable()
                .Where(x => x.SubscriberId == _currentUser.Id)
                .Include(x => x.Tenant.Owner)
                .ProjectTo<DistributorModel>();
        }

        public async Task ChangeStatus(int subId, ESubscriptionStatus status)
        {
            var sub = await SubRepo.GetAsync(subId);
            if (sub != null)
            {
                sub.Status = status;
            }

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
