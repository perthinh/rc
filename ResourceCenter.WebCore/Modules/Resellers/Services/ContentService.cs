﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Extensions;
using Core.DataAccess.Uow;
using Core.Infrastructure.Paging;
using Core.Web.Caching;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Helpers;
using ResourceCenter.WebCore.Modules.Forms.Models;
using ResourceCenter.WebCore.Modules.Resellers.Models;
using ResourceCenter.WebCore.Modules.Resellers.Query;
using Z.EntityFramework.Plus;

namespace ResourceCenter.WebCore.Modules.Resellers.Services
{
    public interface IContentService
    {
        IPagedList<TranslatedAssetModel> GetAssets(PagedAssetsQuery pagination);
        Task<LeadFormModel> GetAssetLeadForm(int assetId, string languageCode);
        Task<TranslatedAssetModel> GetAssetDetail(AssetDetailQuery query);

        ResellerInfoModel GetResellerInfo(int resellerId);
        IList<TranslatedCategoryModel> GetCategories(ResourceQuery query);
        Task<int?> GetSubscribedPartnerType(int tenantId, int resellerId);
    }

    public class ContentService : IContentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPerRequestCache _perRequestCache;

        private SubscriptionRepository SubscriptionRepo => _unitOfWork.Repo<SubscriptionRepository>();
        private SubscriptionAssetRepository SubscriptionAssetRepo => _unitOfWork.Repo<SubscriptionAssetRepository>();
        private FormFieldRepository FieldRepo => _unitOfWork.Repo<FormFieldRepository>();
        private FormRepository FormRepo => _unitOfWork.Repo<FormRepository>();
        private CategoryRepository CategoryRepo => _unitOfWork.Repo<CategoryRepository>();
        private CompanyRepository CompanyRepo => _unitOfWork.Repo<CompanyRepository>();

        public ContentService(IUnitOfWork unitOfWork, IPerRequestCache perRequestCache)
        {
            _unitOfWork = unitOfWork;
            _perRequestCache = perRequestCache;
        }

        public IList<TranslatedCategoryModel> GetCategories(ResourceQuery query)
        {
            var list = CategoryRepo.AsNoFilter()
                .IncludeOptimized(x => x.Translations.Where(a => a.LanguageCode == query.Lang))
                .Where(x => x.TenantId == query.TenantId)
                .OrderBy(x => x.SortOrder)
                .ToList();

            return TreeBuilder.BuildFlat(list.MapToList<TranslatedCategoryModel>());
        }

        public async Task<LeadFormModel> GetAssetLeadForm(int assetId, string languageCode)
        {
            var form = await FormRepo
                .AsNoFilter()
                .Where(x => x.Assets.Any(a => a.Id == assetId))
                .FirstOrDefaultAsync();

            var fields = await FieldRepo
                .AsNoFilter()
                .IncludeOptimized(x => x.Translations.Where(a => a.LanguageCode == languageCode))
                .Where(x => x.FormId == form.Id)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            var result = form.MapTo<LeadFormModel>();
            result.Fields = fields.MapToList<LeadFieldModel>();
            result.AssetId = assetId;
            return result;
        }

        public IPagedList<TranslatedAssetModel> GetAssets(PagedAssetsQuery pagination)
        {
            var ptId = _perRequestCache.RequestPartnerTypeId();

            var countQuery = SubscriptionAssetRepo
                .AsNoFilter()
                .Where(x => x.SubscriberId == pagination.ResellerId &&
                            x.TenantId == pagination.TenantId &&
                            x.PartnerTypeId == ptId);
            if (pagination.CategoryId.HasValue)
            {
                countQuery = countQuery.Where(x => x.CategoryId == pagination.CategoryId);
            }

            var mainQuery = countQuery
                    .IncludeOptimized(x => x.Translations.Where(a => a.LanguageCode == pagination.Lang))
                    .OrderByDescending(x => x.CreatedDate);

            return mainQuery.PageBy(countQuery, pagination.Page, pagination.PageSize, x => x.MapToList<TranslatedAssetModel>());
        }


        public async Task<TranslatedAssetModel> GetAssetDetail(AssetDetailQuery query)
        {
            var ptId = _perRequestCache.RequestPartnerTypeId();
            var entity = await SubscriptionAssetRepo
                            .AsNoFilter()
                            .IncludeOptimized(x => x.Translations.FirstOrDefault(a => a.LanguageCode == query.Lang))
                            .FirstOrDefaultAsync(x => x.SubscriberId == query.ResellerId &&
                                        x.TenantId == query.TenantId &&
                                        x.PartnerTypeId == ptId &&
                                        x.Id == query.AssetId);

            return entity.MapTo<TranslatedAssetModel>();
        }

        public ResellerInfoModel GetResellerInfo(int resellerId)
        {
            return _perRequestCache.GetOrAdd("__ContentService.GetResellerInfo", () =>
            {
                var model = new ResellerInfoModel();
                if (resellerId > 0)
                {
                    var entity = CompanyRepo.AsNoFilter()
                                .Include(x => x.Owner)
                                .SingleOrDefault(x => x.Id == resellerId);

                    entity.MapTo(model);
                }
                return model;
            });
        }

        public Task<int?> GetSubscribedPartnerType(int tenantId, int resellerId)
        {
            return SubscriptionRepo
                .AsNoFilter()
                .Where(x => x.SubscriberId == resellerId &&
                                          x.TenantId == tenantId &&
                                          x.Status == ESubscriptionStatus.Active)
                .Select(x => x.PartnerTypeId)
                .FirstOrDefaultAsync();
        }
    }
}