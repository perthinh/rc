﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Host.Models
{
    public class EditTemplateEmail : ICustomMappings
    {
        [HiddenInput, Required]
        public int Id { get; set; }

        [HtmlAttr("style", "width:300px"), Required]
        public ETemplateName Name { get; set; }

        [UIHint("HtmlEditor")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsDefault { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TemplateEmail, EditTemplateEmail>()
                .ForMember(x => x.IsDefault, opt => opt.MapFrom(x => x.Active));
            configuration.CreateMap<EditTemplateEmail, TemplateEmail>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name.ToString()))
                .ForMember(x => x.Active, opt => opt.MapFrom(x => x.IsDefault));
        }
    }
}
