﻿using System;
using Core.AutoMapper;
using Core.Timing;
using Core.Web.MetadataModel.MetadataAware;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Host.Models
{
    public class HostUserPreview : IMapping<User>
    {
        public int Id { get; set; }

        public bool IsLocked => LockoutEnabled && LockoutEndDateUtc.HasValue && LockoutEndDateUtc.Value.Date > Clock.Now.Date;

        public string Email { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }
        public int TenantId { get; set; }

        [NotRender]
        public bool LockoutEnabled { get; set; }

        [NotRender]
        public DateTime? LockoutEndDateUtc { get; set; }
    }
}

