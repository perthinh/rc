﻿using System;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.Host.Models
{
    public class TemplateEmailPreview : ICustomMappings
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [AllowHtml]
        public string Body { get; set; }
        public bool IsDefault { get; set; }
        public DateTime ModifiedDate { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<TemplateEmailPreview, TemplateEmail>()
                .ForMember(x => x.Active, opt => opt.MapFrom(x => x.IsDefault));
            configuration.CreateMap<TemplateEmail, TemplateEmailPreview>()
                .ForMember(x => x.IsDefault, opt => opt.MapFrom(x => x.Active))
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate));
            configuration.CreateMap<TemplateEmailPreview, EditTemplateEmail>();
        }
    }
}
