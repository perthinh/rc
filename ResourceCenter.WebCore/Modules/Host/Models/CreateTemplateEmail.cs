﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Host.Models
{
    public class CreateTemplateEmail : ICustomMappings
    {
        [HtmlAttr("style", "width:300px"), Required]
        public ETemplateName Name { get; set; }

        [UIHint("HtmlEditor")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsDefault { get; set; } = true;
        
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateTemplateEmail, TemplateEmail>()
                .ForMember(x => x.Name, opt => opt.ResolveUsing(x => x.Name.ToString()))
                .ForMember(x => x.Active, opt => opt.MapFrom(x => x.IsDefault));
        }
    }
}
