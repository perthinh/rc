﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Host.Models;

namespace ResourceCenter.WebCore.Modules.Host.Services
{
    public interface ITemplateEmailService
    {
        Task<List<TemplateEmailPreview>> GetList();
        Task<TemplateEmail> Get(int id);
        Task<TemplateEmail> GetByName(string name);
        Task<TemplateEmail> Create(CreateTemplateEmail model);
        Task<TemplateEmail> Update(EditTemplateEmail model);
        Task<TemplateEmail> Delete(TemplateEmailPreview model);
    }

    public class TemplateEmailService : ITemplateEmailService
    {
        private readonly IUnitOfWork _unitOfWork;

        private TemplateEmailRepository TemplateEmailRepo => _unitOfWork.Repo<TemplateEmailRepository>();

        public TemplateEmailService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<TemplateEmailPreview>> GetList()
        {
            return (await TemplateEmailRepo.GetAllAsync()).MapToList<TemplateEmailPreview>();
        }

        public async Task<TemplateEmail> Get(int id)
        {
            return await TemplateEmailRepo.GetAsync(id);
        }

        public async Task<TemplateEmail> GetByName(string name)
        {
            return await TemplateEmailRepo.Queryable().FirstOrDefaultAsync(x => x.Active && x.Name.Equals(name));
        }

        public async Task<TemplateEmail> Create(CreateTemplateEmail model)
        {
            var entity = model.MapTo<TemplateEmail>();

            TemplateEmailRepo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            return entity;
        }

        public async Task<TemplateEmail> Update(EditTemplateEmail model)
        {
            var entity = await TemplateEmailRepo.GetAsync(model.Id);
            model.MapTo(entity);

            await _unitOfWork.SaveChangesAsync();
            return entity;
        }

        public async Task<TemplateEmail> Delete(TemplateEmailPreview model)
        {
            var entity = await TemplateEmailRepo.GetAsync(model.Id);
            TemplateEmailRepo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }
    }
}
