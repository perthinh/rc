﻿using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.Host.Models;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Modules.Host.Services
{
    public interface IHostUserService
    {
        IQueryable<HostUserPreview> GetList();
        IQueryable<HostUserPreview> GetDistributorUsers(int tenantId);
        Task<IdentityResult> Delete(HostUserPreview model);
    }

    public class ManageUserService : UserService, IHostUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleService _roleService;

        private UserRepository UserRepo => _unitOfWork.Repo<UserRepository>();

        public ManageUserService(
            IUnitOfWork unitOfWork,
            ITemplateEmailSender emailSender,
            IRoleService roleService,
            UserManager userManager) : base(emailSender, userManager)
        {
            _unitOfWork = unitOfWork;
            _roleService = roleService;
        }

        public IQueryable<HostUserPreview> GetList()
        {
            var rolesExcept = _roleService.GetRoleIds(ERole.Host, ERole.User);
            return UserRepo.Queryable()
                        .Where(x => x.Roles.All(y => rolesExcept.All(z => z != y.RoleId)))
                        .ProjectTo<HostUserPreview>();
        }

        public IQueryable<HostUserPreview> GetDistributorUsers(int tenantId)
        {
            var roleId = _roleService.GetRoleId(ERole.User);
            return UserRepo.Queryable()
                           .Where(x => x.Roles.Any(y => y.RoleId == roleId) && x.TenantId == tenantId)
                           .ProjectTo<HostUserPreview>();
        }

        public async Task<IdentityResult> Delete(HostUserPreview model)
        {
            var user = await UserManager.FindByIdAsync(model.Id);
            var result = await UserManager.DeleteAsync(user);

            return result;
        }
    }
}
