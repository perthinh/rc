﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Modules.CompanyAttributes.Models;

namespace ResourceCenter.WebCore.Modules.CompanyAttributes.Services
{
    public interface ICompanyAttributeService
    {
        IQueryable<CompanyAttributeModel> GetList();
        Task<CompanyAttribute> Create(CompanyAttributeModel model);
        Task<CompanyAttribute> Update(CompanyAttributeModel model);
        Task<CompanyAttribute> Delete(CompanyAttributeModel model);
    }

    public class CompanyAttributeService : ICompanyAttributeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private CompanyAttributeRepository Repo => _unitOfWork.Repo<CompanyAttributeRepository>();

        public CompanyAttributeService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<CompanyAttributeModel> GetList()
        {
            return Repo.Queryable()
                .Include(x => x.UpdatedUser)
                .ProjectTo<CompanyAttributeModel>();
        }

        public async Task<CompanyAttribute> Create(CompanyAttributeModel model)
        {
            var entity = model.MapTo<CompanyAttribute>();
            Repo.Insert(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<CompanyAttribute> Update(CompanyAttributeModel model)
        {
            var entity = await Repo.GetAsync(model.Id);
            model.MapTo(entity);
            await _unitOfWork.SaveChangesAsync();

            entity.MapTo(model);
            return entity;
        }

        public async Task<CompanyAttribute> Delete(CompanyAttributeModel model)
        {
            var entity = await Repo.GetAsync(model.Id);
            Repo.Delete(entity);
            await _unitOfWork.SaveChangesAsync();
            return entity;
        }
    }
}