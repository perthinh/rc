﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using Core.Web.MetadataModel.Attributes;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Modules.CompanyAttributes.Models
{
    public class CompanyAttributeModel : ICustomMappings
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string AttributeName { get; set; }
        public bool IsMandatory { get; set; }

        [UIOptions(Deferred = false)]
        public int SortOrder { get; set; }

        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CompanyAttributeModel, CompanyAttribute>();

            configuration.CreateMap<CompanyAttribute, CompanyAttributeModel>()
                .ForMember(x => x.ModifiedDate, opt => opt.MapFrom(x => x.UpdatedDate))
                .ForMember(x => x.ModifiedBy, opt => opt.MapFrom(x => x.UpdatedUser.UserName));
        }
    }
}
