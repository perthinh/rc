﻿using System.Threading.Tasks;
using Core.Exceptions;
using ResourceCenter.DataAccess.Repositories;

namespace ResourceCenter.WebCore.Commands
{
    public static class DeleteTranslationChecker
    {
        public static async Task Execute<T>(T repository, int parentId) where T : class, ITranslationRepository
        {
            var count = await repository.CountTranslations(parentId);
            if (count == 1)
            {
                throw new BusinessException("This entry must have at least one translation");
            }
        }
    }
}
