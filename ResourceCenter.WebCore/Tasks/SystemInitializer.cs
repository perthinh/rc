﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using Core.Tasks;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.AspNetIdentity.Extensions;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceCenter.WebCore.Tasks
{
    public class SystemInitializer : OrderedTask, IRunAtInit
    {
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IUnitOfWork _unitOfWork;
        private TenantRepository TenantRepo => _unitOfWork.Repo<TenantRepository>();

        public SystemInitializer(RoleManager roleManager, UserManager userManager, IUnitOfWork unitOfWork)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public async void Execute()
        {
            await CreateDefaultTenant();
            await CreateRoles();
            await CreateUserHost();
        }

        private async Task CreateRoles()
        {
            foreach (var roleName in Enum.GetNames(typeof(ERole)))
            {
                await _roleManager.CreateIfNotExists(roleName);
            }
        }

        private async Task CreateUserHost()
        {
            var userName = ERole.Host.ToString().ToLower();
            if (_unitOfWork.Repo<UserRepository>().AsNoFilter().Any(x => x.UserName == userName)) return;

            var user = new User
            {
                UserName = userName,
                Email = "cuongdo926@gmail.com",
                TenantId = Tenant.DefaultId
            };

            var result = await _userManager.CreateAsync(user, "zxcvbn");
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, ERole.Host);
            }
        }

        private async Task CreateDefaultTenant()
        {
            var tenant = TenantRepo.GetDefault();
            if (tenant == null)
            {
                var sqlQueryRepo = _unitOfWork.Repo<SqlQueryRepository>();

                _unitOfWork.BeginTransaction();
                sqlQueryRepo.ExecuteSqlCommand("SET IDENTITY_INSERT [User] ON");

                tenant = new Tenant
                {
                    Id = Tenant.DefaultId,
                    Name = "Default",
                    IsActive = true
                };

                TenantRepo.Insert(tenant);
                await _unitOfWork.SaveChangesAsync();

                sqlQueryRepo.ExecuteSqlCommand("SET IDENTITY_INSERT [User] OFF");

                _unitOfWork.Commit();
            }
        }
    }
}
