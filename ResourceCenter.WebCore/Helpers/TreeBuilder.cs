﻿using System.Collections.Generic;
using System.Linq;
using ResourceCenter.WebCore.Models;

namespace ResourceCenter.WebCore.Helpers
{
    public static class TreeBuilder
    {
        public static IList<T> BuildFlat<T>(IList<T> source) where T : class, IFlatNode
        {
            return BuildFlatRecursive(new List<T>(), source, null, 0);
        }

        private static IList<T> BuildFlatRecursive<T>(IList<T> result, IList<T> allItems, T parent, int level) where T : class, IFlatNode
        {
            var children = allItems
                .Where(x => x.ParentId == parent?.Id)
                .OrderBy(x => x.SortOrder)
                .ToList();
            if (parent != null)
            {
                parent.HasChildren = children.Any();
            }

            foreach (var child in children)
            {
                child.Level = level;
                result?.Add(child);
                BuildFlatRecursive(result, allItems, child, level + 1);
            }
            return result;
        }
    }
}
