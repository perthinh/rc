﻿using System;
using System.Collections.Generic;
using System.Web;
using Core.Exceptions;
using Core.Extensions;
using Newtonsoft.Json;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Models;

namespace ResourceCenter.WebCore.Helpers
{
    public static class AssetLeadHelper
    {
        private const string KeyAssetId = "__AssetId";
        private const string KeyFormId = "__FormId";
        private static readonly string[] IgnoreKeys = { KeyAssetId, KeyFormId };

        public static string Serialize(DownloadModel model, HttpRequestBase request, List<FormField> fields)
        {
            var dict = new Dictionary<string, object>();
            foreach (var item in fields)
            {
                var value = request[item.FieldName].ChangeTypeTo<string>(string.Empty);
                if (item.IsRequired && value.Length == 0)
                {
                    throw new BusinessException("Invalid lead information");
                }

                switch (item.DataType)
                {
                    case EFieldDataType.CheckBox:
                        dict.Add(item.FieldName, value.StartsWith("t", StringComparison.OrdinalIgnoreCase));
                        break;
                    default:
                        dict.Add(item.FieldName, value.Truncate(item.DataType.MaxLength()));
                        break;
                }
            }

            dict[KeyAssetId] = model.AssetId;
            dict[KeyFormId] = model.FormId;

            return JsonConvert.SerializeObject(dict);
        }

        public static Dictionary<string, string> Deserialize(AssetDownload entity)
        {
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(entity.LeadContent);
            foreach (var key in IgnoreKeys)
            {
                result.Remove(key);
            }
            return result;
        }
    }
}
