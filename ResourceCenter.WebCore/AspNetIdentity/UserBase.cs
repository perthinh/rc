using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Repositories;

namespace ResourceCenter.WebCore.AspNetIdentity
{
    public abstract class UserBase
    {
        private readonly IUnitOfWork _unitOfWork;

        protected UserManager UserManager;

        public IIdentity Identity { get; set; }

        public LoggedInUser Info => LoggedInUser;
        public int Id => Info.Id;

        public IList<string> Roles => LoggedInUser.Roles;

        public bool IsAuthenticated => Identity.IsAuthenticated;

        protected abstract LoggedInUser LoggedInUser { get; set; }

        protected UserBase(UserManager userManager, IUnitOfWork unitOfWork)
        {
            UserManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public bool IsInRole(params ERole[] roles)
        {
            return roles.Any(role => LoggedInUser.Roles.Contains(role.ToString()));
        }

        protected LoggedInUser CreateLoggedInUser(string userName = null)
        {
            var user = _unitOfWork.Repo<UserRepository>()
                .AsNoFilter()
                .Include(x => x.Tenant)
                .SingleOrDefault(x => x.UserName == (userName ?? Identity.Name));
            var loggedInUser = new LoggedInUser();
            if (user != null)
            {
                loggedInUser.Roles = UserManager.GetRoles(user.Id);
                user.MapTo(loggedInUser);
            }
            return loggedInUser;
        }
    }
}