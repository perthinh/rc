﻿using ResourceCenter.DataAccess.Enums;
using System.Collections.Generic;
using System.Security.Principal;

namespace ResourceCenter.WebCore.AspNetIdentity
{
    public interface ICurrentUser
    {
        int Id { get; }
        LoggedInUser Info { get; }
        IList<string> Roles { get; }
        bool IsInRole(params ERole[] roles);
        bool IsAuthenticated { get; }
        IIdentity Identity { get; set; }
        void Refresh();
    }
}