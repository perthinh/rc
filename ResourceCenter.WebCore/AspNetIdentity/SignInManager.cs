using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Entities;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ResourceCenter.WebCore.AspNetIdentity
{
    public class SignInManager : SignInManager<User, int>
    {
        public SignInManager(UserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((UserManager)UserManager);
        }
    }
}