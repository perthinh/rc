using ResourceCenter.DataAccess.Entities;
using System.Collections.Generic;
using AutoMapper;
using Core.AutoMapper;

namespace ResourceCenter.WebCore.AspNetIdentity
{
    public class LoggedInUser : ICustomMappings
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public IList<string> Roles { get; set; }

        public LoggedInUser()
        {
            Roles = new List<string>();
        }

        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<User, LoggedInUser>()
                .ForMember(x => x.Roles, opt => opt.Ignore());
        }
    }
}