using Core.Web.Caching;
using ResourceCenter.DataAccess.AspNetIdentity;
using System.Web;
using Core.DataAccess.Uow;

namespace ResourceCenter.WebCore.AspNetIdentity
{
    public class CurrentUser : UserBase, ICurrentUser
    {
        private readonly ISessionCache _sessionCache;
        private const string CurrentUserKey = "__CurrentUser__";

        public CurrentUser(UserManager userManager, IUnitOfWork unitOfWork, ISessionCache sessionCache)
            : base(userManager, unitOfWork)
        {
            _sessionCache = sessionCache;
            Identity = HttpContext.Current?.User?.Identity;
        }

        public void Refresh()
        {
            _sessionCache.Remove(CurrentUserKey);
        }

        protected override LoggedInUser LoggedInUser
        {
            get
            {
                return _sessionCache.GetOrAdd(CurrentUserKey, () => CreateLoggedInUser());
            }
            set
            {
                _sessionCache.Set(CurrentUserKey, value);
            }
        }
    }
}