using AutoMapper;
using System.Web.Mvc;
using ResourceCenter.WebCore.Services;

namespace ResourceCenter.WebCore.Resolvers
{
    public class DefaultLanguageResolver : IValueResolver<object, object, string>
    {
        public string Resolve(object source, object destination, string destMember, ResolutionContext context)
        {
            return DependencyResolver.Current.GetService<IUserLanguage>().Default.Name;
        }
    }
}