using AutoMapper;
using ResourceCenter.WebCore.AspNetIdentity;
using System.Web.Mvc;

namespace ResourceCenter.WebCore.Resolvers
{
    public class CurrentUserResolver : IValueResolver<object, object, int>
    {
        public int Resolve(object source, object destination, int destMember, ResolutionContext context)
        {
            return DependencyResolver.Current.GetService<ICurrentUser>().Id;
        }
    }
}