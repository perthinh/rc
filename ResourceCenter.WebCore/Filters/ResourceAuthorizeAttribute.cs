﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Core.Extensions;
using Core.Web.Caching;
using Core.Web.Mvc.Ajax;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Resellers.Services;

namespace ResourceCenter.WebCore.Filters
{
    public class ResourceAuthorizeAttribute : AuthorizeAttribute
    {
        private static IContentService ContentService => DependencyResolver.Current.GetService<IContentService>();
        private static IPerRequestCache PerRequestCache => DependencyResolver.Current.GetService<IPerRequestCache>();

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            try
            {
                var tenantId = httpContext.Request.RequestContext.TenantId().ChangeTypeTo<int>();
                var resellerId = httpContext.Request.RequestContext.ResellerId().ChangeTypeTo<int>();
                var partnerTypeId = ContentService.GetSubscribedPartnerType(tenantId, resellerId).Result;
                if (!partnerTypeId.HasValue) return false;

                PerRequestCache.SetRequestPartnerTypeId(partnerTypeId.Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Response.StatusCode = 403;
                filterContext.Result = new JsonResult
                {
                    Data = AjaxResult.Error().WithMessages("Access Denied").ToResult(),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    ["action"] = "Index",
                    ["controller"] = "Public",
                    ["area"] = "Resources"
                });
            }

        }
    }
}
