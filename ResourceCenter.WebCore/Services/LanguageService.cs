﻿using CacheManager.Core;
using Core.DataAccess.Uow;
using Core.Localization;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ResourceCenter.WebCore.Services
{
    public interface ILanguageService
    {
        IList<LanguageInfo> GetAll();
        IList<LanguageInfo> GetUserLanguages(int userId);
        Task<IList<LanguageInfo>> GetTenantLanguages(int tenantId);
        LanguageInfo GetDefault();
    }

    public class LanguageService : ILanguageService
    {
        private readonly ICacheManager<object> _cache;
        private readonly IUnitOfWork _unitOfWork;
        private LanguageRepository LangRepo => _unitOfWork.Repo<LanguageRepository>();

        public LanguageService(ICacheManager<object> cache, IUnitOfWork unitOfWork)
        {
            _cache = cache;
            _unitOfWork = unitOfWork;
        }

        public IList<LanguageInfo> GetAll()
        {
            var dict = GetLanguageDict();
            return dict.Values.OrderBy(x => x.DisplayName)
                       .Select(x => new LanguageInfo(x.Name, x.DisplayName, GetIcon(dict[x.Name])))
                       .ToList();
        }

        public IList<LanguageInfo> GetUserLanguages(int userId)
        {
            var languageCodes = LangRepo.AsNoFilter()
                .Where(x => x.CreatedBy == userId)
                .ToList();

            return ToListLanguageInfo(languageCodes);
        }

        public async Task<IList<LanguageInfo>> GetTenantLanguages(int tenantId)
        {
            var languageCodes = await LangRepo.AsNoFilter()
                .Where(x => x.TenantId == tenantId)
                .ToListAsync();

            return ToListLanguageInfo(languageCodes);
        }

        private IList<LanguageInfo> ToListLanguageInfo(IEnumerable<Language> languages)
        {
            var dict = GetLanguageDict();
            return languages
                .Select(item => new LanguageInfo(item.LanguageCode,
                        dict[item.LanguageCode].DisplayName,
                        GetIcon(dict[item.LanguageCode]),
                        item.IsDefault))
                .OrderBy(x => x.DisplayName)
                .ToList();
        }

        private IDictionary<string, CultureInfo> GetLanguageDict()
        {
            const string ignoreLangCode = "iv";
            return _cache.GetOrAdd("__LanguageService.GetLanguageDict", key =>
            {
                return CultureInfo.GetCultures(CultureTypes.AllCultures)
                    .Where(x => x.TwoLetterISOLanguageName != ignoreLangCode)
                    .ToDictionary(x => x.Name);
            }) as IDictionary<string, CultureInfo>;
        }

        public LanguageInfo GetDefault()
        {
            var dict = GetLanguageDict();
            const string defaultLangCode = "en";
            return new LanguageInfo(defaultLangCode, dict[defaultLangCode].DisplayName, GetIcon(dict[defaultLangCode]));
        }

        private string GetIcon(CultureInfo culture)
        {
            return $"flag.flag-{culture.TwoLetterISOLanguageName}";
        }
    }
}