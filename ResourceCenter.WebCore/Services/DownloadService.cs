﻿using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.Exceptions;
using Core.Extensions;
using Newtonsoft.Json;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Helpers;
using ResourceCenter.WebCore.Models;
using Z.EntityFramework.Plus;

namespace ResourceCenter.WebCore.Services
{
    public interface IDownloadService
    {
        Task<DownloadResult> DownloadAsset(DownloadModel model, HttpRequestBase request);
    }

    public class DownloadService : IDownloadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private AssetRepository AssetRepo => _unitOfWork.Repo<AssetRepository>();
        private FormFieldRepository FieldRepo => _unitOfWork.Repo<FormFieldRepository>();
        private AssetDownloadRepository AssetDownloadRepo => _unitOfWork.Repo<AssetDownloadRepository>();
        private AssetCounterRepository AssetCounterRepo => _unitOfWork.Repo<AssetCounterRepository>();

        public DownloadService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<DownloadResult> DownloadAsset(DownloadModel model, HttpRequestBase request)
        {
            var asset = await AssetRepo.AsNoFilter().SingleOrDefaultAsync(x => x.Id == model.AssetId);
            if (asset == null)
            {
                return DownloadResult.NotFound;
            }

            await AddDownload(asset, model, request);

            var filePath = asset.FilePath.ToAbsolutePath();
            if (!File.Exists(filePath))
            {
                return DownloadResult.NotFound;
            }

            return new DownloadResult(File.ReadAllBytes(filePath), asset.FileName);
        }

        private async Task AddDownload(Asset asset, DownloadModel model, HttpRequestBase request)
        {
            var assetDownload = model.MapTo<AssetDownload>();
            assetDownload.TenantId = asset.TenantId;
            assetDownload.IPAddress = request.UserHostAddress;

            if (asset.IsGated)
            {
                assetDownload.LeadContent = await CollectLead(model, request);
                assetDownload.IsLead = true;
            }

            AssetDownloadRepo.Insert(assetDownload);
            await _unitOfWork.SaveChangesAsync();

            await AssetCounterRepo.Queryable()
                .Where(x => x.Id == model.AssetId)
                .UpdateAsync(x => new AssetCounter
                {
                    DownloadCount = x.DownloadCount + 1
                });
        }

        private async Task<string> CollectLead(DownloadModel model, HttpRequestBase request)
        {
            if (!model.FormId.HasValue)
            {
                throw new BusinessException("Invalid lead information");
            }

            var formId = model.FormId.Value;
            var fields = await FieldRepo
                .AsNoFilter()
                .Where(x => x.FormId == formId)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();

            return AssetLeadHelper.Serialize(model, request, fields);
        }

        
    }
}