using Core.Configuration;
using Core.IO;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Extensions;
using System.IO;
using System.Web;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Services
{
    public interface IUploadService
    {
        string Upload(HttpPostedFileBase file);
        string UploadThumbnail(HttpPostedFileBase file);
        string GetUploadFolder();
        string GetThumbnailFolder();
        string GetImageFolder();
    }

    public class UploadService : IUploadService
    {
        private readonly ICurrentUser _currentUser;
        private readonly ISettingManager _setting;

        public UploadService(ICurrentUser currentUser, ISettingManager setting)
        {
            _currentUser = currentUser;
            _setting = setting;
        }

        public string Upload(HttpPostedFileBase file)
        {
            return UploadFile(GetUploadFolder(), file);
        }

        public string UploadThumbnail(HttpPostedFileBase file)
        {
            return UploadFile(GetThumbnailFolder(), file);
        }

        public string GetUploadFolder()
        {
            return string.Format("{0}/{1}", _setting.UploadFolder(), GetFolderName());
        }

        public string GetThumbnailFolder()
        {
            return string.Format("{0}/{1}/thumbnail", _setting.UploadFolder(), GetFolderName());
        }

        public string GetImageFolder()
        {
            return string.Format("{0}/{1}/image", _setting.UploadFolder(), GetFolderName());
        }

        private string UploadFile(string path, HttpPostedFileBase file)
        {
            var relativePath = $"{path}/{file.ToUniqueFileName()}";
            var absolutePath = relativePath.ToAbsolutePath();
            DirectoryHelper.CreateIfNotExists(Path.GetDirectoryName(absolutePath));
            file.SaveAs(absolutePath);
            return relativePath;
        }

        private string GetFolderName()
        {
            return _currentUser.IsInRole(ERole.User, ERole.Distributor)
                ? _currentUser.Info.TenantName
                : _currentUser.Info.UserName;
        }
    }
}