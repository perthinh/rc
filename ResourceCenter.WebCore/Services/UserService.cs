﻿using System.Threading.Tasks;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.WebCore.Services.Emails;

namespace ResourceCenter.WebCore.Services
{
    public interface IUserService
    {
        Task<User> GetUser(int id);
        Task<IdentityResult> ToggleIsLocked(int id);
        Task<IdentityResult> ResetPassword(int id);
    }

    public class UserService : IUserService
    {
        protected ITemplateEmailSender EmailSender;
        protected UserManager UserManager;

        public UserService(
            ITemplateEmailSender emailSender,
            UserManager userManager)
        {
            EmailSender = emailSender;
            UserManager = userManager;
        }

        public async Task<User> GetUser(int id)
        {
            return await UserManager.FindByIdAsync(id);
        }

        public async Task<IdentityResult> ToggleIsLocked(int id)
        {
            var user = await UserManager.FindByIdAsync(id);

            // if user is locked then unlock the user and vice versa
            user?.SetLockState(!user.IsLocked);

            return await UserManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> ResetPassword(int id)
        {
            var user = await UserManager.FindByIdAsync(id);

            var password = Membership.GeneratePassword(12, 1);
            user.PasswordHash = UserManager.PasswordHasher.HashPassword(password);

            var result = await UserManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                await EmailSender.SendUserResetPassword(user.Email, password);
            }

            return result;
        }
    }
}
