﻿using System.Linq;
using CacheManager.Core;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Models;

namespace ResourceCenter.WebCore.Services
{
    public interface IRoleService
    {
        CachedRole[] GetAll();
        CachedRole[] GetRoles(params ERole[] roles);
        int[] GetRoleIds(params ERole[] roles);
        int? GetRoleId(ERole role);
    }

    public class RoleService : IRoleService
    {
        private readonly ICacheManager<object> _cache;
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(ICacheManager<object> cache, IUnitOfWork unitOfWork)
        {
            _cache = cache;
            _unitOfWork = unitOfWork;
        }

        public CachedRole[] GetAll()
        {
            return _cache.GetOrAdd("__RoleService.GetAll", _ =>
            {
                var list = _unitOfWork.Repo<RoleRepository>().GetAll().MapToList<CachedRole>();
                return list.ToArray();
            }) as CachedRole[];
        }

        public CachedRole[] GetRoles(params ERole[] roles)
        {
            return !roles.Any() ? GetAll() : GetAll().Where(x => roles.Contains(x.RoleName)).ToArray();
        }

        public int[] GetRoleIds(params ERole[] roles)
        {
            return GetRoles(roles).Select(x => x.Id).ToArray();
        }

        public int? GetRoleId(ERole role)
        {
            return GetRoleIds(role).FirstOrDefault();
        }
    }
}
