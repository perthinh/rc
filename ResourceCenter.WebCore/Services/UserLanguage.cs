﻿using Core.Localization;
using Core.Web.Caching;
using ResourceCenter.WebCore.AspNetIdentity;
using System.Collections.Generic;
using System.Linq;

namespace ResourceCenter.WebCore.Services
{
    public interface IUserLanguage
    {
        IList<LanguageInfo> GetAll();
        LanguageInfo Default { get; }
        string CurrentLanguage { get; set; }
        void Refresh();
    }

    public class UserLanguage : IUserLanguage
    {
        private const string KeyAll = "__UserLanguages_All";
        private const string KeyCurrent = "__UserLanguages_Current";
        private readonly ICurrentUser _currentUser;
        private readonly ISessionCache _sessionCache;
        private readonly ILanguageService _languageService;

        public UserLanguage(ICurrentUser currentUser, ISessionCache sessionCache, ILanguageService languageService)
        {
            _currentUser = currentUser;
            _sessionCache = sessionCache;
            _languageService = languageService;
        }

        public IList<LanguageInfo> GetAll()
        {
            return _sessionCache.GetOrAdd(KeyAll, () => _languageService.GetUserLanguages(_currentUser.Id));
        }

        public LanguageInfo Default => GetAll().FirstOrDefault(x => x.IsDefault) ?? _languageService.GetDefault();

        public string CurrentLanguage
        {
            get { return _sessionCache.GetOrAdd(KeyCurrent, () => Default.Name); }
            set { _sessionCache.Set(KeyCurrent, value); }
        }

        public void Refresh()
        {
            _sessionCache.Remove(KeyAll);
            _sessionCache.Remove(KeyCurrent);
        }
    }
}
