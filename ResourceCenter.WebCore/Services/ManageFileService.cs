﻿using System.Collections.Generic;
using System.IO;
using Core.AutoMapper;
using Core.IO;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.FileManager;

namespace ResourceCenter.WebCore.Services
{
    public interface IManageFileService
    {
        void Upload(UploadedFileModel model);
        IList<FileModel> GetList();
        bool DeleteThumbnail(string fileName);
    }

    public class ManageFileService : IManageFileService
    {
        private readonly IUploadService _uploadService;

        public ManageFileService(IUploadService uploadService)
        {
            _uploadService = uploadService;
        }

        public void Upload(UploadedFileModel model)
        {
            _uploadService.UploadThumbnail(model.FileUpload);
        }

        public bool DeleteThumbnail(string fileName)
        {
            var path = string.Format("{0}/{1}", _uploadService.GetThumbnailFolder(), fileName).ToAbsolutePath();
            return FileHelper.DeleteIfExists(path);
        }

        public IList<FileModel> GetList()
        {
            var root = _uploadService.GetThumbnailFolder();
            var dir = new DirectoryInfo(root.ToAbsolutePath());
            var list = new List<FileModel>();
            if (dir.Exists)
            {
                list = dir.GetFiles().MapToList<FileModel>();
                foreach (var item in list)
                {
                    item.Path = $"{root}/{item.Name}";
                }
            }
            return list;
        }
    }
}