﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Exceptions;
using Core.Net.Mail;
using ResourceCenter.WebCore.Modules.Host.Services;

namespace ResourceCenter.WebCore.Services.Emails
{
    public interface ITemplateEmailSender
    {
        Task SendEmailAsync(ETemplateName template, string to, string subject, IDictionary<string, string> replacements = null);
    }

    public class TemplateEmailSender : ITemplateEmailSender
    {
        private readonly IEmailSender _emailSender;
        private readonly ITemplateEmailService _service;

        public TemplateEmailSender(IEmailSender emailSender, ITemplateEmailService service)
        {
            _emailSender = emailSender;
            _service = service;
        }

        public async Task SendEmailAsync(ETemplateName template, string to, string subject, IDictionary<string, string> replacements = null)
        {
            var body = await GetBody(template);

            if (string.IsNullOrEmpty(body)) throw new BusinessException($"Cannot find email template {template}");

            if (replacements != null)
            {
                foreach (var kvp in replacements)
                {
                    body = body.Replace($"%{kvp.Key}%", kvp.Value);
                }
            }

            await _emailSender.SendAsync(to, subject, body);
        }

        private async Task<string> GetBody(ETemplateName template)
        {
            return (await _service.GetByName(template.ToString()))?.Body;
        }
    }
}

