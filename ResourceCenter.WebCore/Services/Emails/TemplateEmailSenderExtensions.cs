using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ResourceCenter.WebCore.Modules.ManageUser.Models;
using ResourceCenter.WebCore.Modules.Subscriptions.Models;

namespace ResourceCenter.WebCore.Services.Emails
{
    public static class TemplateEmailSenderExtensions
    {
        /// <summary>
        /// Send invitation email to reseller 
        /// </summary>
        public static Task SendResellerInvitation(this ITemplateEmailSender service, string to, Guid code)
        {
            var dict = new Dictionary<string, string>
            {
                { "InvitationCode", code.ToString() }
            };
            return service.SendEmailAsync(ETemplateName.ResellerInvitation, to, "Invitation Reseller", dict);
        }

        /// <summary>
        /// Send a notification email to tenant owner (distributor) when a reseller subscribes content
        /// </summary>
        public static Task SendSubscriberNotification(this ITemplateEmailSender service, string to, string registeredUser)
        {
            var dict = new Dictionary<string, string>
            {
                { "RegisteredUser", registeredUser }
            };
            return service.SendEmailAsync(ETemplateName.NewSubscriberNotification, to, "New Reseller", dict);
        }

        public static Task SendSubscriptionChanged(this ITemplateEmailSender service, ChangedSubscriptionModel model)
        {
            var dict = new Dictionary<string, string>
            {
                { "RegisteredUser", model.Subscriber},
                { "PartnerType", model.NewPartnerType },
                { "Status", model.NewStatus },
            };
            return service.SendEmailAsync(ETemplateName.SubscriptionUpdated, model.EmailTo, "Subscription changed", dict);
        }

        public static Task SendUserCreated(this ITemplateEmailSender service, string email, string username, string password)
        {
            var dict = new Dictionary<string, string>
            {
                { "Username", username},
                { "Password", password },
            };
            return service.SendEmailAsync(ETemplateName.UserCreated, email, "User Created", dict);
        }

        public static Task SendUserEdited(this ITemplateEmailSender service, EditUserModel model)
        {
            var dict = new Dictionary<string, string>
            {
                { "Username", model.UserName},
                { "Password", !string.IsNullOrEmpty(model.Password) ? model.Password : "(No change)" },
                { "FirstName", model.FirstName },
                { "LastName", model.LastName },
                { "MiddleName", model.MiddleName },
                { "Notes", model.Notes },
            };
            return service.SendEmailAsync(ETemplateName.UserEdited, model.Email, "User information changed", dict);
        }

        public static Task SendUserResetPassword(this ITemplateEmailSender service, string email, string password)
        {
            var dict = new Dictionary<string, string>
            {
                { "Password", password },
            };
            return service.SendEmailAsync(ETemplateName.ResetPassword, email, "Password reset", dict);
        }

        public static Task SendForgotPassword(this ITemplateEmailSender service, string email, string requestUrl)
        {
            var dict = new Dictionary<string, string>
            {
                { "RequestUrl", requestUrl },
            };
            return service.SendEmailAsync(ETemplateName.ForgotPassword, email, "Reset your password", dict);
        }
    }
}