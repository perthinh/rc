namespace ResourceCenter.WebCore.Services.Emails
{
    public enum ETemplateName
    {
        ResellerInvitation,
        NewSubscriberNotification,
        SubscriptionUpdated,
        UserCreated,
        UserEdited,
        ResetPassword,
        ForgotPassword
    }
}