﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Core.AutoMapper;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.WebCore.Models
{
    public class DownloadModel : ICustomMappings
    {
        [Required]
        public int AssetId { get; set; }
        public int? FormId { get; set; }
        public int ResellerId { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DownloadModel, AssetDownload>()
                .ForMember(x => x.SubscriberId, opt => opt.MapFrom(x => x.ResellerId));
        }
    }
}