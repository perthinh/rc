using System.Web;

namespace ResourceCenter.WebCore.Models
{
    public class DownloadResult
    {
        public bool HasFile => Data != null;
        public string ContentType { get; }
        public byte[] Data { get; }
        public string FileName { get; }

        public DownloadResult()
        {
        }

        public DownloadResult(byte[] data, string fileName)
        {
            Data = data;
            FileName = fileName;
            ContentType = MimeMapping.GetMimeMapping(fileName);
        }

        public static readonly DownloadResult NotFound = new DownloadResult();
    }
}