﻿using AutoMapper;
using Core.AutoMapper;
using Core.Extensions;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.WebCore.Models
{
    public class CachedRole : ICustomMappings
    {
        public int Id { get; set; }
        public ERole RoleName { get; set; }
        public void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Role, CachedRole>()
                .ForMember(x => x.RoleName, opt => opt.MapFrom(x => x.Name.ToEnum<ERole>()));
        }
    }
}
