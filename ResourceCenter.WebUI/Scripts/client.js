﻿window.onload = function () {

    var el = document.getElementById('_rc_content');

    if (!el) {
        console.log("Not exist iframe content");
        return;
    }

    el.src = el.getAttribute("data-src");
    el.onload = function () {
        el.style.height = el.contentWindow.document.body.scrollHeight + 'px';
    }
}