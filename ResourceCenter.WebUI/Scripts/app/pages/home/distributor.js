/// <reference path="../../../typings/tsd.d.ts" />
var pageCtrl = (function () {
    var docElem = document.documentElement, header, didScroll = false, changeHeaderOn = 200;
    return {
        init: init
    };
    function init() {
        header = $(".navbar-default");
        window.addEventListener("scroll", function (event) {
            if (!didScroll) {
                didScroll = true;
                setTimeout(scrollPage, 250);
            }
        }, false);
    }
    function scrollPage() {
        var sy = scrollY();
        if (sy >= changeHeaderOn) {
            header.addClass("navbar-scroll");
        }
        else {
            header.removeClass("navbar-scroll");
        }
        didScroll = false;
    }
    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }
})();
$(function () {
    $("body").scrollspy({
        target: ".navbar-fixed-top",
        offset: 80
    });
    // Page scrolling feature
    $("a.page-scroll").on("click", function (event) {
        var link = $(this);
        $("html, body").stop().animate({
            scrollTop: $(link.attr("href")).offset().top - 50
        }, 500);
        event.preventDefault();
        $("#navbar").collapse("hide");
    });
    pageCtrl.init();
});
