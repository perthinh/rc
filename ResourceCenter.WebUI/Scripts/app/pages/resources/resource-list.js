/// <reference path="../../../typings/tsd.d.ts" />
var resourceListCtrl = (function () {
    return {
        init: init,
        switchType: switchType
    };
    ///////////////
    function init() {
        $("#btGrid").click(function () { return switchType(false); });
        $("#btList").click(function () { return switchType(true); });
    }
    function switchType(isList) {
        $("#productView").toggleClass("listView", isList);
    }
})();
$(function () {
    resourceListCtrl.init();
});
