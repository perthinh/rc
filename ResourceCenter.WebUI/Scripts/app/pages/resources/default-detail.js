/// <reference path="../../../typings/tsd.d.ts" />
var detailCtrl = (function () {
    var formData;
    var baseUrl;
    var modalInstance;
    return {
        init: init
    };
    ///////////////
    function init(url, data) {
        baseUrl = url;
        formData = data;
        modalInstance = new Modal("Lead Information");
        $("#btDownload").click(loadAssetLeadForm);
    }
    function loadAssetLeadForm() {
        modalInstance.showLoading();
        $.post(baseUrl, formData)
            .done(renderHtmlAndRegisterEvents)
            .fail(function (e) {
            modalInstance.setContent(AjaxHelper.getError(e));
        })
            .always(function () {
            modalInstance.hideLoading();
        });
    }
    function renderHtmlAndRegisterEvents(html) {
        modalInstance.setContent(html);
        var form = modalInstance.findInContent("form").first();
        form.find(".btn.ladda-button").ladda("bind");
        Validator.for(form);
        form.submit(function () {
            setTimeout(function () {
                modalInstance.toggle();
            }, 2000);
        });
    }
})();
