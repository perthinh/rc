/// <reference path="../../../typings/tsd.d.ts" />
var invitationCtrl = (function () {
    return {
        resend: resend
    };
    ///////////////
    function resend(id) {
        if (!confirm("Are you sure you want to resend this email invitation?"))
            return;
        var button = $("#buttonResend" + id);
        button.attr("disabled", "disabled");
        $.post("/Invitation/Resend/", { id: id })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            Notify.success("Resending invitation successfully");
        }).fail(AjaxHelper.fail)
            .always(function () {
            button.removeAttr("disabled");
        });
    }
})();
