/// <reference path="../../../typings/tsd.d.ts" />
var downloadHistoryCtrl = (function () {
    var grid;
    var modalIntance;
    return {
        init: init,
        detail: detail
    };
    ///////////////
    function init() {
        grid = $("#grid").data("kendoGrid");
        modalIntance = new Modal("Lead Content");
        // prevent animation when show/hide modal
        $("#animatedContent").removeClass("animated");
    }
    function detail(e) {
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        modalIntance.toggle();
        modalIntance.showLoading();
        $.post("/Admin/DownloadHistory/Detail", { id: dataItem.Id })
            .done(function (html) {
            modalIntance.setContent(html);
        })
            .fail(function (e) {
            modalIntance.setContent(AjaxHelper.getError(e));
        })
            .always(function () {
            modalIntance.hideLoading();
        });
    }
})();
$(function () {
    downloadHistoryCtrl.init();
});
