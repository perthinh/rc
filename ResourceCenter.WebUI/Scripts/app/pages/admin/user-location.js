/// <reference path="../../../typings/tsd.d.ts" />
var userLocationCtrl = (function () {
    var map;
    var description;
    var location;
    return {
        init: init
    };
    ///////////////
    function init() {
        description = $("#Description");
        location = $("#Location");
        var locationString = location.val().split(",");
        var latitude = null;
        var longitude = null;
        if (locationString.length === 2) {
            latitude = locationString[0], longitude = locationString[1];
        }
        map = new GeoMap(latitude, longitude, description.val());
        map.locationChanged = locationChanged;
    }
    function locationChanged(sender, args) {
        location.val(args.latitude + "," + args.longitude);
        description.val(args.address);
    }
})();
