/// <reference path="../../../typings/tsd.d.ts" />
var dashboardCtrl = (function () {
    return {
        init: init
    };
    ///////////////
    function init(forHost) {
        getDownloadCount({ forToday: false }, "ajaxContent1");
        getDownloadCount({ forToday: false, leadOnly: true }, "ajaxContent2");
        getDownloadCount({ forToday: true, leadOnly: false }, "ajaxContent3");
        getDownloadCount({ forToday: true, leadOnly: true }, "ajaxContent4");
        if (forHost) {
            getLatestDistributors();
        }
        else {
            getLatestResellers();
        }
        getLatestAssets();
    }
    function getDownloadCount(data, contentId) {
        $.get("/Admin/Dashboard/GetDownloadCount", data, function (response) {
            if (AjaxHelper.handleError(response))
                return;
            $("#" + contentId).html("<h1 class=\"no-margins\">" + response.Data + "</h1>");
        }).fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function getLatestDistributors() {
        $.post("/Admin/Dashboard/LatestDistributors").done(function (html) {
            $("#ajaxLatest1").html(html);
        }).fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function getLatestResellers() {
        $.post("/Admin/Dashboard/LatestResellers").done(function (html) {
            $("#ajaxLatest1").html(html);
        }).fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
    function getLatestAssets() {
        $.post("/Admin/Dashboard/LatestAssets").done(function (html) {
            $("#ajaxLatest2").html(html);
        }).fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
})();
