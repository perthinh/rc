/// <reference path="../../../typings/tsd.d.ts" />
var assetCtrl = (function () {
    var ddlForm;
    var chbIsGated;
    return {
        init: init
    };
    ///////////////
    function init() {
        ddlForm = $("#FormId").data("kendoDropDownList");
        chbIsGated = $("#IsGated");
        chbIsGated.on("change", function () {
            ddlForm.enable(this.checked);
        });
        chbIsGated.trigger("change");
        Validator.default();
    }
})();
$(function () {
    assetCtrl.init();
});
