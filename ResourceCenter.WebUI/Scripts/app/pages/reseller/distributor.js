/// <reference path="../../../typings/tsd.d.ts" />
var SubscriptionStatus;
(function (SubscriptionStatus) {
    SubscriptionStatus[SubscriptionStatus["Pending"] = 0] = "Pending";
    SubscriptionStatus[SubscriptionStatus["Active"] = 1] = "Active";
    SubscriptionStatus[SubscriptionStatus["Suspended"] = 2] = "Suspended";
    SubscriptionStatus[SubscriptionStatus["Cancelled"] = 3] = "Cancelled";
})(SubscriptionStatus || (SubscriptionStatus = {}));
var subscriptionCtrl = (function () {
    var grid;
    return {
        init: init,
        changeStatus: changeStatus
    };
    function init() {
        grid = $("#grid").data("kendoGrid");
    }
    ///////////////
    function changeStatus(e) {
        if (!confirm("Are you sure you want to change this subscription status?"))
            return;
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var status = dataItem.Status;
        switch (dataItem.Status) {
            case SubscriptionStatus.Active:
                status = SubscriptionStatus.Suspended;
                break;
            case SubscriptionStatus.Suspended:
                status = SubscriptionStatus.Active;
                break;
        }
        $.post("/Reseller/Distributor/ChangeStatus", { id: dataItem.Id, status: status })
            .done(function (data) {
            if (AjaxHelper.handleError(data))
                return;
            Notify.success("Changing status successfully");
            dataItem.set("Status", data.Data);
        })
            .fail(AjaxHelper.fail);
    }
})();
$(function () {
    subscriptionCtrl.init();
});
