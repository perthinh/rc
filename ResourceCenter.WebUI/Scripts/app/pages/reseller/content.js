/// <reference path="../../../typings/tsd.d.ts" />
var showAssetCtrl = (function () {
    var btShow;
    var form;
    var ajaxContent;
    var validator;
    return {
        init: init
    };
    ///////////////
    function init() {
        form = $("#form");
        ajaxContent = $("#ajaxContent");
        validator = Validator.for(form);
        btShow = $("#btShow");
        form.submit(function (event) {
            event.preventDefault();
            if (!validator.isValid)
                return;
            getResourceLink();
        });
        // Init clipboard control, use for Copy To Clipboard
        var clipboard = new Clipboard('.btn');
        clipboard.on('success', function () {
            Notify.success("Copied");
        });
        clipboard.on('error', function () {
            Notify.error("Cannot copied");
        });
    }
    function getResourceLink() {
        var ladda = btShow.ladda();
        ladda.ladda("start");
        ajaxContent.hide();
        $.post("/Reseller/ContentAsset/Resource", form.serialize())
            .done(function (data) {
            ajaxContent.fadeIn();
            ajaxContent.html(data);
        }).always(function () {
            ladda.ladda("stop");
        });
    }
})();
$(function () {
    showAssetCtrl.init();
});
