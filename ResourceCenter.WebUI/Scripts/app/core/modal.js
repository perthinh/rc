var Modal = (function () {
    function Modal(title, selector) {
        if (selector === void 0) { selector = "#modal"; }
        this.selector = selector;
        this.modal = selector instanceof jQuery ? selector : $(this.selector);
        this.modalTitle = this.modal.find(".modal-title");
        this.modalContent = this.modal.find(".modal-body #ajaxContent");
        this.spinner = this.modal.find(".modal-body .sk-spinner");
        this.setTitle(title);
    }
    Modal.prototype.setLoading = function (value) {
        if (value) {
            this.setContent("");
            this.spinner.show();
        }
        else {
            this.spinner.hide();
        }
    };
    Modal.prototype.showLoading = function () {
        this.setLoading(true);
    };
    Modal.prototype.hideLoading = function () {
        this.setLoading(false);
    };
    Modal.prototype.setTitle = function (value) {
        this.modalTitle.html(value);
    };
    Modal.prototype.setContent = function (value) {
        this.modalContent.html(value);
    };
    Modal.prototype.toggle = function () {
        this.modal.modal("toggle");
    };
    Modal.prototype.findInContent = function (selector) {
        return this.modalContent.find(selector);
    };
    return Modal;
}());
