var defaultZoom = 17;
var GeoMap = (function () {
    function GeoMap(latitude, longitude, description) {
        if (description === void 0) { description = ""; }
        this.init(latitude, longitude, description);
    }
    GeoMap.prototype.init = function (latitude, longitude, description) {
        var _this = this;
        this.geocoder = new google.maps.Geocoder();
        this.map = new google.maps.Map(document.querySelector("#location_map"), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: defaultZoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        // Create the search box and link it to the UI element.
        var mapInput = document.querySelector("#location_map_input");
        var autocomplete = new google.maps.places.Autocomplete(mapInput);
        autocomplete.bindTo("bounds", this.map);
        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapInput);
        this.infoWindow = new google.maps.InfoWindow();
        this.marker = new google.maps.Marker({
            map: this.map,
            draggable: true
        });
        // Event Click for marker
        this.marker.addListener("click", function () {
            if (_this.marker.getAnimation() !== null) {
                _this.marker.setAnimation(null);
            }
            else {
                _this.marker.setAnimation(google.maps.Animation.BOUNCE);
            }
            _this.infoWindow.open(_this.map, _this.marker);
        });
        // Event drag makrker
        google.maps.event.addListener(this.marker, "dragend", function () {
            var location = _this.marker.getPosition();
            _this.setCurrentPosition(location.lat(), location.lng());
        });
        // Event for Search on map
        autocomplete.addListener("place_changed", function () {
            _this.infoWindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }
            if (place.geometry.viewport) {
                _this.map.fitBounds(place.geometry.viewport);
            }
            else {
                _this.map.setCenter(place.geometry.location);
                _this.map.setZoom(defaultZoom);
            }
            _this.setCurrentPosition(place.geometry.location.lat(), place.geometry.location.lng(), place.formatted_address);
        });
        if (latitude && longitude) {
            this.setCurrentPosition(latitude, longitude, description);
        }
        else {
            // Using HTML5 geolocation to get current client's location.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    _this.setCurrentPosition(position.coords.latitude, position.coords.longitude);
                }, function () {
                    _this.handleLocationError(true, _this.infoWindow, _this.map.getCenter());
                });
            }
            else {
                // Browser doesn't support Geolocation
                this.handleLocationError(false, this.infoWindow, this.map.getCenter());
            }
        }
    };
    GeoMap.prototype.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation
            ? "Error: The Geolocation service failed."
            : "Error: Your browser doesn't support geolocation.");
    };
    GeoMap.prototype.setCurrentPosition = function (latitude, longitude, description) {
        var _this = this;
        if (description === void 0) { description = null; }
        var latLng = new google.maps.LatLng(latitude, longitude);
        this.geocoder.geocode({ latLng: latLng }, function (responses) {
            if (responses && responses.length > 0) {
                var address = responses[0].formatted_address;
                if (description)
                    address = description;
                _this.onLocationChanged({
                    address: address,
                    latitude: latitude,
                    longitude: longitude
                });
                _this.marker.setPosition(latLng);
                _this.infoWindow.setContent(address);
                _this.infoWindow.open(_this.map, _this.marker);
                _this.map.setCenter(latLng);
            }
            else {
                console.log("Cannot determine address at this location.");
            }
        });
    };
    GeoMap.prototype.onLocationChanged = function (args) {
        if (this.locationChanged) {
            this.locationChanged(this.map, args);
        }
    };
    return GeoMap;
}());
