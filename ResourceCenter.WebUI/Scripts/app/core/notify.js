var Notify = (function () {
    function Notify() {
        toastr.options = {
            closeButton: true,
            debug: false,
            progressBar: true,
            preventDuplicates: false,
            positionClass: "toast-bottom-right",
            onclick: null,
            showDuration: 400,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
        };
    }
    Notify.show = function (type, message, title) {
        if (title === void 0) { title = ""; }
        Notify.instance.show(type, message, title);
    };
    Notify.success = function (message, title) {
        if (title === void 0) { title = ""; }
        Notify.instance.success(message, title);
    };
    Notify.warning = function (message, title) {
        if (title === void 0) { title = ""; }
        Notify.instance.warning(message, title);
    };
    Notify.info = function (message, title) {
        if (title === void 0) { title = ""; }
        Notify.instance.info(message, title);
    };
    Notify.error = function (message, title) {
        if (title === void 0) { title = ""; }
        Notify.instance.error(message, title);
    };
    Notify.prototype.show = function (type, message, title) {
        if (title === void 0) { title = ""; }
        toastr[type](message, title);
    };
    Notify.prototype.success = function (message, title) {
        if (title === void 0) { title = ""; }
        toastr.success(message, title);
    };
    Notify.prototype.warning = function (message, title) {
        if (title === void 0) { title = ""; }
        toastr.warning(message, title);
    };
    Notify.prototype.info = function (message, title) {
        if (title === void 0) { title = ""; }
        toastr.info(message, title);
    };
    Notify.prototype.error = function (message, title) {
        if (title === void 0) { title = ""; }
        toastr.error(message, title);
    };
    Notify.instance = new Notify();
    return Notify;
}());
