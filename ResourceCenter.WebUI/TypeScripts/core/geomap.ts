﻿declare var google;
const defaultZoom = 17;

interface ILocation {
    address: any;
    latitude: any;
    longitude: any;
}

class GeoMap {
    private map: any;
    private geocoder: any;
    private infoWindow: any;
    private marker: any;

    locationChanged: ITypeDelegate<ILocation>;

    constructor(latitude: any, longitude: any, description = "") {
        this.init(latitude, longitude, description);
    }

    init(latitude:any, longitude:any, description:any) {
        this.geocoder = new google.maps.Geocoder();

        this.map = new google.maps.Map(document.querySelector("#location_map"), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: defaultZoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // Create the search box and link it to the UI element.
        const mapInput = document.querySelector("#location_map_input");
        var autocomplete = new google.maps.places.Autocomplete(mapInput);
        autocomplete.bindTo("bounds", this.map);
        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapInput);

        this.infoWindow = new google.maps.InfoWindow();
        this.marker = new google.maps.Marker({
            map: this.map,
            draggable: true
        });

        // Event Click for marker
        this.marker.addListener("click", () => {
            if (this.marker.getAnimation() !== null) {
                this.marker.setAnimation(null);
            } else {
                this.marker.setAnimation(google.maps.Animation.BOUNCE);
            }

            this.infoWindow.open(this.map, this.marker);
        });

        // Event drag makrker
        google.maps.event.addListener(this.marker, "dragend", () => {
            var location = this.marker.getPosition();
            this.setCurrentPosition(location.lat(), location.lng());
        });

        // Event for Search on map
        autocomplete.addListener("place_changed", () => {
            this.infoWindow.close();
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                this.map.fitBounds(place.geometry.viewport);
            } else {
                this.map.setCenter(place.geometry.location);
                this.map.setZoom(defaultZoom);
            }

            this.setCurrentPosition(place.geometry.location.lat(), place.geometry.location.lng(), place.formatted_address);
        });

        if (latitude && longitude) {
            this.setCurrentPosition(latitude, longitude, description);
        } else {
            // Using HTML5 geolocation to get current client's location.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(position => {
                    this.setCurrentPosition(position.coords.latitude, position.coords.longitude);
                }, () => {
                    this.handleLocationError(true, this.infoWindow, this.map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                this.handleLocationError(false, this.infoWindow, this.map.getCenter());
            }
        }
    }

    handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation
            ? "Error: The Geolocation service failed."
            : "Error: Your browser doesn't support geolocation.");
    }

    setCurrentPosition(latitude, longitude, description = null) {
        const latLng = new google.maps.LatLng(latitude, longitude);

        this.geocoder.geocode({ latLng: latLng },
            responses => {
                if (responses && responses.length > 0) {
                    let address = responses[0].formatted_address;
                    if (description) address = description;

                    this.onLocationChanged({
                        address: address,
                        latitude: latitude,
                        longitude: longitude
                    });

                    this.marker.setPosition(latLng);

                    this.infoWindow.setContent(address);
                    this.infoWindow.open(this.map, this.marker);

                    this.map.setCenter(latLng);
                } else {
                    console.log("Cannot determine address at this location.");
                }
            });
    }

    private onLocationChanged(args: ILocation) {
        if (this.locationChanged) {
            this.locationChanged(this.map, args);
        }
    }
}