﻿/// <reference path="../../typings/tsd.d.ts" />

class Validator {
    private defaultRules = {
        compare(input: JQuery): boolean {
            if (input.is("[data-compare-msg]") && input.data("compare")) {
                return input.val() === $(input.data("compare")).val();
            }
            return true;
        },
        mask(input): boolean {
            if (input.is("[data-mask-msg]") && input.val() !== "") {
                const maskedtextbox = input.data("kendoMaskedTextBox");
                return maskedtextbox.value().indexOf(maskedtextbox.options.promptChar) === -1;
            }
            return true;
        },
        pattern(input: JQuery): boolean {
            if (input.is("[data-pattern-msg]") && input.val() !== "") {
                const reg = new RegExp(input.data("pattern"));
                return reg.test(input.val());
            }
            return true;
        },
        minlength(input: JQuery): boolean {
            if (input.is("[data-minlength-msg]")) {
                var minlength = input.data("minlength");
                return input.val().length === 0 || input.val().length >= minlength;
            }
            return true;
        },
        depRequired(input: JQuery): boolean {
            if (input.is("[data-deprequired-msg]") && input.data("dependency")) {
                const depValue = $(input.data("dependency")).val();
                return (input.val() !== "" && depValue !== "") || (depValue === "" && input.val() === "") || input.val();
            }
            return true;
        },
        selfRef(input: JQuery): boolean {
            if (input.is("[data-selfref-msg]") && input.data("ref")) {
                return $(input.data("ref")).val() !== input.val();
            }
            return true;
        },
        upload(input: any): boolean {
            const fileInput = input[0];

            if (fileInput.type === "file") {
                const $file = $(fileInput);
                var uploadedFiles = input.closest(".k-upload").find(".k-file");
                if ($file.data("upload") === "required" && uploadedFiles.length === 0) {
                    return false;
                }
                return !uploadedFiles.hasClass("k-file-invalid");
            }
            return true;
        }
    };
    private validatable: kendo.ui.Validator;
    private form: JQuery;

    constructor(public selector: any = "#form", rules?: any) {
        this.form = selector instanceof jQuery ? selector : $(this.selector);

        this.registerEventIfAnyUpload();

        this.validatable = this.form.kendoValidator({
            rules: $.extend(true, this.defaultRules, rules)
        }).data("kendoValidator");
    }

    get isValid() {
        return this.validatable.validate();
    }

    // HACK: http://www.telerik.com/forums/validating-upload
    private registerEventIfAnyUpload() {
        if (this.form.find(".k-upload input[type=file]").length === 0) return;
        this.form.submit(e => {
            // MUST find all again (DO NOT use cached variable here)
            this.form.find(".k-upload input[type=file]").removeAttr("disabled");
            if (!this.isValid) {
                e.preventDefault();
            }
        });
    }

    static for(selector: any, rules?: any) {
        return new Validator(selector, rules);
    }

    static default(rules?: any) {
        return new Validator("form", rules);
    }
}

