﻿/// <reference path="../../../typings/tsd.d.ts" />
var userLocationCtrl = (() => {
    var map: GeoMap;

    var description: any;
    var location: any;

    return {
        init: init
    };

    ///////////////
    function init() {
        description = $("#Description");
        location = $("#Location");

        const locationString = location.val().split(",");
        let latitude = null;
        let longitude = null;

        if (locationString.length === 2) {
            [latitude, longitude] = locationString;
        }

        map = new GeoMap(latitude, longitude, description.val());
        map.locationChanged = locationChanged;
    }

    function locationChanged(sender: any, args: ILocation) {
        location.val(args.latitude + "," + args.longitude);
        description.val(args.address);
    }
})();
