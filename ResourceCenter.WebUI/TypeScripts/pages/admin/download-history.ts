﻿/// <reference path="../../../typings/tsd.d.ts" />
var downloadHistoryCtrl = (() => {
    var grid: kendo.ui.Grid;
    var modalIntance: Modal;

    return {
        init: init,
        detail: detail
    };

    ///////////////
    function init() {
        grid = $("#grid").data("kendoGrid");
        modalIntance = new Modal("Lead Content");

        // prevent animation when show/hide modal
        $("#animatedContent").removeClass("animated");
    }

    function detail(e: any) {
        const dataItem: any = grid.dataItem($(e.currentTarget).closest("tr"));
        modalIntance.toggle();
        modalIntance.showLoading();
        $.post("/Admin/DownloadHistory/Detail", { id: dataItem.Id })
            .done(html => {
                modalIntance.setContent(html);
            })
            .fail(e => {
                modalIntance.setContent(AjaxHelper.getError(e));
            })
            .always(() => {
                modalIntance.hideLoading();
            });
    }
})();


$(() => {
    downloadHistoryCtrl.init();
});