﻿/// <reference path="../../../typings/tsd.d.ts" />
var dashboardCtrl = (() => {
    return {
        init: init
    };

    ///////////////
    function init(forHost: boolean) {
        getDownloadCount({ forToday: false }, "ajaxContent1");
        getDownloadCount({ forToday: false, leadOnly: true }, "ajaxContent2");
        getDownloadCount({ forToday: true, leadOnly: false }, "ajaxContent3");
        getDownloadCount({ forToday: true, leadOnly: true }, "ajaxContent4");

        if (forHost) {
            getLatestDistributors();
        } else {
            getLatestResellers();
        }
        getLatestAssets();
    }

    function getDownloadCount(data, contentId) {
        $.get("/Admin/Dashboard/GetDownloadCount", data, response => {
            if (AjaxHelper.handleError(response)) return;
            $(`#${contentId}`).html(`<h1 class="no-margins">${response.Data}</h1>`);
        }).fail(e => {
            AjaxHelper.fail(e);
        });
    }

    function getLatestDistributors() {
        $.post("/Admin/Dashboard/LatestDistributors").done(html => {
            $("#ajaxLatest1").html(html);
        }).fail(e => {
            AjaxHelper.fail(e);
        });
    }

    function getLatestResellers() {
        $.post("/Admin/Dashboard/LatestResellers").done(html => {
            $("#ajaxLatest1").html(html);
        }).fail(e => {
            AjaxHelper.fail(e);
        });
    }

    function getLatestAssets() {
        $.post("/Admin/Dashboard/LatestAssets").done(html => {
            $("#ajaxLatest2").html(html);
        }).fail(e => {
            AjaxHelper.fail(e);
        });
    }
})();
