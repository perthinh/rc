﻿/// <reference path="../../../typings/tsd.d.ts" />
var categoryCtrl = (() => {
    var treeList: kendo.ui.TreeList;
    return {
        deleteNode: deleteNode,
        init: init
    };

    ///////////////
    function init() {
        treeList = $("#treeList").data("kendoTreeList");
    }

    function deleteNode(e) {
        if (!confirm("Are you sure you want to delete this record?")) return;
        const row = $(e.target).closest("tr");
        const model = treeList.dataItem(row);

        $.post("/Category/Delete", { id: model.id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;
                row.remove();
                Notify.success("Delete category successfully");
            })
            .fail(AjaxHelper.fail);
    }
})();

$(() => {
    categoryCtrl.init();
});