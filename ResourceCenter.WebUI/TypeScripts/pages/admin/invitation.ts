﻿/// <reference path="../../../typings/tsd.d.ts" />

var invitationCtrl = (() => {
    return {
        resend: resend
    };

    ///////////////
    function resend(id: number) {
        if (!confirm("Are you sure you want to resend this email invitation?")) return;

        var button = $("#buttonResend" + id);
        button.attr("disabled", "disabled");
        $.post("/Invitation/Resend/", { id: id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;
                Notify.success("Resending invitation successfully");
            }).fail(AjaxHelper.fail)
            .always(() => {
                button.removeAttr("disabled");
            });
    }
})();
