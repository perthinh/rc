﻿/// <reference path="../../../typings/tsd.d.ts" />
var assetCtrl = (() => {
    var ddlForm: kendo.ui.DropDownList;
    var chbIsGated: JQuery;
    return {
        init: init
    };

    ///////////////
    function init() {

        ddlForm = $("#FormId").data("kendoDropDownList");
        chbIsGated = $("#IsGated");

        chbIsGated.on("change", function () {
            ddlForm.enable(this.checked);
        });

        chbIsGated.trigger("change");

        Validator.default();
    }
})();

$(() => {
    assetCtrl.init();
});