﻿/// <reference path="../../../typings/tsd.d.ts" />
var fieldCtrl = (() => {
    var ddlDataType: kendo.ui.DropDownList;
    var textDataSource: JQuery;
    return {
        init: init
    };

    ///////////////
    function init() {
        Validator.default();

        ddlDataType = $("#DataType").data("kendoDropDownList");
        textDataSource = $("#DataSource");

        ddlDataType.bind("change", function () {
            if (this.value() !== "DropDownList") {
                textDataSource.attr("disabled", "disabled");
            } else {
                textDataSource.removeAttr("disabled");
            }
        });
    }
})();

$(() => {
    fieldCtrl.init();
});