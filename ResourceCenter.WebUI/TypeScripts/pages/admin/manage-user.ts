﻿/// <reference path="../../../typings/tsd.d.ts" />
var manageUserCtrl = (() => {
    var grid: kendo.ui.Grid;

    return {
        init: init,
        toggleIsLocked: toggleIsLocked,
        resetPassword: resetPassword
    };

    ///////////////
    function init() {
        grid = $("#grid").data("kendoGrid");
    }

    function toggleIsLocked(e) {

        var dataItem: any = grid.dataItem($(e.currentTarget).closest("tr"));

        if (!confirm(`Are you sure you want to ${dataItem.IsLocked ? 'unlock' : 'lock'} this user?`)) return;

        $.post("/Admin/ManageUser/ToggleIsLocked/", { id: dataItem.Id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;

                dataItem.set("IsLocked", !dataItem.IsLocked);
                Notify.success(`${dataItem.IsLocked ? 'Lock' : 'Unlock'} successfully`);
            })
            .fail(AjaxHelper.fail);
    }

    function resetPassword(id: number) {

        if (!confirm("Are you sure you want to reset password for this user?")) return;

        $.post("/Admin/ManageUser/ResetPassword/", { id: id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;
                Notify.success("Password reset successfully");
            })
            .fail(AjaxHelper.fail);
    }
})();


$(() => {
    manageUserCtrl.init();
});