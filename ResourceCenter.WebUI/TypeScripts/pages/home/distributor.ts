﻿/// <reference path="../../../typings/tsd.d.ts" />
var pageCtrl = (() => {
    var docElem = document.documentElement,
        header: JQuery,
        didScroll = false,
        changeHeaderOn = 200;

    return {
        init: init
    };

    function init() {
        header = $(".navbar-default");
        window.addEventListener("scroll", event => {
            if (!didScroll) {
                didScroll = true;
                setTimeout(scrollPage, 250);
            }
        }, false);
    }

    function scrollPage() {
        const sy = scrollY();
        if (sy >= changeHeaderOn) {
            header.addClass("navbar-scroll");
        } else {
            header.removeClass("navbar-scroll");
        }
        didScroll = false;
    }

    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }

})();

$(() => {
    ($("body") as any).scrollspy({
        target: ".navbar-fixed-top",
        offset: 80
    });

    // Page scrolling feature
    $("a.page-scroll").on("click", function (event) {
        const link = $(this);
        $("html, body").stop().animate({
            scrollTop: $(link.attr("href")).offset().top - 50
        }, 500);
        event.preventDefault();
        ($("#navbar") as any).collapse("hide");
    });

    pageCtrl.init();
});