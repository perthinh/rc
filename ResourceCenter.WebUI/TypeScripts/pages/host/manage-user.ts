﻿/// <reference path="../../../typings/tsd.d.ts" />

var hostManageUserCtrl = (() => {
    var grid: kendo.ui.Grid;
    var defaultTenantId = 1;

    return {
        init: init,
        dataBound: dataBound,
        toggleIsLocked: toggleIsLocked,
        resetPassword: resetPassword
    };

    ///////////////
    function init() {
        grid = $("#grid").data("kendoGrid");
    }

    function dataBound() {
        grid.table.find("tr[role='row']").each(function () {
            const dataItem: any = grid.dataItem(this);
            if (dataItem.TenantId === defaultTenantId) {
                $(this).find(".k-hierarchy-cell").html("");
            }
        });
    }

    function toggleIsLocked(e) {

        var dataItem: any = grid.dataItem($(e.currentTarget).closest("tr"));

        if (!confirm(`Are you sure you want to ${dataItem.IsLocked ? 'unlock' : 'lock'} this user?`)) return;

        $.post("/Host/ManageUser/ToggleIsLocked/", { id: dataItem.Id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;

                dataItem.set("IsLocked", !dataItem.IsLocked);
                Notify.success(`${dataItem.IsLocked ? 'Lock' : 'Unlock'} successfully`);
            })
            .fail(AjaxHelper.fail);
    }

    function resetPassword(id: number) {

        if (!confirm("Are you sure you want to reset password for this user?")) return;

        $.post("/Host/ManageUser/ResetPassword/", { id: id })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;

                Notify.success("Password reset successfully");
            })
            .fail(AjaxHelper.fail);
    }
})();


$(() => {
    hostManageUserCtrl.init();
});