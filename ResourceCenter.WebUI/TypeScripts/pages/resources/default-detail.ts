﻿/// <reference path="../../../typings/tsd.d.ts" />
var detailCtrl = (() => {
    var formData: {};
    var baseUrl: string;
    var modalInstance: Modal;
    return {
        init: init
    };

    ///////////////
    function init(url, data: {}) {
        baseUrl = url;
        formData = data;
        modalInstance = new Modal("Lead Information");
        $("#btDownload").click(loadAssetLeadForm);
    }

    function loadAssetLeadForm() {
        modalInstance.showLoading();
        $.post(baseUrl, formData)
            .done(renderHtmlAndRegisterEvents)
            .fail(e => {
                modalInstance.setContent(AjaxHelper.getError(e));
            })
            .always(() => {
                modalInstance.hideLoading();
            });
    }

    function renderHtmlAndRegisterEvents(html) {
        modalInstance.setContent(html);
        const form: any = modalInstance.findInContent("form").first();
        form.find(".btn.ladda-button").ladda("bind");

        Validator.for(form);
        form.submit(() => {
            setTimeout(() => {
                modalInstance.toggle();
            }, 2000);
        });
    }
})();
