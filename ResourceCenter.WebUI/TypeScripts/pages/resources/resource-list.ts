﻿/// <reference path="../../../typings/tsd.d.ts" />
var resourceListCtrl = (() => {
    return {
        init: init,
        switchType: switchType
    };

    ///////////////
    function init() {
        $("#btGrid").click(() => switchType(false));
        $("#btList").click(() => switchType(true));
    }

    function switchType(isList: boolean) {
        $("#productView").toggleClass("listView", isList);
    }
})();

$(() => {
    resourceListCtrl.init();
});