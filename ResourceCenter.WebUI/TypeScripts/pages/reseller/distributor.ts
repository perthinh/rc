﻿/// <reference path="../../../typings/tsd.d.ts" />
enum SubscriptionStatus {
    Pending,
    Active,
    Suspended,
    Cancelled
}

var subscriptionCtrl = (() => {
    var grid: kendo.ui.Grid;

    return {
        init: init,
        changeStatus: changeStatus
    };

    function init() {
        grid = $("#grid").data("kendoGrid");
    }

    ///////////////
    function changeStatus(e) {

        if (!confirm("Are you sure you want to change this subscription status?")) return;

        var dataItem: any = grid.dataItem($(e.currentTarget).closest("tr"));
        let status: SubscriptionStatus = dataItem.Status;

        switch (dataItem.Status) {
            case SubscriptionStatus.Active:
                status = SubscriptionStatus.Suspended;
                break;
            case SubscriptionStatus.Suspended:
                status = SubscriptionStatus.Active;
                break;
        }

        $.post("/Reseller/Distributor/ChangeStatus", { id: dataItem.Id, status: status })
            .done((data: AjaxResult) => {
                if (AjaxHelper.handleError(data)) return;

                Notify.success("Changing status successfully");
                dataItem.set("Status", data.Data);
            })
            .fail(AjaxHelper.fail);
    }
})();

$(() => {
    subscriptionCtrl.init();
})