﻿/// <reference path="../../../typings/tsd.d.ts" />

var showAssetCtrl = (() => {
    var btShow: any;
    var form: any;
    var ajaxContent: any;
    var validator: Validator;


    return {
        init: init
    };
    ///////////////
    function init() {
        form = $("#form");
        ajaxContent = $("#ajaxContent");
        validator = Validator.for(form);
        btShow = $("#btShow");

        form.submit((event) => {
            event.preventDefault();
            if (!validator.isValid) return;

            getResourceLink();
        });

        // Init clipboard control, use for Copy To Clipboard
        var clipboard = new Clipboard('.btn');
        clipboard.on('success', () => {
            Notify.success("Copied");
        });
        clipboard.on('error', () => {
            Notify.error("Cannot copied");
        });
    }

    function getResourceLink() {
        var ladda = btShow.ladda();
        ladda.ladda("start");
        ajaxContent.hide();
        $.post("/Reseller/ContentAsset/Resource", form.serialize())
            .done(data => {
                ajaxContent.fadeIn();
                ajaxContent.html(data);
            }).always(() => {
                ladda.ladda("stop");
            });
    }
})();

$(() => {
    showAssetCtrl.init();
});