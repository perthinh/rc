﻿using System;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Extensions;
using ResourceCenter.WebCore.Models;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Controllers
{
    public class DownloadController : BaseController
    {
        private readonly IDownloadService _downloadService;

        public DownloadController(IDownloadService downloadService)
        {
            _downloadService = downloadService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Asset(DownloadModel model)
        {
            if (!ModelState.IsValid)
            {
               throw BadRequestException();
            }

            try
            {
                var result = await _downloadService.DownloadAsset(model, Request);
                if (result.HasFile)
                {
                    AddContentHeader(result.FileName);
                    return File(result.Data, result.ContentType);
                }
            }
            catch (Exception ex)
            {
                throw BadRequestException(ex.GetBaseErrorMessage());
            }

            throw NotFoundException();
        }

        private void AddContentHeader(string fileName)
        {
            var cd = new ContentDisposition
            {
                FileName = fileName,
                Inline = false
            };

            Response.Headers.Add("Content-Disposition", cd.ToString());
        }
    }
}