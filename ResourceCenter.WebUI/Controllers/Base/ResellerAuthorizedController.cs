using System.Web.Mvc;

namespace ResourceCenter.WebUI.Controllers.Base
{
    [Authorize(Roles = "Reseller")]
    public abstract class ResellerAuthorizedController : BaseController
    {
    }
}