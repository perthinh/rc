using System;
using System.Web.Mvc;

namespace ResourceCenter.WebUI.Controllers.Base
{
    [Authorize]
    public abstract class AuthorizedController : BaseController
    {
    }
}