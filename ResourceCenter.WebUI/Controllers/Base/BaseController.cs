using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Core.Web.Mvc.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Extensions;

namespace ResourceCenter.WebUI.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        protected void HandleError(Exception ex)
        {
            string message = null;
            if (ex.HandleSqlException(ref message))
            {
                ModelState.AddModelError("", message);
                return;
            }

            ModelState.AddException(ex);
        }

        protected async Task<ActionResult> ExecuteGridActionAsync<T>(DataSourceRequest request, T model, Func<Task> action)
        {
            if (model != null && ModelState.IsValid)
            {
                try
                {
                    await action();
                }
                catch (Exception ex)
                {
                    HandleError(ex);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult JsonAllowGet(object data)
        {
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public HttpException NotFoundException(string statusDescription = null)
        {
            return new HttpException((int)HttpStatusCode.NotFound, statusDescription);
        }

        public HttpException BadRequestException(string statusDescription = null)
        {
            return new HttpException((int)HttpStatusCode.BadRequest, statusDescription);
        }
    }
}