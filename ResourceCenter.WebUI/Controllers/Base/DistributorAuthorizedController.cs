using System.Web.Mvc;

namespace ResourceCenter.WebUI.Controllers.Base
{
    [Authorize(Roles = "Distributor, User")]
    public abstract class DistributorAuthorizedController : BaseController
    {
    }
}