using System.Web.Mvc;

namespace ResourceCenter.WebUI.Controllers.Base
{
    [Authorize(Roles = "Host")]
    public abstract class HostAuthorizedController : BaseController
    {
    }
}