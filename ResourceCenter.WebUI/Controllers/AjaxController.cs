﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.DataAccess.Uow;
using Core.Infrastructure.Objects;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Controllers
{
    public class AjaxController : AuthorizedController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILanguageService _languageService;
        private PartnerTypeRepository PartnerTypeRepo => _unitOfWork.Repo<PartnerTypeRepository>();

        public AjaxController(IUnitOfWork unitOfWork, ILanguageService languageService)
        {
            _unitOfWork = unitOfWork;
            _languageService = languageService;
        }

        public async Task<JsonResult> GetTenantPartnerTypes(int tenantId)
        {
            var list = await PartnerTypeRepo.AsNoFilter()
                                  .Where(x => x.TenantId == tenantId)
                                  .Select(x => new PairInt
                                  {
                                      Text = x.Name,
                                      Value = x.Id
                                  })
                                  .ToListAsync();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetTenantLanguages(int tenantId)
        {
            var list = await _languageService.GetTenantLanguages(tenantId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}