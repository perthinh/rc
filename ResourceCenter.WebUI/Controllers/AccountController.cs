﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Filters;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Account.Models;
using ResourceCenter.WebCore.Modules.Account.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Controllers
{
    public class AccountController : AuthorizedController
    {
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IAccountService _service;

        public AccountController(IAccountService service, IAuthenticationManager authenticationManager)
        {
            _service = service;
            _authenticationManager = authenticationManager;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            Thread.Sleep(5000);
            var result = await _service.Login(model);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", @"This account has been locked out.");
                    return View(model);
                default:
                    ModelState.AddModelError("", @"Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Register(CreateDistributorModel model)
        {
            var result = await _service.Register(model, Request.UserLanguage());
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            AddErrors(result);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult RegisterReseller()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> RegisterReseller(CreateResellerModel model)
        {
            var result = await _service.RegisterReseller(model, Request.UserLanguage());
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }

            AddErrors(result);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            return View((await _service.ConfirmEmail(userId, code)).Succeeded ? "ConfirmEmail" : "Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                var result = await _service.ForgotPassword(model);
                if (result == null)
                {
                    ModelState.AddModelError("", @"The Email does not exist");
                }
                else
                {
                    return View("ForgotPasswordConfirmation");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(int? userId = null, string code = null)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            var result = await _service.ResetPassword(model);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            AddErrors(result);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult Logout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
        }
        #endregion

        private void AddErrors(IdentityResult result)
        {
            ModelState.AddIdentityResultErrors(result);
        }
    }
}