﻿using System.Web.Mvc;

namespace ResourceCenter.WebUI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Public/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}