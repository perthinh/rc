﻿using Core.Dependency.Tasks;
using Core.Tasks;
using Microsoft.Owin;
using Owin;
using ResourceCenter.WebUI;

[assembly: OwinStartup(typeof(Startup))]
namespace ResourceCenter.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            PackageConfig.Register();

            DependencyConfig.Register(app);
            AuthConfig.Register(app);

            ScopeTaskCommand.ExecuteInitTasks();
        }
    }
}
