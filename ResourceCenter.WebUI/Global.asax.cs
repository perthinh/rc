﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Core.Extensions;
using ResourceCenter.WebUI.Controllers;
using ResourceCenter.WebUI.Infrastructure.Views;

namespace ResourceCenter.WebUI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ExtendedRazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
#if  !DEBUG
            HandleApplicationError(sender as MvcApplication);
#endif
        }

        private void HandleApplicationError(HttpApplication sender)
        {
            var httpContext = sender.Context;
            var currentController = "";
            var currentAction = "";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null)
            {
                currentController = currentRouteData.Values["controller"].ChangeTypeTo<string>(string.Empty);
                currentAction = currentRouteData.Values["action"].ChangeTypeTo<string>(string.Empty);
            }

            var exception = Server.GetLastError();
            var action = "Index";

            if (exception is HttpException)
            {
                var httpException = exception as HttpException;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        action = "NotFound";
                        break;
                        // others if any
                }
            }

            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = (exception as HttpException)?.GetHttpCode() ?? 500;
            httpContext.Response.TrySkipIisCustomErrors = true;

            var routeData = new RouteData();
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;
            routeData.Values["area"] = "";

            var controller = new ErrorController
            {
                ViewData = { Model = new HandleErrorInfo(exception, currentController, currentAction) }
            };
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }
    }
}
