﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Commands;
using ResourceCenter.WebCore.Modules.Forms.Models;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class FieldTranslationController : DistributorAuthorizedController
    {
        private readonly IUnitOfWork _unitOfWork;
        private FieldTranslationRepository TranslationRepo => _unitOfWork.Repo<FieldTranslationRepository>();

        public FieldTranslationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [ChildActionOnly]
        public PartialViewResult Index(int fieldId)
        {
            ViewBag.FieldId = fieldId;
            return PartialView();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request, int fieldId)
        {
            var list = TranslationRepo.Queryable()
                .Where(x => x.FormFieldId == fieldId)
                .OrderBy(x => x.LanguageCode)
                .ProjectTo<FieldTranslationModel>();
            return Json(list.ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request,
            int fieldId, FieldTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = model.MapTo<FormFieldTranslation>();
                entity.FormFieldId = fieldId;
                TranslationRepo.Insert(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request,
            int fieldId, FieldTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = await TranslationRepo.GetAsync(model.Id);
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request,
            int fieldId, FieldTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                await DeleteTranslationChecker.Execute(TranslationRepo, fieldId);

                var entity = await TranslationRepo.GetAsync(model.Id);
                TranslationRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            });
        }
    }
}