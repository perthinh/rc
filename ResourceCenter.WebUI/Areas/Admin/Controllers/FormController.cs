﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Forms.Models;
using ResourceCenter.WebCore.Modules.Forms.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class FormController : DistributorAuthorizedController
    {
        private readonly IFormService _service;

        public FormController(IFormService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request, FormModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Create(model));

        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, FormModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Update(model));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, FormModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }

        public ViewResult Preview(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Preview(int id, PreviewFormModel model)
        {
            model.PreviewForm = await _service.GetLeadForm(id, model.Language);
            return View(model);
        }
     
    }
}