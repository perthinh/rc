﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Ajax;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Invitations.Models;
using ResourceCenter.WebCore.Modules.Invitations.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class InvitationController : DistributorAuthorizedController
    {
        private readonly IInvitationService _service;

        public InvitationController(IInvitationService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, InvitationPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Update(model));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateInvitationModel model)
        {
            try
            {
                await _service.Create(model);
                return RedirectToAction("Index")
                            .WithSuccess($"Your invitation has been successfully sent to {model.EmailTo}");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }


        [HttpPost]
        public async Task<ActionResult> Resend(int id)
        {
            try
            {
                await _service.ResendEmail(id);
                return Json(AjaxResult.Success().ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }
    }
}