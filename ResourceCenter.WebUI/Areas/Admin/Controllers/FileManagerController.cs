﻿using System;
using System.Web.Mvc;
using Core.Web.Mvc.Ajax;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.FileManager;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class FileManagerController : DistributorAuthorizedController
    {
        private readonly IManageFileService _service;

        public FileManagerController(IManageFileService service)
        {
            _service = service;
        }

        public ActionResult Index(string targetId)
        {
            return View(new UploadedFileModel
            {
                TargetId = targetId
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public ActionResult Index(UploadedFileModel model)
        {
            try
            {
                _service.Upload(model);
                return RedirectToAction("Index", new { targetId = model.TargetId })
                                .WithSuccess("Upload successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            return View(model);
        }

        public ActionResult ListView_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult DeleteFile(FileModel model)
        {
            try
            {
                _service.DeleteThumbnail(model.Name);
                return Json(AjaxResult.Success().ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }
    }
}
