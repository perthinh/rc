﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Commands;
using ResourceCenter.WebCore.Modules.Assets.Models;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class AssetTranslationController : DistributorAuthorizedController
    {
        private readonly IUnitOfWork _unitOfWork;
        private AssetTranslationRepository TranslationRepo => _unitOfWork.Repo<AssetTranslationRepository>();

        public AssetTranslationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [ChildActionOnly]
        public PartialViewResult Index(int assetId)
        {
            ViewBag.AssetId = assetId;
            return PartialView();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request, int assetId)
        {
            var list = TranslationRepo.Queryable()
                .Where(x => x.AssetId == assetId)
                .OrderBy(x => x.LanguageCode)
                .ProjectTo<AssetTranslationModel>();
            return Json(list.ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request,
            int assetId, AssetTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = model.MapTo<AssetTranslation>();
                entity.AssetId = assetId;
                TranslationRepo.Insert(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request,
            int assetId, AssetTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = await TranslationRepo.GetAsync(model.Id);
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request,
            int assetId, AssetTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                await DeleteTranslationChecker.Execute(TranslationRepo, assetId);

                var entity = await TranslationRepo.GetAsync(model.Id);
                TranslationRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            });
        }
    }
}