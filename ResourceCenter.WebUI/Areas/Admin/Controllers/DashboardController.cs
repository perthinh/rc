﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Ajax;
using ResourceCenter.WebCore.Modules.Dashboard.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class DashboardController : AuthorizedController
    {
        private readonly IDashboardService _service;

        public DashboardController(IDashboardService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public PartialViewResult LatestDistributors()
        {
            var list = _service.GetLatestDistributors();
            return PartialView(list);
        }

        [HttpPost]
        public PartialViewResult LatestResellers()
        {
            var list = _service.GetLatestResellers();
            return PartialView(list);
        }

        [HttpPost]
        public PartialViewResult LatestAssets()
        {
            var list = _service.GetLatestAssets();
            return PartialView(list);
        }

        public async Task<ActionResult> GetDownloadCount(bool forToday, bool leadOnly = false)
        {
            var count = await _service.GetDownloadCount(forToday, leadOnly);
            return JsonAllowGet(AjaxResult.Success().Data(count.ToString("##,##0")).ToResult());
        }
    }
}