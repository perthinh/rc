﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Repositories;
using ResourceCenter.WebCore.Commands;
using ResourceCenter.WebCore.Modules.Categories.Models;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class CategoryTranslationController : DistributorAuthorizedController
    {
        private readonly IUnitOfWork _unitOfWork;
        private CategoryTranslationRepository TranslationRepo => _unitOfWork.Repo<CategoryTranslationRepository>();

        public CategoryTranslationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [ChildActionOnly]
        public PartialViewResult Index(int categoryId)
        {
            ViewBag.CategoryId = categoryId;
            return PartialView();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request, int categoryId)
        {
            var list = TranslationRepo.Queryable()
                .Where(x => x.CategoryId == categoryId)
                .OrderBy(x => x.LanguageCode)
                .ProjectTo<CategoryTranslationModel>();
            return Json(list.ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request,
            int categoryId, CategoryTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = model.MapTo<CategoryTranslation>();
                entity.CategoryId = categoryId;
                TranslationRepo.Insert(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request,
            int categoryId, CategoryTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                var entity = await TranslationRepo.GetAsync(model.Id);
                model.MapTo(entity);
                await _unitOfWork.SaveChangesAsync();

                entity.MapTo(model);
            });
        }

        [HttpPost]
        public async Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request,
            int categoryId, CategoryTranslationModel model)
        {
            return await ExecuteGridActionAsync(request, model, async () =>
            {
                await DeleteTranslationChecker.Execute(TranslationRepo, categoryId);

                var entity = await TranslationRepo.GetAsync(model.Id);
                TranslationRepo.Delete(entity);
                await _unitOfWork.SaveChangesAsync();
            });
        }
    }
}