﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Ajax;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.ManageUser.Models;
using ResourceCenter.WebCore.Modules.ManageUser.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class ManageUserController : DistributorAuthorizedController
    {
        private readonly IManageUserService _service;

        public ManageUserController(IManageUserService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Grid actions
        public JsonResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, UserPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => ExecuteIdentityAction(() => _service.Delete(model)));
        }

        [HttpPost]
        public async Task<JsonResult> ToggleIsLocked(int id)
        {
            try
            {
                var result = await _service.ToggleIsLocked(id);
                if (result.Succeeded)
                {
                    return Json(AjaxResult.Success().ToResult());
                }

                ModelState.AddIdentityResultErrors(result);
                return Json(AjaxResult.Error().WithModelState(ModelState).ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }

        [HttpPost]
        public async Task<JsonResult> ResetPassword(int id)
        {
            try
            {
                var result = await _service.ResetPassword(id);
                if (result.Succeeded)
                {
                    return Json(AjaxResult.Success().ToResult());
                }

                ModelState.AddIdentityResultErrors(result);
                return Json(AjaxResult.Error().WithModelState(ModelState).ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }

        private async Task<IdentityResult> ExecuteIdentityAction(Func<Task<IdentityResult>> func)
        {
            var result = await func();

            if (!result.Succeeded)
            {
                ModelState.AddIdentityResultErrors(result);
            }

            return result;
        }
        #endregion

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateUserModel model)
        {
            try
            {
                var result = await ExecuteIdentityAction(() => _service.Create(model));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index").WithSuccess($"User {model.UserName} created");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View((await _service.GetUser(id)).MapTo<EditUserModel>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(EditUserModel model)
        {
            try
            {
                var result = await ExecuteIdentityAction(() => _service.Update(model));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index").WithSuccess($"User {model.UserName} updated");
                }
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }
    }
}