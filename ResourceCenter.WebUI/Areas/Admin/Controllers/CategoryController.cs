﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Ajax;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Categories.Models;
using ResourceCenter.WebCore.Modules.Categories.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class CategoryController : DistributorAuthorizedController
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> TreeList_Read([DataSourceRequest] DataSourceRequest request)
        {
            var list = await _service.GetList();
            return Json(list.ToTreeDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateCategoryModel model)
        {
            try
            {
                var entity = await _service.Create(model);
                return RedirectToAction("Edit", new { id = entity.Id })
                            .WithSuccess("Create successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            var entity = await _service.Get(id);
            if (entity == null)
            {
                throw NotFoundException();
            }
            return View(entity.MapTo<EditCategoryModel>());
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(int id, EditCategoryModel model)
        {
            try
            {
                await _service.Update(model);
                return RedirectToAction("Edit", new { id = model.Id })
                            .WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
                return Json(AjaxResult.Success().ToResult());
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return Json(AjaxResult.Error().WithModelState(ModelState).ToResult());
            }
        }
    }
}