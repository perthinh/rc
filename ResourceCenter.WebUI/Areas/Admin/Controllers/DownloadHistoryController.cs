﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.DownloadHistory.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class DownloadHistoryController : DistributorAuthorizedController
    {
        private readonly IDownloadHistoryService _historyService;

        public DownloadHistoryController(IDownloadHistoryService historyService)
        {
            _historyService = historyService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_historyService.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> Detail(int id)
        {
            var result = await _historyService.GetDetail(id);
            return PartialView(result);
        }
    }
}