using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Assets.Models;
using ResourceCenter.WebCore.Modules.Assets.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class AssetController : DistributorAuthorizedController
    {
        private readonly IAssetService _service;

        public AssetController(IAssetService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AssetFilterModel model)
        {
            return View(model);
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request, int? partnerTypeId)
        {
            return Json(_service.GetList(partnerTypeId).ToDataSourceResult(request));
        }

        public ActionResult Create()
        {
            return View(new CreateAssetModel());
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateAssetModel model)
        {
            try
            {
                var entity = await _service.Create(model);
                return RedirectToAction("Edit", new { id = entity.Id })
                    .WithSuccess("Create successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            var entity = await _service.Get(id);
            if (entity == null)
            {
                throw NotFoundException();
            }

            return View(entity.MapTo<EditAssetModel>());
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(int id, EditAssetModel model)
        {
            try
            {
                await _service.Update(model);
                return RedirectToAction("Edit", new { id = model.Id })
                            .WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, AssetPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }
    }
}