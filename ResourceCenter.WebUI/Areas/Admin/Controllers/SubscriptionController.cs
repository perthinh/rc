﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Subscriptions.Models;
using ResourceCenter.WebCore.Modules.Subscriptions.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class SubscriptionController : DistributorAuthorizedController
    {
        private readonly ISubscriptionService _service;

        public SubscriptionController(ISubscriptionService service)
        {
            _service = service;
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.PartnerTypes = await _service.GetTenantPartnerTypes();
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, SubscriptionModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Update(model));
        }
    }
}