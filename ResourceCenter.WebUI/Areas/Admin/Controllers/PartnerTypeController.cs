﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.PartnerTypes.Models;
using ResourceCenter.WebCore.Modules.PartnerTypes.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class PartnerTypeController : DistributorAuthorizedController
    {
        private readonly IPartnerTypeService _service;

        public PartnerTypeController(IPartnerTypeService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request, PartnerTypeModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Create(model));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, PartnerTypeModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Update(model));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, PartnerTypeModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }
    }
}