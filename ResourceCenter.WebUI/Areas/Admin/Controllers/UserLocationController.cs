﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Location.Models;
using ResourceCenter.WebCore.Modules.Location.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class UserLocationController : AuthorizedController
    {
        private readonly IUserLocationService _service;

        public UserLocationController(IUserLocationService service)
        {
            _service = service;
        }

        // GET: Admin/Location
        public ActionResult Index()
        {
            return View();
        }

        #region Grid action
        public JsonResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, UserLocationPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }
        #endregion

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(UserLocationModel model)
        {
            try
            {
                await _service.Create(model);

                return RedirectToAction("Index").WithSuccess("Create successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View((await _service.GetLocation(id)).MapTo<UserLocationModel>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(UserLocationModel model)
        {
            try
            {
                await _service.Update(model);

                return View(model).WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }
    }
}