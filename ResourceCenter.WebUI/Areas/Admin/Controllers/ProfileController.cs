﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Profile.Models;
using ResourceCenter.WebCore.Modules.Profile.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class ProfileController : AuthorizedController
    {
        private readonly IProfileService _service;

        public ProfileController(IProfileService service)
        {
            _service = service;
        }

        public async Task<ActionResult> PersonalInfo()
        {
            return View(await _service.GetPersonalInfo());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> PersonalInfo(PersonalInfoModel model)
        {
            var result = await _service.UpdatePersonalInfo(model);

            if (result.Succeeded)
            {
                return View(model).WithSuccess("Update successfully");
            }

            ModelState.AddIdentityResultErrors(result);
            return View(model);
        }

        public async Task<ActionResult> CompanyInfo()
        {
            return View(await _service.GetCompanyInfo());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> CompanyInfo(CompanyModel model)
        {
            try
            {
                await _service.UpdateCompanyInfo(model);
                return View(model).WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            var result = await _service.ChangePassword(model);

            if (result.Succeeded)
            {
                return RedirectToAction("ChangePassword").WithSuccess("Update successfully");
            }

            ModelState.AddIdentityResultErrors(result);
            return View(model);
        }
    }
}
