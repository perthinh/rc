﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Forms.Models;
using ResourceCenter.WebCore.Modules.Forms.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class FormFieldController : DistributorAuthorizedController
    {
        private readonly IFormFieldService _service;
        private readonly IFormService _formService;

        public FormFieldController(IFormFieldService service, IFormService formService)
        {
            _service = service;
            _formService = formService;
        }

        public async Task<ActionResult> Index(int formId)
        {
            var form = await _formService.Get(formId);
            if (form == null)
            {
                return RedirectToAction("Index", "Form").WithError("The form does not exist");
            }
            return View(form.MapTo<FormViewModel>());
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request, int formId)
        {
            return Json(_service.GetList(formId).ToDataSourceResult(request));
        }

        public ActionResult Create(int formId)
        {
            var model = new CreateFieldModel
            {
                FormId = formId
            };
            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateFieldModel model)
        {
            try
            {
                var entity = await _service.Create(model);
                return RedirectToAction("Edit", new { id = entity.Id })
                    .WithSuccess("Create successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            var entity = await _service.Get(id);
            if (entity == null)
            {
                throw NotFoundException();
            }

            return View(entity.MapTo<EditFieldModel>());
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(int id, EditFieldModel model)
        {
            try
            {
                await _service.Update(model);
                return RedirectToAction("Edit", new { id = model.Id })
                            .WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }


        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, FormFieldPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }
    }
}