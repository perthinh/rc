﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.AssetLanguages.Models;
using ResourceCenter.WebCore.Modules.AssetLanguages.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Admin.Controllers
{
    public class LanguageController : AuthorizedController
    {
        private readonly IAssetLanguageService _assetLangService;

        public LanguageController(IAssetLanguageService assetLangService)
        {
            _assetLangService = assetLangService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_assetLangService.Queryable().ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Create([DataSourceRequest] DataSourceRequest request, LanguageModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _assetLangService.Insert(model));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, LanguageModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _assetLangService.Update(model));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, LanguageModel model)
        {
            return ExecuteGridActionAsync(request, model, () => _assetLangService.Delete(model));
        }
    }
}