﻿using System.Web.Mvc;

namespace ResourceCenter.WebUI.Areas.Host
{
    public class HostAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Host";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Host_default",
                "Host/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ResourceCenter.WebUI.Areas.Host.Controllers" }

            );
        }
    }
}