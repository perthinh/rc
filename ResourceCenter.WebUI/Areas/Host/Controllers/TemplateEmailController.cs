﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.AutoMapper;
using Core.Web.Mvc.Alerts;
using Core.Web.Mvc.Filters;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Modules.Host.Models;
using ResourceCenter.WebCore.Modules.Host.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Host.Controllers
{
    public class TemplateEmailController : HostAuthorizedController
    {
        private readonly ITemplateEmailService _service;

        public TemplateEmailController(ITemplateEmailService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Grid actions
        public async Task<JsonResult> Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json((await _service.GetList()).ToDataSourceResult(request));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Update([DataSourceRequest] DataSourceRequest request, TemplateEmailPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Update(model.MapTo<EditTemplateEmail>()));
        }

        [HttpPost]
        public Task<ActionResult> Grid_Destroy([DataSourceRequest] DataSourceRequest request, TemplateEmailPreview model)
        {
            return ExecuteGridActionAsync(request, model, () => _service.Delete(model));
        }
        #endregion

        public ActionResult Create()
        {
            return View(new CreateTemplateEmail());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Create(CreateTemplateEmail model)
        {
            try
            {
                await _service.Create(model);
                return RedirectToAction("Index").WithSuccess("Create successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            return View((await _service.Get(id)).MapTo<EditTemplateEmail>());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Edit(EditTemplateEmail model)
        {
            try
            {
                await _service.Update(model);
                return View(model).WithSuccess("Update successfully");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            return View(model);
        }
    }
}