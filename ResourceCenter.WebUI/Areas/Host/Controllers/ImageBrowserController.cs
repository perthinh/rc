﻿using System.Web.Http;
using Core.IO;
using Kendo.Mvc.UI;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Services;

namespace ResourceCenter.WebUI.Areas.Host.Controllers
{
    [Authorize(Roles = "Host")]
    public class ImageBrowserController : EditorImageBrowserController
    {
        private readonly IUploadService _uploadService;

        public ImageBrowserController(IUploadService uploadService)
        {
            _uploadService = uploadService;
        }

        public override string ContentPath => CreateUserFolder();

        private string CreateUserFolder()
        {
            DirectoryHelper.CreateIfNotExists(_uploadService.GetImageFolder().ToAbsolutePath());
            return _uploadService.GetImageFolder();
        }
    }
}