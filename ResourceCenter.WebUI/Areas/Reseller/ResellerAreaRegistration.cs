﻿using System.Web.Mvc;

namespace ResourceCenter.WebUI.Areas.Reseller
{
    public class ResellerAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Reseller";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Reseller_default",
                "Reseller/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ResourceCenter.WebUI.Areas.Reseller.Controllers" }
            );
        }
    }
}