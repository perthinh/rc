﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Filters;
using ResourceCenter.WebCore.Modules.Resellers.Models;
using ResourceCenter.WebCore.Modules.Resellers.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Reseller.Controllers
{
    public class SubscriptionController : ResellerAuthorizedController
    {
        private readonly ISubscriberService _service;

        public SubscriptionController(ISubscriberService service)
        {
            _service = service;
        }

        public ActionResult Subscribe()
        {
            return View(new SubscribeModel());
        }

        [HttpPost, ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> Subscribe(SubscribeModel model)
        {
            try
            {
                var isSubscribed = await _service.IsSubscribed(model.TenantId);
                if (isSubscribed)
                {
                    ModelState.AddModelError("", "This distributor was already subscribed");
                    return View(model);
                }

                await _service.Subscribe(model);
                return RedirectToAction("Result");
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            return View(model);
        }

        public ActionResult Result()
        {
            return View();
        }

        public ActionResult InvitationCode()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateModel]
        public async Task<ActionResult> InvitationCode(InvitationCodeModel model)
        {
            try
            {
                await _service.Subscribe(model);
                return RedirectToAction("Result");
            }
            catch (Exception ex)
            {
                HandleError(ex);
                return View(model);
            }
        }
    }
}