﻿using System.Web.Mvc;
using ResourceCenter.WebCore.Modules.Resellers.Models;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Reseller.Controllers
{
    public class ContentAssetController : ResellerAuthorizedController
    {
        public ActionResult Index()
        {
            return View(new PreviewAssetModel());
        }

        [HttpPost]
        public PartialViewResult Resource(PreviewAssetModel model)
        {
            return PartialView(model);
        }
    }
}