﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Core.Web.Mvc.Ajax;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Modules.Resellers.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Reseller.Controllers
{
    public class DistributorController : ResellerAuthorizedController
    {
        private readonly IDistributorService _service;

        public DistributorController(IDistributorService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Grid_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.GetList().ToDataSourceResult(request));
        }

        [HttpPost]
        public async Task<ActionResult> ChangeStatus(int id, ESubscriptionStatus status)
        {
            try
            {
                await _service.ChangeStatus(id, status);
                return Json(AjaxResult.Success().Data(status).ToResult());
            }
            catch (Exception ex)
            {
                return Json(AjaxResult.Error().WithException(ex).ToResult());
            }
        }
    }
}