﻿using System.Web.Mvc;

namespace ResourceCenter.WebUI.Areas.Resources
{
    public class ResourcesAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Resources";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "ResourceHub",
                url: "Resources/{lang}/{resellerId}/{tenantId}/{controller}/{action}/{id}",
                defaults: new { controller = "Default", action = "Index", area = "Resources", id = UrlParameter.Optional },
                namespaces: new[] { "ResourceCenter.WebUI.Areas.Resources.Controllers" }
            );

            context.MapRoute(
                "Resources_default",
                "Resources/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ResourceCenter.WebUI.Areas.Resources.Controllers" }
            );
        }
    }
}