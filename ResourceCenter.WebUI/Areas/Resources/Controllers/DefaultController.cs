﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ResourceCenter.WebCore.Filters;
using ResourceCenter.WebCore.Modules.Resellers.Query;
using ResourceCenter.WebCore.Modules.Resellers.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Resources.Controllers
{
    [ResourceAuthorize]
    public class DefaultController : BaseController
    {
        private readonly IContentService _contentService;

        public DefaultController(IContentService contentService)
        {
            _contentService = contentService;
        }

        public ActionResult Index(PagedAssetsQuery query)
        {
            return View(_contentService.GetAssets(query));
        }

        public async Task<ActionResult> Detail(AssetDetailQuery query)
        {
            var result = await _contentService.GetAssetDetail(query);
            if (result == null)
            {
                return RedirectToAction("Index");
            }
            return View(result);
        }

        public async Task<PartialViewResult> AssetLeadForm(AssetDetailQuery query)
        {
            ViewBag.ResellerId = query.ResellerId;
            var model = await _contentService.GetAssetLeadForm(query.AssetId, query.Lang);
            return PartialView(model);
        }
    }
}