﻿using System.Web.Mvc;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Resources.Controllers
{
    public class PublicController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}