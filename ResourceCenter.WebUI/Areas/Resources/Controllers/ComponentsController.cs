﻿using System.Web.Mvc;
using ResourceCenter.WebCore.Modules.Resellers.Query;
using ResourceCenter.WebCore.Modules.Resellers.Services;
using ResourceCenter.WebUI.Controllers.Base;

namespace ResourceCenter.WebUI.Areas.Resources.Controllers
{
    /// <summary>
    /// DO NOT USE async in this controller
    /// </summary>
    [ChildActionOnly]
    public class ComponentsController : BaseController
    {
        private readonly IContentService _contentService;

        public ComponentsController(IContentService contentService)
        {
            _contentService = contentService;
        }

        public PartialViewResult Header(int resellerId)
        {
            var model = _contentService.GetResellerInfo(resellerId);
            return PartialView(model);
        }

        public PartialViewResult Contact(int resellerId)
        {
            var model = _contentService.GetResellerInfo(resellerId);
            return PartialView(model);
        }

        public PartialViewResult Categories(ResourceQuery query)
        {
            return PartialView(_contentService.GetCategories(query));
        }
    }
}