﻿using System.Web.Optimization;

namespace ResourceCenter.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js-admin")
                 .Include("~/Scripts/libs/jquery.min.js")
                 .Include("~/Scripts/kendo/kendo.all.min.js")
                 .Include("~/Scripts/kendo/kendo.aspnetmvc.min.js")
                 .Include("~/Scripts/libs/bootstrap.min.js")
                 .Include("~/Scripts/libs/toastr.min.js")
                 //.Include("~/Scripts/libs/es6-promise.min.js")
                 .Include("~/Scripts/libs/sweetalert2.min.js")
                 .Include("~/Scripts/libs/spin.min.js")
                 .Include("~/Scripts/ladda.js")
                 .Include("~/Scripts/libs/ladda.jquery.min.js")
                 .Include("~/Scripts/plugins/metisMenu/metisMenu.min.js")
                 .Include("~/Scripts/plugins/slimscroll/jquery.slimscroll.min.js")
                 .Include("~/Scripts/plugins/pace/pace.min.js")
                 .IncludeDirectory("~/Scripts/app/core", "*.js")
                 .Include("~/Scripts/app/app.js")
             );


            bundles.Add(new StyleBundle("~/Content/css/all").Include(
                "~/Content/vendors/bootstrap.css",
                "~/Content/vendors/awesome-bootstrap-checkbox.css",
                "~/Content/vendors/font-awesome.css",
                "~/Content/vendors/toastr.css",
                "~/Content/vendors/sweetalert2.css",
                "~/Content/vendors/ladda-themeless.min.css",
                "~/Content/css/animate.css",
                "~/Content/css/kendo.common-bootstrap.min.css",
                "~/Content/css/kendo.bootstrap.min.css",
                "~/Content/css/inspinia.css",
                "~/Content/css/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/js-home").Include(
                "~/Scripts/libs/jquery.min.js",
                "~/Scripts/libs/bootstrap.min.js",
                "~/Scripts/plugins/slimscroll/jquery.slimscroll.min.js",
                "~/Scripts/plugins/wow/wow.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js-blank").Include(
                "~/Scripts/libs/jquery.min.js",
                "~/Scripts/libs/bootstrap.min.js",
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/libs/toastr.min.js",
                "~/Scripts/libs/spin.min.js",
                "~/Scripts/ladda.js",
                "~/Scripts/libs/ladda.jquery.min.js")
                .IncludeDirectory("~/Scripts/app/core", "*.js")
                .Include("~/Scripts/app/app_blank.js"));
#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
