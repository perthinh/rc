﻿using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using CacheManager.Core;
using Core.Dependency.Extensions;
using Core.Web.Caching;
using Core.Web.MetadataModel;
using Core.Web.MetadataModel.Filters;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Owin;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using Core.DataAccess.Context;
using Core.DataAccess.Uow;
using Core.Tasks;
using MvcSiteMapProvider.Loader;
using MvcSiteMapProvider.Web.Mvc;
using MvcSiteMapProvider.Xml;
using ResourceCenter.DataAccess;
using ResourceCenter.DataAccess.AspNetIdentity;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.Resources;
using ResourceCenter.WebCore.AspNetIdentity;
using ResourceCenter.WebCore.Services;
using ResourceCenter.WebCore.Services.Emails;
using ResourceCenter.WebCore.Tasks;
using ResourceCenter.WebUI.Infrastructure.SiteMap.SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace ResourceCenter.WebUI
{
    public class DependencyConfig
    {
        public static void Register(IAppBuilder app)
        {
            var container = new Container();
            var scopeLifestyle = new ExecutionContextScopeLifestyle();

            var hybrid = Lifestyle.CreateHybrid(
                  () => scopeLifestyle.GetCurrentScope(container) != null,
                  scopeLifestyle,
                  new WebRequestLifestyle());

            container.Options.DefaultScopedLifestyle = hybrid;
            //container.Options.AutoWirePropertiesWithAttribute<InjectAttribute>();

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();

            container.RegisterCollection<IRunAtInit>(new[] { typeof(SystemInitializer).Assembly });
            container.RegisterCollection<IModelMetadataFilter>(new[] { typeof(IModelMetadataFilter).Assembly });
            container.Register<ModelMetadataProvider, ExtensibleModelMetadataProvider>();

            container.Register(() =>
              container.IsVerifying()
                  ? new OwinContext(new Dictionary<string, object>()).Authentication
                  : HttpContext.Current.GetOwinContext().Authentication, Lifestyle.Scoped);

            container.Register<UserManager>(Lifestyle.Scoped);
            container.RegisterInitializer<UserManager>(manager => manager.Configure(app));

            container.Register<ICurrentUser, CurrentUser>(Lifestyle.Scoped);
            container.Register<SignInManager>(Lifestyle.Scoped);
            container.Register<RoleManager>(Lifestyle.Scoped);
            container.Register<IUserStore<User, int>, UserStore>(Lifestyle.Scoped);
            container.Register<IRoleStore<Role, int>, RoleStore>(Lifestyle.Scoped);

            container.Register<IDataContext>(() => new EfContext(), Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<ISessionCache, SessionCache>(Lifestyle.Scoped);
            container.Register<IPerRequestCache, PerRequestCache>(Lifestyle.Scoped);

            container.Register<IUserLanguage, UserLanguage>(Lifestyle.Scoped);
            container.Register<ITemplateEmailSender, TemplateEmailSender>(Lifestyle.Scoped);

            container.RegisterDefaultConventions("ResourceCenter.WebCore");

            container.RegisterDefaultDepdendencies(new[] {
                ResTexts.ResourceManager
            });

            container.RegisterDependencyManager();

            RegisterCacheManager(container);

            MvcSiteMapProviderContainerInitializer.SetUp(container);

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            InitializeMvcSiteMap(container);
        }

        private static void InitializeMvcSiteMap(Container container)
        {
            // Setup global sitemap loader (required)
            MvcSiteMapProvider.SiteMaps.Loader = container.GetInstance<ISiteMapLoader>();
            // Check all configured .sitemap files to ensure they follow the XSD for MvcSiteMapProvider (optional)
            var validator = container.GetInstance<ISiteMapXmlValidator>();
            validator.ValidateXml(HostingEnvironment.MapPath("~/Mvc.sitemap"));
            // Register the Sitemaps routes for search engines (optional)
            XmlSiteMapController.RegisterRoutes(RouteTable.Routes);
        }

        private static void RegisterCacheManager(Container container)
        {
            var cacheConfig = ConfigurationBuilder.BuildConfiguration(settings =>
            {
                settings.WithSystemRuntimeCacheHandle("inprocess");
            });

            container.RegisterSingleton(() => CacheFactory.FromConfiguration<object>(cacheConfig));

            container.ResolveUnregisteredType += (sender, e) =>
            {
                if (e.UnregisteredServiceType.IsGenericType &&
                    e.UnregisteredServiceType.GetGenericTypeDefinition() == typeof(ICacheManager<>))
                {
                    e.Register(() => CacheFactory.FromConfiguration(
                        e.UnregisteredServiceType.GetGenericArguments()[0],
                        cacheConfig));
                }
            };
        }
    }
}