﻿using System.Web.Mvc;
using Core.Web.Audit;
using Core.Web.Mvc.Filters;
using ResourceCenter.WebUI.Infrastructure.Filters;

namespace ResourceCenter.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorFilter());
            filters.Add(new AjaxHandledErrorFilter());
            filters.Add(new UserAuditAttribute());
        }
    }
}
