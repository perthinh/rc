﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using Core.Web.Extensions;
using HtmlTags;
using Kendo.Mvc.UI;
using ResourceCenter.DataAccess.Enums;
using ResourceCenter.WebCore.Extensions;
using ResourceCenter.WebCore.Modules.Forms.Models;

namespace ResourceCenter.WebUI.Infrastructure.Extensions
{
    public static class LeadFormExtensions
    {
        public static MvcHtmlString LeadFieldEditor(this HtmlHelper helper, LeadFieldModel model, bool deferred = false)
        {
            switch (model.DataType)
            {
                case EFieldDataType.DropDownList:
                    return DropDownList(helper, model, deferred);

                case EFieldDataType.CheckBox:
                    return CheckBox(helper, model);

                case EFieldDataType.MultilineText:
                   return MultilineText(model);

                case EFieldDataType.Text:
                default:
                    return Text(model);
            }
        }

        private static MvcHtmlString DropDownList(HtmlHelper helper, LeadFieldModel model, bool deferred)
        {
            var dropdownlist = helper.Kendo()
                       .DropDownList()
                       .Name(model.FieldName)
                       .BindTo(model.ParsedDataSource)
                       .Deferred(deferred)
                       .ToHtmlString();

            var tag = new DivTag().AppendHtml(dropdownlist);

            return CreateFormGroup(model, tag);
        }

        private static MvcHtmlString CheckBox(HtmlHelper helper, LeadFieldModel model)
        {
            var checkbox = helper.CheckBox(model.FieldName, model.GetDefaultValue<bool>())
                       .ToHtmlString();

            var label = new HtmlTag("label")
                .Attr("for", model.FieldName)
                .AppendHtml(model.RequiredLabel);

            var tag = new DivTag()
                 .AddClass("checkbox")
                 .AppendHtml(checkbox);

            // Special case for checkbox: add label inside div.checkbox
            tag.Children.Add(label);

            var formGroup = new DivTag().AddClass("form-group");
            formGroup.Children.Add(tag);
            return MvcHtmlString.Create(formGroup.ToHtmlString());
        }

        private static MvcHtmlString MultilineText(LeadFieldModel model)
        {
            var tag = new HtmlTag("textarea")
                .NameAndId(model.FieldName)
                .Text(model.DefaultValue)
                .AddClass("form-control")
                .Placeholder(model.Label)
                .Attr("maxlength", model.DataType.MaxLength())
                .Attr("rows", 4)
                .AttrIfTrue(model.IsRequired, "required", "");

            return CreateFormGroup(model, tag);
        }

        private static MvcHtmlString Text(LeadFieldModel model)
        {
            var tag = new TextboxTag(model.FieldName, model.DefaultValue)
                .AddClass("form-control")
                .Placeholder(model.Label)
                .Attr("maxlength", model.DataType.MaxLength())
                .AttrIfTrue(model.IsRequired, "required", "");

            return CreateFormGroup(model, tag);
        }

        private static MvcHtmlString CreateFormGroup(LeadFieldModel model, HtmlTag tag)
        {
            var label = new HtmlTag("label").AppendHtml(model.RequiredLabel);
            var formGroup = new DivTag().AddClass("form-group");
            formGroup.Children.Add(label);
            formGroup.Children.Add(tag);
            return MvcHtmlString.Create(formGroup.ToHtmlString());
        }
    }
}
