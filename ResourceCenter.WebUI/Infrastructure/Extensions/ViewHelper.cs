﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using HtmlTags;
using MvcSiteMapProvider;

namespace ResourceCenter.WebUI.Infrastructure.Extensions
{
    public static class ViewHelper
    {
        public static string AdminLayout => "~/Views/Shared/_LayoutAdmin.cshtml";

        public static IHtmlString RenderPageScript(string page)
        {
            if (!page.EndsWith(".js"))
            {
                page += ".js";
            }
            return Scripts.Render($"/scripts/app/pages/{page}");
        }

        public static IHtmlString RenderStyles()
        {
            var sb = new StringBuilder();
            sb.AppendLine(CreateFontLink("Open+Sans:400,300,600,700"));
            sb.AppendLine(CreateFontLink("Roboto:400,300,500,700"));
            sb.AppendLine(Styles.Render($"~/Content/css/all").ToHtmlString());
            return new HtmlString(sb.ToString());
        }

        public static IHtmlString RenderScripts(string type = "admin")
        {
            return Scripts.Render($"~/bundles/js-{type}");
        }

        public static string PageTitle(this HtmlHelper helper)
        {
            var pageTitle = helper.ViewBag.Title ?? SiteMaps.Current.CurrentNode?.Title;
            return string.Format("{0}Resource Center", string.IsNullOrEmpty(pageTitle) ? "" : pageTitle + " - ");
        }

        public static IHtmlString RenderGoogleMapScript(this HtmlHelper helper, string callback)
        {
            var tag = new HtmlTag("script")
                .Attr("src", $"https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBdngeE_HR-s6NYAvMLaeg0Z_WNHKroDYk&callback={callback}")
                .Attr("async", "")
                .Attr("defer", "");

            return helper.Raw(tag);
        }

        private static string CreateFontLink(string font)
        {
            return new HtmlTag("link")
                .Attr("href", $"https://fonts.googleapis.com/css?family={font}")
                .Attr("rel", "stylesheet")
                .Attr("type", "text/css")
                .ToHtmlString();
        }
    }
}