// include gulp
var gulp = require('gulp'),
    del = require('del');

gulp.task('clean:css', function () {
    return del([
        './Content/vendors/**/*',
        './Content/fonts/**/*'
    ]);
});

gulp.task('clean:js', function () {
    return del(['./Scripts/libs/**/*']);
});

gulp.task('copy:js', ['clean:js'], function () {
    return gulp.src([
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        './bower_components/toastr/toastr.min.js',
        './bower_components/toastr/toastr.js.map',
        './bower_components/sweetalert2/dist/sweetalert2.min.js',
        './bower_components/es6-promise/es6-promise.min.js',
        './bower_components/ladda/dist/ladda.jquery.min.js',
        './bower_components/ladda/dist/spin.min.js',
        './bower_components/clipboard/dist/clipboard.min.js'
    ]).pipe(gulp.dest('./Scripts/libs'));
});

gulp.task('copy:css', ['clean:css'], function () {
    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap/dist/css/bootstrap.css.map',
        './bower_components/font-awesome/css/font-awesome.css',
        './bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        './bower_components/toastr/toastr.css',
        './bower_components/sweetalert2/dist/sweetalert2.css',
        './bower_components/ladda/dist/ladda-themeless.min.css'
    ]).pipe(gulp.dest('./Content/vendors'));
    
      gulp.src([
        './bower_components/bootstrap/dist/fonts/**/*',
        './bower_components/font-awesome/fonts/**/*'
    ]).pipe(gulp.dest('./Content/fonts'));
});

gulp.task('default', ['copy:js', 'copy:css']);