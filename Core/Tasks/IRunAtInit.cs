﻿
namespace Core.Tasks
{
    public interface IRunAtInit : IOrderedTask
    {
        void Execute();
    }
}
