namespace Core.Infrastructure.Objects
{
    public class KeyValue<TK, TV>
    {
        /// <summary>
        /// Name.
        /// </summary>
        public TV Value { get; set; }

        /// <summary>
        /// Value.
        /// </summary>
        public TK Key { get; set; }
    }

    public class KeyValue : KeyValue<string, string>
    {
    }
}