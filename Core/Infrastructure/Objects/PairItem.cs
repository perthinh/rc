using System;

namespace Core.Infrastructure.Objects
{
    /// <summary>
    /// Can be used to store Name/Value (or Key/Value) pairs.
    /// </summary>
    public class PairInt : TextValuePair<int>
    {
    }

    public class PairGuid : TextValuePair<Guid>
    {
    }

    public class PairString : TextValuePair<string>
    {
    }

    [Serializable]
    public class TextValuePair<TV> : PairItem<string, TV>
    {
    }

    public class PairItem<TT, TV>
    {
        /// <summary>
        /// Name.
        /// </summary>
        public TT Text { get; set; }

        /// <summary>
        /// Value.
        /// </summary>
        public TV Value { get; set; }
    }
}