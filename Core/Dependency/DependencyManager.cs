using System;
using System.Collections.Generic;

namespace Core.Dependency
{
    public static class DependencyManager
    {
        private static Func<IIocManager> FuncInstance { get; set; }
        public static IIocManager Current => FuncInstance();

        public static void SetCurrent(Func<IIocManager> funcInstance)
        {
            FuncInstance = funcInstance;
        }

        public static T GetInstance<T>() where T : class
        {
            return Current.GetInstance<T>();
        }

        public static object GetInstance(Type type)
        {
            return Current.GetInstance(type);
        }

        public static object GetAllInstances(Type type)
        {
            return Current.GetAllInstances(type);
        }

        public static IEnumerable<T> GetAllInstances<T>() where T : class
        {
            return Current.GetAllInstances<T>();
        }
    }
}