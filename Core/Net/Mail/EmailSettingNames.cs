﻿namespace Core.Net.Mail
{
    public static class EmailSettingNames
    {
        public const string DefaultFromAddress = "Mail.DefaultFromAddress";

        public const string DefaultFromDisplayName = "Mail.DefaultFromDisplayName";

        public static class Smtp
        {
            public const string Host = "Mail.Smtp.Host";

            public const string Port = "Mail.Smtp.Port";

            public const string UserName = "Mail.Smtp.UserName";

            public const string Password = "Mail.Smtp.Password";

            public const string Domain = "Mail.Smtp.Domain";

            public const string EnableSsl = "Mail.Smtp.EnableSsl";

            public const string UseDefaultCredentials = "Mail.Smtp.UseDefaultCredentials";
        }
    }
}