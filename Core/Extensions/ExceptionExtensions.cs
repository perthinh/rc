﻿using System;

namespace Core.Extensions
{
    public static class ExceptionExtensions
    {
        public static string GetBaseErrorMessage(this Exception ex)
        {
            return ex.GetBaseException().Message;
        }
    }
}
