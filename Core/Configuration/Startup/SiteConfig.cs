using Core.Auditing;
using Core.Dependency;
using Core.Localization;
using Core.Net.Mail;

namespace Core.Configuration.Startup
{
    public class SiteConfig : ISiteConfig
    {
        public ISettingManager Settings { get; }
        public ILocalizationConfig Localization { get; }
        public IEmailSender EmailSender { get; }
        public IAuditingConfig Auditing { get; }

        public SiteConfig()
        {
            Localization = DependencyManager.GetInstance<ILocalizationConfig>();
            EmailSender = DependencyManager.GetInstance<IEmailSender>();
            Auditing = DependencyManager.GetInstance<IAuditingConfig>();
            Settings = DependencyManager.GetInstance<ISettingManager>();
        }
    }
}