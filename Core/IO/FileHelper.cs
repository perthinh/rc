﻿using System.IO;

namespace Core.IO
{
    /// <summary>
    /// A helper class for File operations.
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Checks and deletes given file if it does exists.
        /// </summary>
        /// <param name="filePath">Path of the file</param>
        public static bool DeleteIfExists(string filePath)
        {
            var exists = File.Exists(filePath);
            if (exists)
            {
                File.Delete(filePath);
            }
            return exists;
        }
    }
}
