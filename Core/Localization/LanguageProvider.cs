﻿using CuttingEdge.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Localization
{
    public class LanguageProvider : ILanguageProvider
    {
        public IList<LanguageInfo> Languages { get; }

        public LanguageProvider()
        {
            Languages = new List<LanguageInfo>();
        }

        public LanguageProvider Add(LanguageInfo languageInfo)
        {
            Condition.Requires(languageInfo).IsNotNull();

            if (Languages.Any(x => string.Equals(x.Name, languageInfo.Name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new SystemException($"Duplicate language info {languageInfo.Name}");
            }
            Languages.Add(languageInfo);
            return this;
        }

        public LanguageProvider AddRange(IList<LanguageInfo> languages)
        {
            foreach (var languageInfo in languages)
            {
                Add(languageInfo);
            }
            return this;
        }

        public LanguageProvider WithDefault()
        {
            Languages.Add(new LanguageInfo("en-US", "English", "flag-en-us", isDefault: true));
            return this;
        }
    }
}