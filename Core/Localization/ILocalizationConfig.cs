﻿using Core.Localization.Sources;

namespace Core.Localization
{
    public interface ILocalizationConfig
    {
        ILanguageProvider LanguageProvider { get; }
        ILocalizationSource Source { get; set; }
        bool IsEnabled { get; set; }
        bool ReturnGivenTextIfNotFound { get; set; }
        bool WrapGivenTextIfNotFound { get; set; }
        bool TitleizeGivenTextIfNotFound { get; set; }
    }
}