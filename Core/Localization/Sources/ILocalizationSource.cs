﻿using System.Globalization;

namespace Core.Localization.Sources
{
    public interface ILocalizationSource
    {
        string GetString(string name);
        string GetStringOrNull(string name);
        string GetString(string name, CultureInfo cultureInfo);
        string GetStringOrNull(string name, CultureInfo cultureInfo);
    }
}