﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Localization.Sources
{
    public static class LocalizationSourceExtensions
    {
        public static string GetModelString(this ILocalizationSource source, Type containerType, string propertyName,
            string metadataName = null)
        {
            var text = source.GetModelStringOrNull(containerType, propertyName, metadataName);
            if (string.IsNullOrWhiteSpace(text))
            {
                text = LocalizationSourceHelper.ReturnGivenNameOrThrowException(
                            LocalizationSourceHelper.GenerateModelName(containerType, propertyName, metadataName));
            }
            return text;
        }

        public static string GetModelStringOrNull(this ILocalizationSource source, Type containerType, string propertyName, string metadataName = null)
        {
            var name = LocalizationSourceHelper.GenerateModelName(containerType, propertyName, metadataName);
            var text = source.GetStringOrNull(name);
            if (string.IsNullOrWhiteSpace(text))
            {
                text = source.GetStringOrNull(LocalizationSourceHelper.GenerateCommonName(propertyName, metadataName));
            }
            return text;
        }

        public static string ErrorMessage<T>(this ILocalizationSource source, T attribute, params object[] parameters) where T : ValidationAttribute
        {
            return GetErrorMessageOrDefault<T>(source, attribute.ErrorMessage, parameters);
        }

        public static string FormatErrorMessage<T>(this ILocalizationSource source, T attribute, params object[] parameters) where T : ValidationAttribute
        {
            return GetErrorMessageOrDefault<T>(source, attribute.FormatErrorMessage((string)parameters[0]), parameters);
        }

        private static string GetErrorMessageOrDefault<T>(ILocalizationSource source, string defaultText, object[] parameters)
            where T : ValidationAttribute
        {
            var name = typeof(T).Name;
            var text = source.GetStringOrNull(name);
            return string.IsNullOrWhiteSpace(text) ? defaultText : string.Format(text, parameters);
        }
    }
}
