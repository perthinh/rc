﻿using System;
using System.Runtime.Serialization;

namespace Core.Exceptions
{
    [Serializable]
    public class SysException : Exception
    {
        /// <summary>
        /// Creates a new <see cref="SysException"/> object.
        /// </summary>
        public SysException()
        {
            
        }

        /// <summary>
        /// Creates a new <see cref="SysException"/> object.
        /// </summary>
        public SysException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        {

        }

        /// <summary>
        /// Creates a new <see cref="SysException"/> object.
        /// </summary>
        /// <param name="message">Exception message</param>
        public SysException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Creates a new <see cref="SysException"/> object.
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception</param>
        public SysException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
