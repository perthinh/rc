﻿using CacheManager.Core;
using System;

namespace Core.Runtime.Caching.Extensions
{
    public static class CacheManagerExtensions
    {
        private static CacheItem<T> CreateItem<T>(string key, T value, string region = null)
        {
            return string.IsNullOrWhiteSpace(region)
                ? new CacheItem<T>(key, value)
                : new CacheItem<T>(key, region, value);
        }

        public static void Put(this ICacheManager<object> cacheManager, string key, object value, CachePolicy policy, string region = null)
        {
            cacheManager.Put<object>(key, value, policy, region);
        }

        public static void Put<T>(this ICacheManager<T> cacheManager, string key, T value, CachePolicy policy, string region = null)
        {
            var item = CreateItem(key, value, region);
            switch (policy.Mode)
            {
                case CacheExpirationMode.Duration:
                    item.WithAbsoluteExpiration(DateTimeOffset.Now.Add(policy.Duration));
                    break;
                case CacheExpirationMode.Sliding:
                    item.WithSlidingExpiration(policy.SlidingExpiration);
                    break;
                case CacheExpirationMode.Absolute:
                    item.WithAbsoluteExpiration(policy.AbsoluteExpiration);
                    break;
                default:
                    item.WithNoExpiration();
                    break;
            }

            cacheManager.Put(item);
        }
    }
}