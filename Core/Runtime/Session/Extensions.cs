﻿namespace Core.Runtime.Session
{
    public static class Extensions
    {
        public static int? CurrentUserId(this IUserSession session)
        {
            return session.ImpersonatorUserId ?? session.UserId;
        }
    }
}
