﻿namespace Core.Runtime.Session
{
    public interface IUserSession
    {
        int? TenantId { get; }
        int? ImpersonatorTenantId { get; }

        int? UserId { get; }
        int? ImpersonatorUserId { get; }
    }
}
