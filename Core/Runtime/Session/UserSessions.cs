﻿using Core.Dependency;

namespace Core.Runtime.Session
{
    public static class UserSessions
    {
        public static IUserSession Current => DependencyManager.GetInstance<IUserSession>();
        public static int? UserId => Current.ImpersonatorUserId ?? Current.UserId;
        public static int? TenantId => Current.ImpersonatorTenantId ?? Current.TenantId;
    }
}
