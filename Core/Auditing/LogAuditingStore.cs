﻿using Serilog;
using System.Threading.Tasks;

namespace Core.Auditing
{
    public class LogAuditingStore : IAuditingStore
    {
        public Task SaveAsync(AuditInfo auditInfo)
        {
            Log.ForContext<LogAuditingStore>().Information(auditInfo.Exception, auditInfo.ToString());
            return Task.FromResult(0);
        }
    }
}