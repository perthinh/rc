﻿using Core.Extensions;
using Core.Runtime.Session;
using System.Reflection;

namespace Core.Auditing
{
    public static class AuditingHelper
    {
        public static bool ShouldSaveAudit(IAuditingConfig config, ICustomAttributeProvider method)
        {
            return config.IsEnabled &&
                   method != null && !method.HasAttribute<DisableAuditingAttribute>();
        }

        public static bool ShouldSaveAudit(IAuditingConfig config, IUserSession userSession)
        {
            return config.IsEnabled &&
                   (userSession.UserId.HasValue || (!userSession.UserId.HasValue && config.IsEnabledForAnonymousUsers));
        }
    }
}