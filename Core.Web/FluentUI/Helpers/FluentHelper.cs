﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Core.Web.FluentUI.Builders;

namespace Core.Web.FluentUI.Helpers
{
    public class FluentHelper<TModel>
    {
        protected readonly HtmlHelper<TModel> Helper;

        public FluentHelper(HtmlHelper<TModel> helper)
        {
            Helper = helper;
        }

        public GenericTagBuilder ParagraphFor(string name)
        {
            return new GenericTagBuilder("p", name);
        }

        public GenericTagBuilder ParagraphFor<TProp>(Expression<Func<TModel, TProp>> expression)
        {
            var text = ModelMetadata.FromLambdaExpression(expression, Helper.ViewData).Model?.ToString();
            return ParagraphFor(Helper.NameFor(expression).ToString()).Text(text);
        }
    }
}