﻿using System.Collections.Generic;
using HtmlTags;

namespace Core.Web.FluentUI.Helpers
{
    public static class HtmlTagExtensions
    {
        public static HtmlTag Attr(this HtmlTag htmlTag, IDictionary<string, object> attributes)
        {
            if (attributes != null)
            {
                foreach (var attribute in attributes)
                {
                    htmlTag.Attr(attribute.Key, attribute.Value);
                }
            }
            return htmlTag;
        }

        public static HtmlTag NameAndId(this HtmlTag htmlTag, string name)
        {
            return htmlTag.Name(name).Id(name);
        }
    }
}