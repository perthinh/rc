﻿using Core.Web.FluentUI.Models;

namespace Core.Web.FluentUI.Builders
{
    public class GenericTagBuilder : BuilderBase<GenericTagModel, GenericTagBuilder>
    {
        public GenericTagBuilder(string tagName, string name)
        {
            Model.TagName = tagName;
            Model.Name = name;
        }

        public GenericTagBuilder Text(string text)
        {
            Model.Text = text;
            return this;
        }
    }
}