using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Web.Mvc.Models
{
    public abstract class ModelValidator<T>
    {
        protected T Model { get; }
        protected ValidationContext ValidationContext { get; }
        protected IList<ValidationResult> ValidationResults { get; set; }

        protected ModelValidator(T model, ValidationContext validationContext)
        {
            Model = model;
            ValidationContext = validationContext;
            ValidationResults = new List<ValidationResult>();
        }

        protected abstract void ValidateCore();

        public virtual IList<ValidationResult> Validate()
        {
            ValidateCore();
            return ValidationResults;
        }
    }
}