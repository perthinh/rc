﻿using System.Web.Mvc;

namespace Core.Web.Mvc.Alerts
{
    public class AlertDecoratorResult : ActionResult
    {
        public ActionResult InnerResult { get; set; }
        public AlertType AlertType { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }

        public AlertDecoratorResult(ActionResult innerResult, AlertType alertType, string message, string title = null)
        {
            InnerResult = innerResult;
            AlertType = alertType;
            Message = message;
            Title = title;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var alerts = context.Controller.TempData.GetAlerts();
            alerts.Add(new Alert
            {
                AlertType = AlertType,
                Message = Message,
                Title = Title
            });
            InnerResult.ExecuteResult(context);
        }
    }
}