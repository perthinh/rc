﻿namespace Core.Web.Mvc.Alerts
{
    public class Alert
	{
		public AlertType AlertType { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }

		public string GetAlertType()
		{
			return AlertType.ToString().ToLower();
		}

	}
}