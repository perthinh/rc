﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Core.Web.Caching
{
    public class SessionCache : ISessionCache
    {
        private HttpSessionState Session => HttpContext.Current.Session;

        public void Set(string key, object value)
        {
            Session[key] = value;
        }

        public bool Contains(string key)
        {
            return Session[key] != null;
        }

        public void Clear()
        {
            Session.Clear();
        }

        public object Get(string key)
        {
            return Session[key];
        }

        public T Get<T>(string key)
        {
            if (!Contains(key))
            {
                return default(T);
            }
            return (T)Get(key);
        }

        public T GetOrAdd<T>(string key, T value)
        {
            AddIfNotExists(key, value);
            return (T)Get(key);
        }

        public T GetOrAdd<T>(string key, Func<T> funcGetValue)
        {
            if (!Contains(key))
            {
                Session[key] = funcGetValue();
            }
            return (T)Get(key);
        }

        public void Remove(string key)
        {
            Session.Remove(key);
        }

        private void AddIfNotExists(string key, object value)
        {
            if (!Contains(key))
            {
                Session[key] = value;
            }
        }
    }
}