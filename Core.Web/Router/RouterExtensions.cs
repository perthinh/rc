using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using Core.Extensions;

namespace Core.Web.Router
{
    public static class RouterExtensions
    {
        public static string GetSiteMapUrl(this HtmlHelper htmlHelper, MvcRoute node)
        {
            if (!node.Linkable) return "#";

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            return urlHelper.Action(node.Action, node.Controller, new { area = node.Area });
        }

        public static bool IsInCurrentPath(this MvcRoute node)
        {
            var currentNode = node.Router.CurrentNode;
            return node == currentNode ||
                   currentNode.GetParentNodes().Any(parent => node == parent);
        }

        public static string ActiveClass(this MvcRoute node)
        {
            return node.IsInCurrentPath() ? "active" : string.Empty;
        }

        internal static MvcRoute ToSiteMapNode(this XElement element)
        {
            var node = new MvcRoute
            {
                Controller = element.GetAttributeValue("controller"),
                Action = element.GetAttributeValue("action", "Index"),
                Area = element.GetAttributeValue("area", null),
                Icon = element.GetAttributeValue("icon"),
                Description = element.GetAttributeValue("description"),
                Title = element.GetAttributeValue("title"),
                Visible = element.GetAttributeValue("visible", true),
            };

            node.Linkable = !string.IsNullOrWhiteSpace(node.Controller) &&
                            element.GetAttributeValue("linkable", true);

            return node;
        }

        private static T GetAttributeValue<T>(this XElement element, string attribute, T defaultValue = default(T))
        {
            return element.GetAttributeValue(attribute).ChangeTypeTo<T>(defaultValue);
        }

        private static string GetAttributeValue(this XElement element, string attribute, string defaultValue = "")
        {
            var attr = element.Attribute(attribute);
            return attr?.Value ?? defaultValue;
        }
    }
}