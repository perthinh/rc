namespace Core.Web.Router.Providers
{
    public interface IRouteProvider
    {
        MvcRouter Load();
    }
}