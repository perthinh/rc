﻿using System.Web;
using Core.Extensions;

namespace Core.Web.Router
{
    internal static class KeyGenerator
    {
        public static string FromSiteMapNode(MvcRoute node)
        {
            return GenerateKey(node.Action, node.Controller, node.Area);
        }

        public static string FromCurrentRoute()
        {
            if (HttpContext.Current == null)
            {
                return string.Empty;
            }

            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
         
            return GenerateKey(routeValues["action"].ChangeTypeTo<string>(string.Empty),
                               routeValues["controller"].ChangeTypeTo<string>(string.Empty),
                               routeValues["area"].ChangeTypeTo<string>(string.Empty));
        }

        private static string GenerateKey(string action, string controller, string area)
        {
            return $"{action}_{controller}_{area}".ToLower();
        }
    }
}