using System;

namespace Core.Web.MetadataModel.Validations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ImageUploadAttribute : FileUploadAttribute
    {
        public ImageUploadAttribute()
        {
            AllowedExtensions = new[] { ".gif", ".jpg", ".png", ".jpeg" };
            MaxSizeInMB = 3;
        }
    }
}