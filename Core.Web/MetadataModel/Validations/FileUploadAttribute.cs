using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Validations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FileUploadAttribute : ValidationAttribute, IMetadataAware
    {
        public const int DefaultMaxSizeInMB = 10;
        public int MaxSizeInMB { get; set; } = DefaultMaxSizeInMB;
        public bool IsRequired { get; set; } = true;
        public string[] AllowedExtensions { get; set; }

        public FileUploadAttribute()
        {
            ErrorMessage = "{0} is invalid";
        }

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null && !IsRequired)
            {
                return true;
            }

            return file?.ContentLength <= MaxSizeInMB * 1024 * 1024;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            if (string.IsNullOrWhiteSpace(metadata.DataTypeName))
            {
                metadata.DataTypeName = DataType.Upload.ToString();
            }
        }
    }
}