﻿using System.ComponentModel.DataAnnotations;

namespace Core.Web.MetadataModel.Validations
{
    public class DependencyRequiredAttribute : ValidationAttribute
    {
        public string OtherProperty { get; }

        public DependencyRequiredAttribute(string otherProperty)
        {
            OtherProperty = otherProperty;
            ErrorMessage = "{0} is required";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var otherPropValue = context.ObjectInstance.GetType().GetProperty(OtherProperty).GetValue(context.ObjectInstance, null);
            if (!string.IsNullOrEmpty(otherPropValue?.ToString()) &&
                string.IsNullOrEmpty(value?.ToString()))
            {
                return new ValidationResult(FormatErrorMessage(context.DisplayName));
            }
            return ValidationResult.Success;
        }
    }
}
