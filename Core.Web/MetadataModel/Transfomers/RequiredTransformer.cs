﻿using Core.Extensions;
using Core.Web.MetadataModel.Common;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public class RequiredTransformer : GenericTransformer<RequiredAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Merge("required", string.Empty);
            dict.AddRuleMessage("required", ErrorMessage(metadata, attribute));
        }
    }
}