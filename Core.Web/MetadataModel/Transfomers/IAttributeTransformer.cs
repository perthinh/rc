﻿using Core.Web.MetadataModel.Common;
using System;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public interface IAttributeTransformer
    {
        void Transform(ModelMetadata metadata, Attribute attribute, HtmlAttributeDictionary dict);
    }
}