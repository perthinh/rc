﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class DependencyRequiredTransformer : GenericTransformer<DependencyRequiredAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, DependencyRequiredAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("dependency", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("deprequired", ErrorMessage(metadata, attribute));
        }
    }
}
