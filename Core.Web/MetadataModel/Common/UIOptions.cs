﻿using System;
using Core.Web.MetadataModel.Attributes;

namespace Core.Web.MetadataModel.Common
{
    public class UIOptions
    {
        public bool Deferred { get; set; } = true;
        public bool ShowLabel { get; set; } = true;
        public string HelpText { get; set; }
        public string OptionLabel { get; set; }
        public bool FutureDate { get; set; }
        public bool Disabled { get; set; }

        public static UIOptions ConvertToOptions(UIOptionsAttribute attribute)
        {
            var options = new UIOptions();
            if (attribute != null)
            {
                options.Deferred = attribute.Deferred;
                options.OptionLabel = attribute.OptionLabel;
                options.ShowLabel = attribute.ShowLabel;
                options.HelpText = attribute.HelpText;
                options.FutureDate = attribute.FutureDate;
                options.Disabled = attribute.Disabled;
            }
            return options;
        }
    }
}
