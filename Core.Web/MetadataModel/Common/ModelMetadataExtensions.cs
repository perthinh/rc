using System;
using HtmlTags;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Core.Utils;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Common
{
    public static class ModelMetadataExtensions
    {
        private const string KeyAdditionalAttribute = "__KeyAdditionalAttribute";
        private const string KeyUIOptions = "__KeyUIOptions";

        public static HtmlAttributeDictionary AdditionalAttributes(this ModelMetadata metadata)
        {
            if (!metadata.AdditionalValues.ContainsKey(KeyAdditionalAttribute))
            {
                metadata.AdditionalValues.Add(KeyAdditionalAttribute, new HtmlAttributeDictionary());
            }
            return metadata.AdditionalValues[KeyAdditionalAttribute] as HtmlAttributeDictionary;
        }

        public static UIOptions Options(this ModelMetadata metadata)
        {
            return metadata.AdditionalValues[KeyUIOptions] as UIOptions;
        }

        public static void SetOptions(this ModelMetadata metadata, UIOptions options)
        {
            metadata.AdditionalValues[KeyUIOptions] = options;
        }

        public static HtmlAttributeDictionary ModelHtmlAttributes(this HtmlHelper helper)
        {
            var userHtmlAttributes = helper.ViewData["htmlAttributes"] ?? new { };
            var metadata = helper.ViewData.ModelMetadata;

            var result = new HtmlAttributeDictionary();
            if (!metadata.IsHidden())
            {
                result.Placeholder(metadata.Watermark);
            }
            result.Merge(metadata.AdditionalAttributes());
            result.Merge(userHtmlAttributes);

            return result;
        }

        public static HtmlAttributeDictionary ModelHtmlAttributesFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            var result = new HtmlAttributeDictionary();
            var metadata = helper.ViewData.ModelMetadata.Properties.SingleOrDefault(x => x.PropertyName == LamdaHelper.PropertyName(expression));

            if (metadata == null)
            {
                return result;
            }

            if (!metadata.IsHidden())
            {
                result.Placeholder(metadata.Watermark);
            }
            result.Merge(metadata.AdditionalAttributes());
            return result;
        }

        public static MvcHtmlString DisplayLabel(this ModelMetadata metadata)
        {
            var spanTag = new HtmlTag("span")
                .Text(" *")
                .Style("visibility", metadata.IsRequired() ? "visible" : "hidden");

            return MvcHtmlString.Create(string.Format("{0}{1}", metadata.DisplayName, spanTag.ToHtmlString()));
        }

        public static bool IsBoolean(this ModelMetadata metadata)
        {
            return metadata.ModelType == typeof(bool) || metadata.ModelType == typeof(bool?);
        }

        public static bool IsHidden(this ModelMetadata metadata)
        {
            return metadata.TemplateHint != null && metadata.TemplateHint.StartsWith("Hidden");
        }

        private static bool IsRequired(this ModelMetadata metadata)
        {
            if (!metadata.ModelType.IsValueType && metadata.IsRequired) return true;

            var hasRequiredAttr = metadata.ContainerType
                .GetProperty(metadata.PropertyName)
                .GetCustomAttributes(typeof(RequiredAttribute), false)
                .Any();

            var required = metadata.ModelType.IsValueType && hasRequiredAttr;
            if (required) return true;

            var uploadAttribute = metadata.ContainerType
                .GetProperty(metadata.PropertyName)
                .GetCustomAttributes(typeof(FileUploadAttribute), false)
                .FirstOrDefault() as FileUploadAttribute;

            return uploadAttribute?.IsRequired ?? false;
        }
    }
}