using System.Web.Mvc;

namespace Core.Web.MetadataModel.MetadataAware
{
    public class TextOnlyAttribute : MetadataAwareAttribute
    {
        public override void OnMetadataCreated(ModelMetadata metadata)
        {
            if (string.IsNullOrWhiteSpace(metadata.TemplateHint))
            {
                metadata.TemplateHint = "TextOnly";
            }
        }
    }
}