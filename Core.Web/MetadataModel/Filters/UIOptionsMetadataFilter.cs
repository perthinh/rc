﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.Common;

namespace Core.Web.MetadataModel.Filters
{
    public class UIOptionsMetadataFilter : IModelMetadataFilter
    {
        public void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes)
        {
            var attribute = attributes.OfType<UIOptionsAttribute>().FirstOrDefault();
            metadata.SetOptions(UIOptions.ConvertToOptions(attribute));
        }
    }
}