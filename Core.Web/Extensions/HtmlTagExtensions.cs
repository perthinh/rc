﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using HtmlTags;

namespace Core.Web.Extensions
{
    public static class HtmlTagExtensions
    {
        public static HtmlTag Attr(this HtmlTag htmlTag, IDictionary<string, object> attributes)
        {
            if (attributes != null)
            {
                foreach (var attribute in attributes)
                {
                    if (string.Equals(attribute.Key, "style", StringComparison.OrdinalIgnoreCase))
                    {
                        var styles = attribute.Value.ChangeTypeTo<string>().ToKeyValues();
                        foreach (var item in styles)
                        {
                            htmlTag.Style(item.Key, item.Value);
                        }
                    }
                    else
                    {
                        htmlTag.Attr(attribute.Key, attribute.Value);
                    }
                }
            }
            return htmlTag;
        }

        public static HtmlTag NameAndId(this HtmlTag htmlTag, string name)
        {
            return htmlTag.Name(name).Id(name);
        }

        public static HtmlTag AttrIfNotNull(this HtmlTag htmlTag, string key, object value)
        {
            if (value != null)
            {
                htmlTag.Attr(key, value);
            }
            return htmlTag;
        }

        public static HtmlTag AttrIfTrue(this HtmlTag htmlTag, bool condition, string key, object value)
        {
            if (condition)
            {
                htmlTag.Attr(key, value);
            }
            return htmlTag;
        }

        public static HtmlTag Placeholder(this HtmlTag htmlTag, object value)
        {
            return htmlTag.AttrIfNotNull("placeholder", value);
        }
    }
}