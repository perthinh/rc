﻿using Core.Utils;
using Core.Web.MetadataModel.Common;
using HtmlTags;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Core.Extensions;

namespace Core.Web.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString BoostrapLabel(this HtmlHelper helper, string propName, bool horizontalForm = true)
        {
            var metadata = helper.ViewData.ModelMetadata.Properties.FirstOrDefault(x => x.PropertyName == propName);
            var labelTag = new HtmlTag("label");
            if (horizontalForm)
            {
                labelTag.AddClass("col-lg-2 control-label");
            }
            labelTag.AppendHtml(metadata.DisplayLabel().ToHtmlString());
            return MvcHtmlString.Create(labelTag.ToHtmlString());
        }

        public static MvcHtmlString BoostrapLabelFor<TModel, TProp>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProp>> expression, bool horizontalForm = true)
        {
            return BoostrapLabel(helper, LamdaHelper.PropertyName(expression), horizontalForm);
        }

        public static MvcHtmlString ErrorSummary(this HtmlHelper helper, bool excludePropertyErrors = false, string message = "")
        {
            var modelState = helper.ViewData.ModelState;
            if (modelState.IsValid) return MvcHtmlString.Empty;

            var ulTag = new HtmlTag("ul");
            var divTag = new DivTag()
                    .AddClass("alert alert-danger text-left");

            divTag.Children.Add(ulTag);

            if (!excludePropertyErrors)
            {
                foreach (var error in modelState.Where(x => x.Key != message)
                                                .SelectMany(x => x.Value.Errors))
                {
                    ulTag.Children.Add(new HtmlTag("li").Text(error.ErrorMessage));
                }
            }

            if (modelState[message] != null)
            {
                foreach (var error in modelState[message].Errors)
                {
                    ulTag.Children.Add(new HtmlTag("li").Text(error.ErrorMessage));
                }
            }
            return MvcHtmlString.Create(divTag.ToHtmlString());
        }

        public static MvcForm FormPost(this HtmlHelper helper, bool horizontal = false, object htmlAttributes = null)
        {
            var dict = new HtmlAttributeDictionary
            {
                ["novalidate"] = "",
                ["id"] = "form"
            };

            dict.Merge(htmlAttributes);
            if (horizontal)
            {
                dict.Attr("class", "form-horizontal");
            }

            return BeginForm(helper, dict);
        }
        public static MvcForm FormPostInline(this HtmlHelper helper, object htmlAttributes = null)
        {
            var dict = new HtmlAttributeDictionary
            {
                ["novalidate"] = "",
                ["id"] = "form",
                ["class"] = "form-inline"
            };
            dict.Merge(htmlAttributes);
            return BeginForm(helper, dict);
        }

        private static MvcForm BeginForm(HtmlHelper helper, HtmlAttributeDictionary htmlAttributes)
        {
            var routeData = helper.ViewContext.RouteData;
            return helper.BeginForm(routeData.Values["action"].ToString(),
                routeData.Values["controller"].ToString(),
                FormMethod.Post, htmlAttributes);
        }

        public static MvcHtmlString HelpText(this HtmlHelper helper, ModelMetadata metadata)
        {
            var helpText = metadata.Options().HelpText;
            return !string.IsNullOrWhiteSpace(helpText)
                ? MvcHtmlString.Create($"<span class='help-block m-b-none'> {helpText}</span>")
                : MvcHtmlString.Empty;
        }

        public static MvcHtmlString InvalidMessageFor<TModel, TProp>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProp>> expression)
        {
            return InvalidMessage(helper, helper.NameForModel().ToString());
        }
        public static MvcHtmlString InvalidMessage(this HtmlHelper helper, string name)
        {
            var tag = new HtmlTag("span")
                .AddClass("k-invalid-msg")
                .Data("for", name);

            return MvcHtmlString.Create(tag.ToHtmlString());
        }

        public static MvcHtmlString ModalPartial(this HtmlHelper helper, string modalId = "modal")
        {
            return helper.Partial("_ModalWindow", new ViewDataDictionary
            {
                ["ModalId"] = modalId
            });
        }

        /// <summary>
        /// PageClass give you ability to specify custom style for specific view based on action
        /// </summary>
        public static string PageClass(this HtmlHelper html)
        {
            return html.ViewContext.RouteData.Values["action"].ChangeTypeTo<string>();
        }
    }
}