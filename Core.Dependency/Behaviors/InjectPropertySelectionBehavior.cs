﻿using SimpleInjector.Advanced;
using System;
using System.Linq;
using System.Reflection;

namespace Core.Dependency.Behaviors
{
    internal class InjectPropertySelectionBehavior : IPropertySelectionBehavior
    {
        public bool SelectProperty(Type type, PropertyInfo prop)
        {
            return prop.GetCustomAttributes(typeof(InjectAttribute)).Any();
        }
    }
}