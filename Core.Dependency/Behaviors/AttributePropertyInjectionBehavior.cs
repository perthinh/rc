﻿using SimpleInjector.Advanced;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Core.Dependency.Behaviors
{
    internal sealed class AttributePropertyInjectionBehavior : IPropertySelectionBehavior
    {
        private readonly Type _attributeType;
        private readonly IPropertySelectionBehavior _baseBehavior;

        public AttributePropertyInjectionBehavior(IPropertySelectionBehavior baseBehavior, Type attributeType)
        {
            _baseBehavior = baseBehavior;
            _attributeType = attributeType;
        }

        [DebuggerStepThrough]
        public bool SelectProperty(Type serviceType, PropertyInfo property)
        {
            return IsPropertyDecoratedWithAttribute(property) ||
                   _baseBehavior.SelectProperty(serviceType, property);
        }

        [DebuggerStepThrough]
        private bool IsPropertyDecoratedWithAttribute(PropertyInfo property)
        {
            return property.GetCustomAttributes(_attributeType, true).Any();
        }
    }
}