﻿using System;
using Core.Auditing;
using Core.Configuration;
using Core.Configuration.Startup;
using Core.Localization;
using Core.Localization.Sources;
using Core.Net.Mail.Smtp;
using Core.Runtime.Session;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Resources;
using Core.Net.Mail;

namespace Core.Dependency.Extensions
{
    public static class ContainerExtensions
    {
        [DebuggerStepThrough]
        public static void RegisterLocalization(this Container container, IList<ResourceManager> sources, IList<LanguageInfo> languages = null)
        {
            container.RegisterSingleton<ILocalizationConfig, LocalizationConfig>();
            container.RegisterSingleton<ILanguageManager, LanguageManager>();

            if (languages == null || !languages.Any())
            {
                container.RegisterSingleton<ILanguageProvider>(() => new LanguageProvider().WithDefault());
            }
            else
            {
                container.RegisterSingleton<ILanguageProvider>(() => new LanguageProvider().AddRange(languages));
            }

            container.RegisterSingleton<ILocalizationSource>(() => new ResourseFilesLocalizationSource(sources));
        }

        [DebuggerStepThrough]

        public static void RegisterDependencyManager(this Container container)
        {
            container.RegisterSingleton<IIocManager>(new DefaultIocManager(container));
            DependencyManager.SetCurrent(container.GetInstance<IIocManager>);
        }

        [DebuggerStepThrough]

        public static void RegisterDefaultDepdendencies(this Container container, IList<ResourceManager> sources, IList<LanguageInfo> languages = null)
        {
            container.RegisterSingleton<ISiteConfig, SiteConfig>();

            container.Register<IEmailSender, SmtpEmailSender>(Lifestyle.Scoped);
            container.Register<IAuditingStore, LogAuditingStore>(Lifestyle.Scoped);

            container.RegisterSingleton<IUserSession, ClaimsUserSession>();
            container.RegisterSingleton<ISmtpEmailSenderConfig, SmtpEmailSenderConfig>();
            container.RegisterSingleton<ISettingManager, FileSettingManager>();
            container.RegisterSingleton<IAuditingConfig, AuditingConfig>();

            container.RegisterLocalization(sources, languages);
        }

        [DebuggerStepThrough]

        public static Container SimpleInjectorContainer(this IIocManager manager)
        {
            return manager.GetContainer() as Container;
        }

        [DebuggerStepThrough]
        public static Scope BeginScope(this IIocManager manager)
        {
            return manager.SimpleInjectorContainer().BeginExecutionContextScope();
        }

        public static void RegisterDefaultConventions(this Container container, string assemblyPrefix, params string[] nsPrefixes)
        {
            var registrations = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.StartsWith(assemblyPrefix))
                .SelectMany(x => x.GetExportedTypes())
                .Where(type => (!nsPrefixes.Any() ||
                                nsPrefixes.Any(prefix => type.Namespace != null && type.Namespace.StartsWith(prefix))) &&
                                !type.IsInterface &&
                                !type.IsAbstract &&
                                type.GetInterfaces().Any() &&
                                type.Name.EndsWith("Service"))
                .Select(type => new
                {
                    Service = type.GetInterfaces().FirstOrDefault(x => x.Name.EndsWith(type.Name)),
                    Implementation = type
                })
                .Where(x => x.Service != null && x.Service.FullName.StartsWith(assemblyPrefix));

            foreach (var reg in registrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }
        }
    }
}