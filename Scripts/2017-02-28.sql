CREATE DATABASE ResourceCenter
GO
Use ResourceCenter
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Asset](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Alias] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FilePath] [nvarchar](255) NOT NULL,
	[Thumbnail] [nvarchar](255) NULL,
	[Published] [bit] NOT NULL,
	[IsGated] [bit] NOT NULL,
	[IsCobranded] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[FormId] [int] NULL,
	[CategoryId] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Asset_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [AssetCounter](
	[AssetId] [int] NOT NULL,
	[ViewCount] [int] NOT NULL,
	[DownloadCount] [int] NOT NULL,
 CONSTRAINT [PK_AssetCounter] PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [AssetDownload](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssetId] [int] NULL,
	[SubscriberId] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
	[LeadContent] [nvarchar](4000) NULL,
	[IPAddress] [varchar](50) NOT NULL,
	[IsLead] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AssetDownload] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [AssetPartnerType](
	[AssetId] [int] NOT NULL,
	[PartnerTypeId] [int] NOT NULL,
 CONSTRAINT [PK_AssetPartnerType] PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC,
	[PartnerTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [AssetTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssetId] [int] NOT NULL,
	[AssetTitle] [nvarchar](100) NOT NULL,
	[ShortDescription] [nvarchar](255) NULL,
	[LongDescription] [nvarchar](1000) NULL,
	[LanguageCode] [varchar](15) NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_AssetTranslation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Alias] [nvarchar](100) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[ParentId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CategoryTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[LanguageCode] [varchar](15) NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_CategoryTranslation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Company](
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Logo] [nvarchar](255) NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[County] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [varchar](16) NULL,
	[Country] [nvarchar](50) NULL,
	[WebsiteUrl] [nvarchar](255) NULL,
	[ExternalCode] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[TenantId] [int] NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CompanyAttribute](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [nvarchar](100) NOT NULL,
	[IsMandatory] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_CompanyAttribute] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [CompanyAttributeValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttributeId] [int] NOT NULL,
	[Value] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_CompanyAttributeValue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL,
	[Application] [nvarchar](60) NOT NULL,
	[Host] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Source] [nvarchar](60) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[User] [nvarchar](50) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[TimeUtc] [datetime] NOT NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NOT NULL
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Form](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Form] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FormField](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FormId] [int] NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
	[DataType] [int] NOT NULL,
	[IsRequired] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_FormField] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FormFieldTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FormFieldId] [int] NOT NULL,
	[LanguageCode] [varchar](15) NOT NULL,
	[Label] [nvarchar](100) NOT NULL,
	[DataSource] [nvarchar](255) NULL,
	[DefaultValue] [nvarchar](255) NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_FormFieldTranslation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Invitation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegisteredUserId] [int] NULL,
	[EmailTo] [nvarchar](255) NOT NULL,
	[InvitationCode] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ExpiredDate] [datetime] NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Invitation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Language](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LanguageCode] [varchar](15) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Lead](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssetId] [int] NULL,
	[SubscriberId] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
	[LeadContent] [nvarchar](4000) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IPAddress] [varchar](50) NULL,
 CONSTRAINT [PK_Lead] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Message](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[Body] [nvarchar](4000) NULL,
	[Status] [int] NOT NULL,
	[FolderId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[SentBy] [int] NULL,
	[RecipientId] [int] NOT NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PartnerType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PartnerTypeTranslation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PartnerTypeId] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[LanguageCode] [varchar](15) NOT NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_PartnerTypeTranslation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Subscription](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[SubscriberId] [int] NOT NULL,
	[PartnerTypeId] [int] NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_Subscriber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SystemHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OwnerId] [int] NOT NULL,
	[SubscriberId] [int] NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[RecordId] [int] NOT NULL,
	[Action] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SystemHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [TemplateEmail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Body] [nvarchar](4000) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_TemplateEmail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Tenant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[OwnerId] [int] NULL,
 CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](255) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](255) NULL,
	[SecurityStamp] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](20) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[JobTitle] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Notes] [nvarchar](255) NULL,
	[TenantId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UserClaim](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.UserClaim] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UserLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Location] [geography] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[TenantId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UserLogin](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserLogin] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UserRole](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [SubscriptionAsset]
AS
SELECT        dbo.Asset.Id, dbo.Asset.FileName, dbo.Asset.FilePath, dbo.Asset.Thumbnail, dbo.Asset.IsGated, dbo.Asset.IsCobranded, dbo.Asset.TenantId, dbo.Asset.FormId, dbo.Asset.CategoryId, 
                         dbo.AssetPartnerType.PartnerTypeId, dbo.Asset.CreatedDate, dbo.Asset.UpdatedDate, dbo.Subscription.SubscriberId
FROM            dbo.Asset LEFT OUTER JOIN
                         dbo.AssetPartnerType ON dbo.Asset.Id = dbo.AssetPartnerType.AssetId INNER JOIN
                         dbo.Subscription ON dbo.Asset.TenantId = dbo.Subscription.TenantId
WHERE        (dbo.Subscription.Status = 1) AND (dbo.Asset.Published = 1)

GO
SET IDENTITY_INSERT [Asset] ON 

INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (1, N'I Love You (Simon Elvin)', N't0326801.jpg', N'/upload/host1/thumbnail/t0326801.jpg', N'/upload/host1/thumbnail/t0326801.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (2, N'Elvis Hunka Burning Love', N't16110p.jpg', N'/upload/host1/thumbnail/t16110p.jpg', N'/upload/host1/thumbnail/t16110p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (3, N'Funny Love', N't16162p.jpg', N'/upload/host1/thumbnail/t16162p.jpg', N'/upload/host1/thumbnail/t16162p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (4, N'Today, Tomorrow & Forever', N't16363p.jpg', N'/upload/host1/thumbnail/t16363p.jpg', N'/upload/host1/thumbnail/t16363p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (5, N'Smiley Heart Red Balloon', N't16744p.jpg', N'/upload/host1/thumbnail/t16744p.jpg', N'/upload/host1/thumbnail/t16744p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (6, N'Love 24 Karat', N't16756p.jpg', N'/upload/host1/thumbnail/t16756p.jpg', N'/upload/host1/thumbnail/t16756p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (7, N'Smiley Kiss Red Balloon', N't16864p.jpg', N'/upload/host1/thumbnail/t16864p.jpg', N'/upload/host1/thumbnail/t16864p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (8, N'Love You Hearts', N't16967p.jpg', N'/upload/host1/thumbnail/t16967p.jpg', N'/upload/host1/thumbnail/t16967p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (9, N'Love Me Tender', N't16973p.jpg', N'/upload/host1/thumbnail/t16973p.jpg', N'/upload/host1/thumbnail/t16973p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (10, N'I Can''t Get Enough of You Baby', N't16974p.jpg', N'/upload/host1/thumbnail/t16974p.jpg', N'/upload/host1/thumbnail/t16974p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (11, N'Picture Perfect Love Swing', N't16980p.jpg', N'/upload/host1/thumbnail/t16980p.jpg', N'/upload/host1/thumbnail/t16980p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (12, N'I Love You Roses', N't214006p.jpg', N'/upload/host1/thumbnail/t214006p.jpg', N'/upload/host1/thumbnail/t214006p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (13, N'I Love You Script', N't214041p.jpg', N'/upload/host1/thumbnail/t214041p.jpg', N'/upload/host1/thumbnail/t214041p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (14, N'Love Rose', N't214168p.jpg', N'/upload/host1/thumbnail/t214168p.jpg', N'/upload/host1/thumbnail/t214168p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (15, N'You''re So Special', N't215302p.jpg', N'/upload/host1/thumbnail/t215302p.jpg', N'/upload/host1/thumbnail/t215302p.jpg', 1, 1, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-17 16:11:51.280' AS DateTime), 7, 12, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (16, N'I Love You Red Flourishes', N't22849b.jpg', N'/upload/host1/thumbnail/t22849b.jpg', N'/upload/host1/thumbnail/t22849b.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (17, N'I Love You Script', N't45093.jpg', N'/upload/host1/thumbnail/t45093.jpg', N'/upload/host1/thumbnail/t45093.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 4, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (18, N'Love Cascade Hearts', N't68841b.jpg', N'/upload/host1/thumbnail/t68841b.jpg', N'/upload/host1/thumbnail/t68841b.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (19, N'You''re So Special', N't7004801.jpg', N'/upload/host1/thumbnail/t7004801.jpg', N'/upload/host1/thumbnail/t7004801.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (20, N'Love Script', N't7008501.jpg', N'/upload/host1/thumbnail/t7008501.jpg', N'/upload/host1/thumbnail/t7008501.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (21, N'Baby Hi Little Angel', N't115343p.jpg', N'/upload/host1/thumbnail/t115343p.jpg', N'/upload/host1/thumbnail/t115343p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (22, N'I''m Younger Than You', N't16118p.jpg', N'/upload/host1/thumbnail/t16118p.jpg', N'/upload/host1/thumbnail/t16118p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (23, N'Birthday Balloon', N't26013.jpg', N'/upload/host1/thumbnail/t26013.jpg', N'/upload/host1/thumbnail/t26013.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (24, N'Birthday Star Balloon', N't35732.jpg', N'/upload/host1/thumbnail/t35732.jpg', N'/upload/host1/thumbnail/t35732.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 6, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (25, N'Tweety Stars', N't0276001.jpg', N'/upload/host1/thumbnail/t0276001.jpg', N'/upload/host1/thumbnail/t0276001.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 7, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (26, N'You''re Special', N't0704901.jpg', N'/upload/host1/thumbnail/t0704901.jpg', N'/upload/host1/thumbnail/t0704901.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 7, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (27, N'I''m Sorry (Simon Elvin) Balloon', N't0707401.jpg', N'/upload/host1/thumbnail/t0707401.jpg', N'/upload/host1/thumbnail/t0707401.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 7, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (28, N'World''s Greatest Mom', N't114103p.jpg', N'/upload/host1/thumbnail/t114103p.jpg', N'/upload/host1/thumbnail/t114103p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 7, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (29, N'Good Luck', N't114118p.jpg', N'/upload/host1/thumbnail/t114118p.jpg', N'/upload/host1/thumbnail/t114118p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (30, N'Big Congratulations Balloon', N't114208p.jpg', N'/upload/host1/thumbnail/t114208p.jpg', N'/upload/host1/thumbnail/t114208p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (31, N'You''re So Special', N't16148p.jpg', N'/upload/host1/thumbnail/t16148p.jpg', N'/upload/host1/thumbnail/t16148p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (32, N'Thinking of You', N't16151p.jpg', N'/upload/host1/thumbnail/t16151p.jpg', N'/upload/host1/thumbnail/t16151p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (33, N'Welcome Back', N't16558p.jpg', N'/upload/host1/thumbnail/t16558p.jpg', N'/upload/host1/thumbnail/t16558p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (34, N'Words of Thanks', N't16772p.jpg', N'/upload/host1/thumbnail/t16772p.jpg', N'/upload/host1/thumbnail/t16772p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (35, N'Missed You''ll Be', N't16809p.jpg', N'/upload/host1/thumbnail/t16809p.jpg', N'/upload/host1/thumbnail/t16809p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (36, N'You''re Appreciated', N't16988p.jpg', N'/upload/host1/thumbnail/t16988p.jpg', N'/upload/host1/thumbnail/t16988p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (37, N'Thinking of You', N't214046p.jpg', N'/upload/host1/thumbnail/t214046p.jpg', N'/upload/host1/thumbnail/t214046p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (38, N'Get Well-Daisy Smiles', N't21825b.jpg', N'/upload/host1/thumbnail/t21825b.jpg', N'/upload/host1/thumbnail/t21825b.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (39, N'Toy Story', N't0366101.jpg', N'/upload/host1/thumbnail/t0366101.jpg', N'/upload/host1/thumbnail/t0366101.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (40, N'Rugrats Tommy & Chucky', N't03944l.jpg', N'/upload/host1/thumbnail/t03944l.jpg', N'/upload/host1/thumbnail/t03944l.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (41, N'Rugrats & Reptar Character', N't03945L.jpg', N'/upload/host1/thumbnail/t03945L.jpg', N'/upload/host1/thumbnail/t03945L.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (42, N'Tweety & Sylvester', N't0510801.jpg', N'/upload/host1/thumbnail/t0510801.jpg', N'/upload/host1/thumbnail/t0510801.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (43, N'Mickey Close-up', N't0521201.jpg', N'/upload/host1/thumbnail/t0521201.jpg', N'/upload/host1/thumbnail/t0521201.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (44, N'Minnie Close-up', N't0522101.jpg', N'/upload/host1/thumbnail/t0522101.jpg', N'/upload/host1/thumbnail/t0522101.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (45, N'Teletubbies Time', N't0611401.jpg', N'/upload/host1/thumbnail/t0611401.jpg', N'/upload/host1/thumbnail/t0611401.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 8, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (46, N'Barbie My Special Things', N't0661701.jpg', N'/upload/host1/thumbnail/t0661701.jpg', N'/upload/host1/thumbnail/t0661701.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (47, N'Paddington Bear', N't215017p.jpg', N'/upload/host1/thumbnail/t215017p.jpg', N'/upload/host1/thumbnail/t215017p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (48, N'I Love You Snoopy', N't215402p.jpg', N'/upload/host1/thumbnail/t215402p.jpg', N'/upload/host1/thumbnail/t215402p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (49, N'Pooh Adult', N't81947pl.jpg', N'/upload/host1/thumbnail/t81947pl.jpg', N'/upload/host1/thumbnail/t81947pl.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (50, N'Pokemon Character', N't83947.jpg', N'/upload/host1/thumbnail/t83947.jpg', N'/upload/host1/thumbnail/t83947.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (51, N'Pokemon Ash & Pikachu', N't83951.jpg', N'/upload/host1/thumbnail/t83951.jpg', N'/upload/host1/thumbnail/t83951.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (52, N'Smiley Kiss Yellow', N't16862p.jpg', N'/upload/host1/thumbnail/t16862p.jpg', N'/upload/host1/thumbnail/t16862p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (53, N'Smiley Face', N't214154p.jpg', N'/upload/host1/thumbnail/t214154p.jpg', N'/upload/host1/thumbnail/t214154p.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (54, N'Soccer Shape', N't28734.jpg', N'/upload/host1/thumbnail/t28734.jpg', N'/upload/host1/thumbnail/t28734.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (55, N'Goal Ball', N'ta1180401.jpg', N'/upload/host1/thumbnail/ta1180401.jpg', N'/upload/host1/thumbnail/ta1180401.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (56, N'Wedding Doves', N't1368601.jpg', N'/upload/host1/thumbnail/t1368601.jpg', N'/upload/host1/thumbnail/t1368601.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (57, N'Crystal Rose Silver', N't38196.jpg', N'/upload/host1/thumbnail/t38196.jpg', N'/upload/host1/thumbnail/t38196.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (58, N'Crystal Rose Gold', N't38199.jpg', N'/upload/host1/thumbnail/t38199.jpg', N'/upload/host1/thumbnail/t38199.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 9, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (59, N'Crystal Rose Red', N't38202.jpg', N'/upload/host1/thumbnail/t38202.jpg', N'/upload/host1/thumbnail/t38202.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (60, N'Crystal Etched Hearts', N't42014.jpg', N'/upload/host1/thumbnail/t42014.jpg', N'/upload/host1/thumbnail/t42014.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (61, N'Crystal Love Doves Silver', N't42080.jpg', N'/upload/host1/thumbnail/t42080.jpg', N'/upload/host1/thumbnail/t42080.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
INSERT [Asset] ([Id], [Alias], [FileName], [FilePath], [Thumbnail], [Published], [IsGated], [IsCobranded], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [FormId], [CategoryId], [TenantId]) VALUES (62, N'Crystal Etched Hearts', N't42139.jpg', N'/upload/host1/thumbnail/t42139.jpg', N'/upload/host1/thumbnail/t42139.jpg', 1, 0, 0, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, CAST(N'2017-02-02 00:00:00.000' AS DateTime), 7, NULL, 2, 3)
SET IDENTITY_INSERT [Asset] OFF
SET IDENTITY_INSERT [AssetDownload] ON 

INSERT [AssetDownload] ([Id], [AssetId], [SubscriberId], [TenantId], [LeadContent], [IPAddress], [IsLead], [CreatedDate]) VALUES (1, 15, 8, 3, N'{"FirstName":"sadfsa","LastName":"dfsadf","Genre":"Male","Notes":"Something abosdfsafut you","Agree":"false","AssetId":15,"FormId":12}', N'::1', 1, CAST(N'2017-02-28 17:05:43.873' AS DateTime))
INSERT [AssetDownload] ([Id], [AssetId], [SubscriberId], [TenantId], [LeadContent], [IPAddress], [IsLead], [CreatedDate]) VALUES (2, 10, 8, 3, NULL, N'::1', 0, CAST(N'2017-02-28 17:06:08.737' AS DateTime))
SET IDENTITY_INSERT [AssetDownload] OFF
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (1, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (1, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (2, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (2, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (3, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (3, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (4, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (4, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (5, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (5, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (6, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (6, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (7, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (7, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (8, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (8, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (9, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (9, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (10, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (10, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (11, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (11, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (12, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (12, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (13, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (13, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (14, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (14, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (15, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (15, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (16, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (16, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (17, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (17, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (18, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (18, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (19, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (19, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (20, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (20, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (21, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (21, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (22, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (22, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (23, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (23, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (24, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (24, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (25, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (25, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (26, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (26, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (27, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (27, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (28, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (28, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (29, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (29, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (30, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (30, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (31, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (31, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (32, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (32, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (33, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (33, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (34, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (34, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (35, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (35, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (36, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (36, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (37, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (37, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (38, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (38, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (39, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (39, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (40, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (40, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (41, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (41, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (42, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (42, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (43, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (43, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (44, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (44, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (45, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (45, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (46, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (46, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (47, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (47, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (48, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (48, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (49, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (49, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (50, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (50, 2)
GO
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (51, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (51, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (52, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (52, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (53, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (53, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (54, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (54, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (55, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (55, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (56, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (56, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (57, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (57, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (58, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (58, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (59, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (59, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (60, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (60, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (61, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (61, 2)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (62, 1)
INSERT [AssetPartnerType] ([AssetId], [PartnerTypeId]) VALUES (62, 2)
SET IDENTITY_INSERT [AssetTranslation] ON 

INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (2, 1, N'I Love You (Simon Elvin)', N'<script>alert(''boo'')</script>An adorable romantic balloon by Simon Elvin. You''ll fall in love with the cute bear bearing a bouquet of roses, a heart with I Love You, and ''a'' card.', N'<script>alert(''boo'')</script>An adorable romantic balloon by Simon Elvin. You''ll fall in love with the cute bear bearing a bouquet of roses, a heart with I Love You, and ''a'' card.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (3, 2, N'Elvis Hunka Burning Love', N'A heart shaped balloon with the great Elvis on it and the words "You''re My Hunka Hunka Burnin'' Love!". Also a copy of the Kings Signature.', N'A heart shaped balloon with the great Elvis on it and the words "You''re My Hunka Hunka Burnin'' Love!". Also a copy of the Kings Signature.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (4, 3, N'Funny Love', N'A red heart-shaped balloon with "I love you" written on a white heart surrounded by cute little hearts and flowers.', N'A red heart-shaped balloon with "I love you" written on a white heart surrounded by cute little hearts and flowers.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (5, 4, N'Today, Tomorrow & Forever', N'White heart-shaped balloon with the words "Today, Tomorrow and Forever" surrounded with red hearts of varying shapes. "I Love You" appears at the bottom in a red heart.', N'White heart-shaped balloon with the words "Today, Tomorrow and Forever" surrounded with red hearts of varying shapes. "I Love You" appears at the bottom in a red heart.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (6, 5, N'Smiley Heart Red Balloon', N'Red heart-shaped balloon with a smiley face. Perfect for saying I Love You!', N'Red heart-shaped balloon with a smiley face. Perfect for saying I Love You!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (7, 6, N'Love 24 Karat', N'A red heart-shaped balloon with "I Love You" in script writing. Gold heart outlines adorn the background.', N'A red heart-shaped balloon with "I Love You" in script writing. Gold heart outlines adorn the background.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (8, 7, N'Smiley Kiss Red Balloon', N'Red heart-shaped balloon with a smiley face and three kisses. A perfect gift for Valentine''s Day!', N'Red heart-shaped balloon with a smiley face and three kisses. A perfect gift for Valentine''s Day!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (9, 8, N'Love You Hearts', N'A balloon with a simple message of love. What can be more romantic?', N'A balloon with a simple message of love. What can be more romantic?', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (10, 9, N'Love Me Tender', N'A heart-shaped balloon with a picture of the King himself-Elvis Presley. This must-have for any Elvis fan has "Love Me Tender" written on it with a copy of Elvis''s signature.', N'A heart-shaped balloon with a picture of the King himself-Elvis Presley. This must-have for any Elvis fan has "Love Me Tender" written on it with a copy of Elvis''s signature.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (11, 10, N'I Can''t Get Enough of You Baby', N'When you just can''t get enough of someone, this Austin Powers style balloon says it all.', N'When you just can''t get enough of someone, this Austin Powers style balloon says it all.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (12, 11, N'Picture Perfect Love Swing', N'A red heart-shaped balloon with a cute picture of two children kissing on a swing.', N'A red heart-shaped balloon with a cute picture of two children kissing on a swing.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (13, 12, N'I Love You Roses', N'A white heart-shaped balloon has "I Love You" written on it and is beautifully decorated with two flowers, a small red heart in the middle, and miniature hearts all around.', N'A white heart-shaped balloon has "I Love You" written on it and is beautifully decorated with two flowers, a small red heart in the middle, and miniature hearts all around.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (14, 13, N'I Love You Script', N'A romantic red heart-shaped balloon with "I Love You" in white. What more can you say?', N'A romantic red heart-shaped balloon with "I Love You" in white. What more can you say?', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (15, 14, N'Love Rose', N'A white heart-shaped balloon with a rose and the words "I Love You." Romantic and irresistible.', N'A white heart-shaped balloon with a rose and the words "I Love You." Romantic and irresistible.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (16, 15, N'You''re So Special', N'Tell someone how special he or she is with this lovely heart-shaped balloon with a cute bear holding a flower.', N'Tell someone how special he or she is with this lovely heart-shaped balloon with a cute bear holding a flower.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (17, 16, N'I Love You Red Flourishes', N'A simple but romantic red heart-shaped balloon with "I Love You" in large script writing.', N'A simple but romantic red heart-shaped balloon with "I Love You" in large script writing.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (18, 17, N'I Love You Script', N'A simple, romantic red heart-shaped balloon with "I Love You" in small script writing.', N'A simple, romantic red heart-shaped balloon with "I Love You" in small script writing.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (19, 18, N'Love Cascade Hearts', N'A romantic red heart-shaped balloon with hearts and I "Love You."', N'A romantic red heart-shaped balloon with hearts and I "Love You."', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (20, 19, N'You''re So Special', N'Someone special in your life? Let them know by sending this "You''re So Special" balloon!', N'Someone special in your life? Let them know by sending this "You''re So Special" balloon!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (21, 20, N'Love Script', N'Romance is in the air with this red heart-shaped balloon. Perfect for the love of your life.', N'Romance is in the air with this red heart-shaped balloon. Perfect for the love of your life.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (22, 21, N'Baby Hi Little Angel', N'Baby Hi Little Angel', N'Baby Hi Little Angel', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (23, 22, N'I''m Younger Than You', N'Roses are red, violets are blue, but this balloon isn''t a romantic balloon at all. Have a laugh, and tease someone older.', N'Roses are red, violets are blue, but this balloon isn''t a romantic balloon at all. Have a laugh, and tease someone older.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (24, 23, N'Birthday Balloon', N'Great Birthday Balloons. Available in pink or blue. One side says "Happy Birthday To You" and the other side says  "Birthday Girl" on the Pink Balloon and "Birthday Boy" on the Blue Balloon. Especially great for children''s parties.', N'Great Birthday Balloons. Available in pink or blue. One side says "Happy Birthday To You" and the other side says  "Birthday Girl" on the Pink Balloon and "Birthday Boy" on the Blue Balloon. Especially great for children''s parties.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (25, 24, N'Birthday Star Balloon', N'Send a birthday message with this delightful star-shaped balloon and make someone''s day!', N'Send a birthday message with this delightful star-shaped balloon and make someone''s day!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (26, 25, N'Tweety Stars', N'A cute Tweety bird on a blue heart-shaped balloon with stars. Sylvester is in the background, plotting away as usual.', N'A cute Tweety bird on a blue heart-shaped balloon with stars. Sylvester is in the background, plotting away as usual.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (27, 26, N'You''re Special', N'An unusual heart-shaped balloon with the words "You''re special.".', N'An unusual heart-shaped balloon with the words "You''re special.".', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (28, 27, N'I''m Sorry (Simon Elvin) Balloon', N'The perfect way to say you''re sorry. Send a thought with this cute bear  balloon.', N'The perfect way to say you''re sorry. Send a thought with this cute bear  balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (29, 28, N'World''s Greatest Mom', N'A lovely way to tell your Mom that she''s special. Surprise her with this lovely balloon on her doorstep.', N'A lovely way to tell your Mom that she''s special. Surprise her with this lovely balloon on her doorstep.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (30, 29, N'Good Luck', N'Big day ahead? Wish someone "Good Luck" with this colorful balloon!', N'Big day ahead? Wish someone "Good Luck" with this colorful balloon!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (31, 30, N'Big Congratulations Balloon', N'Does someone deserve a special pat on the back? This balloon is a perfect way to pass on the message', N'Does someone deserve a special pat on the back? This balloon is a perfect way to pass on the message', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (32, 31, N'You''re So Special', N'A purple balloon with the simple words "You''re so Special!" on it. Go on, let them know they are special.', N'A purple balloon with the simple words "You''re so Special!" on it. Go on, let them know they are special.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (33, 32, N'Thinking of You', N'A round balloon just screaming out "Thinking of You!"; especially great if you are far away from someone you care for.', N'A round balloon just screaming out "Thinking of You!"; especially great if you are far away from someone you care for.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (34, 33, N'Welcome Back', N'A great way to say Welcome Back!', N'A great way to say Welcome Back!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (35, 34, N'Words of Thanks', N'A round balloon with lots and lots of Thank You''s written on it. You''re sure to get the message through with this grateful balloon.', N'A round balloon with lots and lots of Thank You''s written on it. You''re sure to get the message through with this grateful balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (36, 35, N'Missed You''ll Be', N'If someone special is Going away, let this cute puppy balloon tell them they''ll be missed.', N'If someone special is Going away, let this cute puppy balloon tell them they''ll be missed.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (37, 36, N'You''re Appreciated', N'A spotty balloon with the words "You''re Appreciated". I bet they''ll appreciate it too!', N'A spotty balloon with the words "You''re Appreciated". I bet they''ll appreciate it too!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (38, 37, N'Thinking of You', N'Thinking of someone? Let them know with this thoughtful heart-shaped balloon with flowers in the background.', N'Thinking of someone? Let them know with this thoughtful heart-shaped balloon with flowers in the background.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (39, 38, N'Get Well-Daisy Smiles', N'We all get sick sometimes and need something to cheer us up. Make the world brighter for someone with this Get Well Soon balloon.', N'We all get sick sometimes and need something to cheer us up. Make the world brighter for someone with this Get Well Soon balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (40, 39, N'Toy Story', N'Woody and Buzz from Toy Story, on a round balloon.', N'Woody and Buzz from Toy Story, on a round balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (41, 40, N'Rugrats Tommy & Chucky', N'If you are a Rugrats fan, you''ll be nuts about this purple Rugrats balloon featuring Chucky and Tommy. A definite Nickelodeon Toon favorite.', N'If you are a Rugrats fan, you''ll be nuts about this purple Rugrats balloon featuring Chucky and Tommy. A definite Nickelodeon Toon favorite.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (42, 41, N'Rugrats & Reptar Character', N'Rugrats balloon featuring Angelica, Chucky, Tommy, and Reptar.', N'Rugrats balloon featuring Angelica, Chucky, Tommy, and Reptar.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (43, 42, N'Tweety & Sylvester', N'A blue round balloon with the great cartoon pair: Tweety & Sylvester.', N'A blue round balloon with the great cartoon pair: Tweety & Sylvester.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (44, 43, N'Mickey Close-up', N'A close-up of Mickey Mouse on a blue heart-shaped balloon. Check out our close-up matching Minnie balloon.', N'A close-up of Mickey Mouse on a blue heart-shaped balloon. Check out our close-up matching Minnie balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (45, 44, N'Minnie Close-up', N'A close-up of Minnie Mouse on a pink heart-shaped balloon. Check out our close-up matching Mickey balloon.', N'A close-up of Minnie Mouse on a pink heart-shaped balloon. Check out our close-up matching Mickey balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (46, 45, N'Teletubbies Time', N'Time for Teletubbies balloon. Great gift for any kid.', N'Time for Teletubbies balloon. Great gift for any kid.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (47, 46, N'Barbie My Special Things', N'Barbie and her friends on a round balloon.', N'Barbie and her friends on a round balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (48, 47, N'Paddington Bear', N'Remember Paddington? A must-have for any Paddington Bear lover.', N'Remember Paddington? A must-have for any Paddington Bear lover.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (49, 48, N'I Love You Snoopy', N'The one and only Snoopy hugging Charlie Brown to say "I Love You."', N'The one and only Snoopy hugging Charlie Brown to say "I Love You."', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (50, 49, N'Pooh Adult', N'An adorable Winnie the Pooh balloon.', N'An adorable Winnie the Pooh balloon.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (51, 50, N'Pokemon Character', N'A Pokemon balloon with a lot of mini pictures of the rest of the cast. Pokemon, Gotta catch ''em all!', N'A Pokemon balloon with a lot of mini pictures of the rest of the cast. Pokemon, Gotta catch ''em all!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (52, 51, N'Pokemon Ash & Pikachu', N'A Pokemon balloon with Ash and Pikachu. Gotta catch ''em all!', N'A Pokemon balloon with Ash and Pikachu. Gotta catch ''em all!', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (53, 52, N'Smiley Kiss Yellow', N'The ever-famous Smiley Face balloon on the classic yellow background with three smooch kisses.', N'The ever-famous Smiley Face balloon on the classic yellow background with three smooch kisses.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (54, 53, N'Smiley Face', N'A red heart-shaped balloon with a cartoon smiley face.', N'A red heart-shaped balloon with a cartoon smiley face.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (55, 54, N'Soccer Shape', N'A soccer-shaped balloon great for any soccer fan.', N'A soccer-shaped balloon great for any soccer fan.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (56, 55, N'Goal Ball', N'A round soccer balloon. Ideal for any sports fan, or an original way to celebrate an important Goal in that "oh so important" game.', N'A round soccer balloon. Ideal for any sports fan, or an original way to celebrate an important Goal in that "oh so important" game.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (57, 56, N'Wedding Doves', N'A white heart-shaped balloon with wedding wishes and intricate designs of doves in silver.', N'A white heart-shaped balloon with wedding wishes and intricate designs of doves in silver.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (58, 57, N'Crystal Rose Silver', N'A transparent heart-shaped balloon with silver roses. Perfect for a silver anniversary or a wedding with a silver theme.', N'A transparent heart-shaped balloon with silver roses. Perfect for a silver anniversary or a wedding with a silver theme.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (59, 58, N'Crystal Rose Gold', N'A transparent heart-shaped balloon with Gold roses. Perfect for a Golden anniversary or a wedding with a Gold theme.', N'A transparent heart-shaped balloon with Gold roses. Perfect for a Golden anniversary or a wedding with a Gold theme.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (60, 59, N'Crystal Rose Red', N'A transparent heart-shaped balloon with red roses. Perfect for an anniversary or a wedding with a red theme.', N'A transparent heart-shaped balloon with red roses. Perfect for an anniversary or a wedding with a red theme.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (61, 60, N'Crystal Etched Hearts', N'A transparent heart-shaped balloon with silver hearts. Perfect for a silver anniversary or a wedding with a silver theme.', N'A transparent heart-shaped balloon with silver hearts. Perfect for a silver anniversary or a wedding with a silver theme.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (62, 61, N'Crystal Love Doves Silver', N'A transparent heart-shaped balloon with two love doves in silver.', N'A transparent heart-shaped balloon with two love doves in silver.', N'en', 3)
INSERT [AssetTranslation] ([Id], [AssetId], [AssetTitle], [ShortDescription], [LongDescription], [LanguageCode], [TenantId]) VALUES (63, 62, N'Crystal Etched Hearts', N'A transparent heart-shaped balloon with red hearts.', N'A transparent heart-shaped balloon with red hearts.', N'en', 3)
SET IDENTITY_INSERT [AssetTranslation] OFF
SET IDENTITY_INSERT [Category] ON 

INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (2, N'Birthdays', 2, NULL, CAST(N'2017-01-24 00:32:27.360' AS DateTime), 7, 7, CAST(N'2017-02-15 19:41:28.420' AS DateTime), 3)
INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (4, N'Love & Romance	', 1, NULL, CAST(N'2017-02-02 21:21:47.967' AS DateTime), 7, 7, CAST(N'2017-02-24 14:04:26.823' AS DateTime), 3)
INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (6, N'Weddings', 3, NULL, CAST(N'2017-02-15 19:39:56.300' AS DateTime), 7, 7, CAST(N'2017-02-15 19:39:56.300' AS DateTime), 3)
INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (7, N'Message Balloons', 5, NULL, CAST(N'2017-02-15 19:40:15.090' AS DateTime), 7, 7, CAST(N'2017-02-15 19:40:15.090' AS DateTime), 3)
INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (8, N'Cartoons', 6, NULL, CAST(N'2017-02-15 19:40:35.257' AS DateTime), 7, 7, CAST(N'2017-02-15 19:40:35.257' AS DateTime), 3)
INSERT [Category] ([Id], [Alias], [SortOrder], [ParentId], [CreatedDate], [CreatedBy], [UpdatedBy], [UpdatedDate], [TenantId]) VALUES (9, N'Miscellaneous', 7, NULL, CAST(N'2017-02-15 19:40:51.287' AS DateTime), 7, 7, CAST(N'2017-02-15 19:40:51.287' AS DateTime), 3)
SET IDENTITY_INSERT [Category] OFF
SET IDENTITY_INSERT [CategoryTranslation] ON 

INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (1, 2, N'Birthdays', N'Tell someone "Happy Birthday" with one of these wonderful balloons!', N'en', 3)
INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (5, 4, N'Love & Romance', N'Here''s our collection of balloons with romantic messages.', N'en', 3)
INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (8, 6, N'Weddings', N'Going to a wedding? Here''s a collection of balloons for that special event!', N'en', 3)
INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (9, 7, N'Message Balloons', N'Why write on paper, when you can deliver your message on a balloon?', N'en', 3)
INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (10, 8, N'Cartoons', N'Buy a balloon with your child''s favorite cartoon character!', N'en', 3)
INSERT [CategoryTranslation] ([Id], [CategoryId], [Name], [Description], [LanguageCode], [TenantId]) VALUES (11, 9, N'Miscellaneous', N'Various baloons that your kid will most certainly love!', N'en', 3)
SET IDENTITY_INSERT [CategoryTranslation] OFF
INSERT [Company] ([UserId], [Name], [Logo], [Address1], [Address2], [City], [County], [State], [ZipCode], [Country], [WebsiteUrl], [ExternalCode], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (7, N'asfafasf', N'asfa', N'asf', N'asfasf', N'fa', N'faf', N'af', N'af', N'asfa', N'http://abc.com', N'asfaf', CAST(N'2017-02-14 22:24:21.663' AS DateTime), 7, CAST(N'2017-02-14 22:24:21.663' AS DateTime), 7, 3)
INSERT [Company] ([UserId], [Name], [Logo], [Address1], [Address2], [City], [County], [State], [ZipCode], [Country], [WebsiteUrl], [ExternalCode], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (8, N'My company', N'/Upload/reseller/thumbnail/b17e406477ea49899ea6b3eb146b61c1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-02-16 14:34:30.167' AS DateTime), 8, CAST(N'2017-02-22 16:17:01.343' AS DateTime), 8, 1)
SET IDENTITY_INSERT [CompanyAttribute] ON 

INSERT [CompanyAttribute] ([Id], [AttributeName], [IsMandatory], [SortOrder], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (1, N'SKU', 1, 0, CAST(N'2017-02-12 10:45:44.587' AS DateTime), 7, CAST(N'2017-02-12 21:50:33.530' AS DateTime), 7, 3)
INSERT [CompanyAttribute] ([Id], [AttributeName], [IsMandatory], [SortOrder], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (2, N'Code', 1, 1, CAST(N'2017-02-12 10:46:34.040' AS DateTime), 7, CAST(N'2017-02-12 21:50:37.230' AS DateTime), 7, 3)
SET IDENTITY_INSERT [CompanyAttribute] OFF
SET IDENTITY_INSERT [Form] ON 

INSERT [Form] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (12, N'Download', N'Personal Information', 7, CAST(N'2017-02-09 15:56:30.893' AS DateTime), CAST(N'2017-02-11 00:15:04.503' AS DateTime), 7, 3)
INSERT [Form] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (18, N'asdf', N'asdfsf', 7, CAST(N'2017-02-10 16:55:39.227' AS DateTime), CAST(N'2017-02-10 16:55:39.227' AS DateTime), 7, 3)
SET IDENTITY_INSERT [Form] OFF
SET IDENTITY_INSERT [FormField] ON 

INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (2, 12, N'LastName', 0, 1, 2, 3)
INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (3, 12, N'FirstName', 0, 1, 1, 3)
INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (4, 12, N'Genre', 2, 0, 3, 3)
INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (6, 12, N'Notes', 1, 1, 4, 3)
INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (12, 12, N'Agree', 3, 0, 5, 3)
INSERT [FormField] ([Id], [FormId], [FieldName], [DataType], [IsRequired], [SortOrder], [TenantId]) VALUES (13, 18, N'Id', 0, 1, 0, 3)
SET IDENTITY_INSERT [FormField] OFF
SET IDENTITY_INSERT [FormFieldTranslation] ON 

INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (3, 3, N'en', N'First Name', NULL, NULL, 3)
INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (7, 2, N'en', N'Last Name', NULL, NULL, 3)
INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (8, 4, N'en', N'Genre', N'Male
Female
Others', NULL, 3)
INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (10, 6, N'en', N'Notes', NULL, N'Something about you', 3)
INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (11, 12, N'en', N'Agree', NULL, N'1', 3)
INSERT [FormFieldTranslation] ([Id], [FormFieldId], [LanguageCode], [Label], [DataSource], [DefaultValue], [TenantId]) VALUES (12, 13, N'en', N'asdf sad fasd', NULL, NULL, 3)
SET IDENTITY_INSERT [FormFieldTranslation] OFF
SET IDENTITY_INSERT [Language] ON 

INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (1, N'en', 1, 7, CAST(N'2017-01-24 00:33:21.823' AS DateTime), CAST(N'2017-02-02 16:07:11.437' AS DateTime), 7, 3)
INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (2, N'vi', 0, 7, CAST(N'2017-01-24 00:33:32.823' AS DateTime), CAST(N'2017-02-02 15:12:05.517' AS DateTime), 7, 3)
INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (3, N'zh-CN', 0, 7, CAST(N'2017-01-25 19:03:56.153' AS DateTime), CAST(N'2017-01-25 19:03:56.153' AS DateTime), 7, 3)
INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (5, N'en', 0, 8, CAST(N'2017-01-30 10:08:23.687' AS DateTime), CAST(N'2017-01-30 10:08:23.687' AS DateTime), 7, 1)
INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (6, N'fr', 0, 8, CAST(N'2017-01-30 10:08:29.813' AS DateTime), CAST(N'2017-01-30 10:08:29.813' AS DateTime), 7, 1)
INSERT [Language] ([Id], [LanguageCode], [IsDefault], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (7, N'aa', 0, 6, CAST(N'2017-02-24 08:07:37.603' AS DateTime), CAST(N'2017-02-24 08:07:37.603' AS DateTime), 6, 1)
SET IDENTITY_INSERT [Language] OFF
SET IDENTITY_INSERT [PartnerType] ON 

INSERT [PartnerType] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (1, N'Golden', N'Golden', 7, CAST(N'2017-01-24 08:53:48.997' AS DateTime), CAST(N'2017-02-03 10:46:13.260' AS DateTime), 7, 3)
INSERT [PartnerType] ([Id], [Name], [Description], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy], [TenantId]) VALUES (2, N'Silver', N'Silver', 7, CAST(N'2017-01-24 08:53:53.783' AS DateTime), CAST(N'2017-02-24 14:06:45.870' AS DateTime), 7, 3)
SET IDENTITY_INSERT [PartnerType] OFF
SET IDENTITY_INSERT [Role] ON 

INSERT [Role] ([Id], [Name]) VALUES (1003, N'Host')
INSERT [Role] ([Id], [Name]) VALUES (1004, N'Distributor')
INSERT [Role] ([Id], [Name]) VALUES (1005, N'Reseller')
INSERT [Role] ([Id], [Name]) VALUES (1006, N'User')
SET IDENTITY_INSERT [Role] OFF
SET IDENTITY_INSERT [Subscription] ON 

INSERT [Subscription] ([Id], [TenantId], [SubscriberId], [PartnerTypeId], [Status], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy]) VALUES (1, 3, 8, 1, 1, 8, CAST(N'2017-01-26 22:11:01.573' AS DateTime), CAST(N'2017-02-03 11:01:00.317' AS DateTime), 7)
SET IDENTITY_INSERT [Subscription] OFF
SET IDENTITY_INSERT [Tenant] ON 

INSERT [Tenant] ([Id], [Name], [IsActive], [CreatedDate], [OwnerId]) VALUES (1, N'Default', 1, CAST(N'2017-01-23 21:38:02.733' AS DateTime), NULL)
INSERT [Tenant] ([Id], [Name], [IsActive], [CreatedDate], [OwnerId]) VALUES (3, N'host1', 1, CAST(N'2017-01-23 22:32:51.890' AS DateTime), 7)
SET IDENTITY_INSERT [Tenant] OFF
SET IDENTITY_INSERT [User] ON 

INSERT [User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName], [MiddleName], [JobTitle], [Department], [Notes], [TenantId]) VALUES (6, N'cuongdo926@gmail.com', 0, N'ADnVb5TNtJ/WX4/dIhzTXpDt2KT4lYhNyLNajXf4tbxpQrwqm1Ef+fITygSUVs1Wig==', N'8f3721c7-6395-4b29-9a35-7ae3e2250c90', NULL, 0, 0, NULL, 0, 0, N'host', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName], [MiddleName], [JobTitle], [Department], [Notes], [TenantId]) VALUES (7, N'host@gmail.com', 0, N'AJYbnVA3MJEO9ZTFvNZLzwjNoUIyZ5H4qCPyuHda85QcgodDlWvkxew/4zpEhTDu5Q==', N'b7336ebb-bc43-4077-9eb5-dfd443c12d63', NULL, 0, 0, NULL, 0, 0, N'host1', N'asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdas', N'asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdas', N'dasd', N'asd', N'sdfdsgdf', N'asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasasdas', 3)
INSERT [User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName], [MiddleName], [JobTitle], [Department], [Notes], [TenantId]) VALUES (8, N'reseller@gmail.com', 0, N'ALFWFhtfA5E/4H79Nh9lU3SkCECwcRj5c8HDRroOHFS58tCAhw+aaQPf6UR0mwV9FQ==', N'2c6b7ff1-4469-4dc9-bd12-1f92d61637fc', NULL, 0, 0, NULL, 0, 0, N'reseller', NULL, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [User] OFF
INSERT [UserRole] ([UserId], [RoleId]) VALUES (6, 1003)
INSERT [UserRole] ([UserId], [RoleId]) VALUES (7, 1004)
INSERT [UserRole] ([UserId], [RoleId]) VALUES (8, 1005)
ALTER TABLE [ELMAH_Error] ADD  CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED 
(
	[ErrorId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
GO
ALTER TABLE [Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Category] FOREIGN KEY([CategoryId])
REFERENCES [Category] ([Id])
GO
ALTER TABLE [Asset] CHECK CONSTRAINT [FK_Asset_Category]
GO
ALTER TABLE [Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Form] FOREIGN KEY([FormId])
REFERENCES [Form] ([Id])
GO
ALTER TABLE [Asset] CHECK CONSTRAINT [FK_Asset_Form]
GO
ALTER TABLE [Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Asset] CHECK CONSTRAINT [FK_Asset_Tenant]
GO
ALTER TABLE [Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Asset] CHECK CONSTRAINT [FK_Asset_User]
GO
ALTER TABLE [Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Asset] CHECK CONSTRAINT [FK_Asset_User1]
GO
ALTER TABLE [AssetCounter]  WITH CHECK ADD  CONSTRAINT [FK_AssetCounter_Asset] FOREIGN KEY([AssetId])
REFERENCES [Asset] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [AssetCounter] CHECK CONSTRAINT [FK_AssetCounter_Asset]
GO
ALTER TABLE [AssetDownload]  WITH CHECK ADD  CONSTRAINT [FK_AssetDownload_Asset] FOREIGN KEY([AssetId])
REFERENCES [Asset] ([Id])
GO
ALTER TABLE [AssetDownload] CHECK CONSTRAINT [FK_AssetDownload_Asset]
GO
ALTER TABLE [AssetDownload]  WITH CHECK ADD  CONSTRAINT [FK_AssetDownload_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [AssetDownload] CHECK CONSTRAINT [FK_AssetDownload_Tenant]
GO
ALTER TABLE [AssetDownload]  WITH CHECK ADD  CONSTRAINT [FK_AssetDownload_User] FOREIGN KEY([SubscriberId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [AssetDownload] CHECK CONSTRAINT [FK_AssetDownload_User]
GO
ALTER TABLE [AssetPartnerType]  WITH CHECK ADD  CONSTRAINT [FK_AssetPartnerType_Asset] FOREIGN KEY([AssetId])
REFERENCES [Asset] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [AssetPartnerType] CHECK CONSTRAINT [FK_AssetPartnerType_Asset]
GO
ALTER TABLE [AssetPartnerType]  WITH CHECK ADD  CONSTRAINT [FK_AssetPartnerType_PartnerType] FOREIGN KEY([PartnerTypeId])
REFERENCES [PartnerType] ([Id])
GO
ALTER TABLE [AssetPartnerType] CHECK CONSTRAINT [FK_AssetPartnerType_PartnerType]
GO
ALTER TABLE [AssetTranslation]  WITH CHECK ADD  CONSTRAINT [FK_AssetTranslation_Asset] FOREIGN KEY([AssetId])
REFERENCES [Asset] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [AssetTranslation] CHECK CONSTRAINT [FK_AssetTranslation_Asset]
GO
ALTER TABLE [AssetTranslation]  WITH CHECK ADD  CONSTRAINT [FK_AssetTranslation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [AssetTranslation] CHECK CONSTRAINT [FK_AssetTranslation_Tenant]
GO
ALTER TABLE [Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Category] FOREIGN KEY([ParentId])
REFERENCES [Category] ([Id])
GO
ALTER TABLE [Category] CHECK CONSTRAINT [FK_Category_Category]
GO
ALTER TABLE [Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Category] CHECK CONSTRAINT [FK_Category_Tenant]
GO
ALTER TABLE [Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Category] CHECK CONSTRAINT [FK_Category_User]
GO
ALTER TABLE [Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Category] CHECK CONSTRAINT [FK_Category_User1]
GO
ALTER TABLE [CategoryTranslation]  WITH CHECK ADD  CONSTRAINT [FK_CategoryTranslation_Category] FOREIGN KEY([CategoryId])
REFERENCES [Category] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [CategoryTranslation] CHECK CONSTRAINT [FK_CategoryTranslation_Category]
GO
ALTER TABLE [CategoryTranslation]  WITH CHECK ADD  CONSTRAINT [FK_CategoryTranslation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [CategoryTranslation] CHECK CONSTRAINT [FK_CategoryTranslation_Tenant]
GO
ALTER TABLE [Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_User] FOREIGN KEY([UserId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Company] CHECK CONSTRAINT [FK_Company_User]
GO
ALTER TABLE [Company]  WITH CHECK ADD  CONSTRAINT [FK_Organization_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Company] CHECK CONSTRAINT [FK_Organization_Tenant]
GO
ALTER TABLE [Company]  WITH CHECK ADD  CONSTRAINT [FK_Organization_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Company] CHECK CONSTRAINT [FK_Organization_User]
GO
ALTER TABLE [Company]  WITH CHECK ADD  CONSTRAINT [FK_Organization_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Company] CHECK CONSTRAINT [FK_Organization_User1]
GO
ALTER TABLE [CompanyAttribute]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttribute_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [CompanyAttribute] CHECK CONSTRAINT [FK_CompanyAttribute_Tenant]
GO
ALTER TABLE [CompanyAttribute]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttribute_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [CompanyAttribute] CHECK CONSTRAINT [FK_CompanyAttribute_User]
GO
ALTER TABLE [CompanyAttribute]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttribute_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [CompanyAttribute] CHECK CONSTRAINT [FK_CompanyAttribute_User1]
GO
ALTER TABLE [CompanyAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttributeValue_CompanyAttribute] FOREIGN KEY([AttributeId])
REFERENCES [CompanyAttribute] ([Id])
GO
ALTER TABLE [CompanyAttributeValue] CHECK CONSTRAINT [FK_CompanyAttributeValue_CompanyAttribute]
GO
ALTER TABLE [CompanyAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttributeValue_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [CompanyAttributeValue] CHECK CONSTRAINT [FK_CompanyAttributeValue_User]
GO
ALTER TABLE [CompanyAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_CompanyAttributeValue_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [CompanyAttributeValue] CHECK CONSTRAINT [FK_CompanyAttributeValue_User1]
GO
ALTER TABLE [Form]  WITH CHECK ADD  CONSTRAINT [FK_Form_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Form] CHECK CONSTRAINT [FK_Form_Tenant]
GO
ALTER TABLE [Form]  WITH CHECK ADD  CONSTRAINT [FK_Form_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Form] CHECK CONSTRAINT [FK_Form_User]
GO
ALTER TABLE [Form]  WITH CHECK ADD  CONSTRAINT [FK_Form_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Form] CHECK CONSTRAINT [FK_Form_User1]
GO
ALTER TABLE [FormField]  WITH CHECK ADD  CONSTRAINT [FK_FormField_Form] FOREIGN KEY([FormId])
REFERENCES [Form] ([Id])
GO
ALTER TABLE [FormField] CHECK CONSTRAINT [FK_FormField_Form]
GO
ALTER TABLE [FormField]  WITH CHECK ADD  CONSTRAINT [FK_FormField_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [FormField] CHECK CONSTRAINT [FK_FormField_Tenant]
GO
ALTER TABLE [FormFieldTranslation]  WITH CHECK ADD  CONSTRAINT [FK_FormFieldTranslation_FormField] FOREIGN KEY([FormFieldId])
REFERENCES [FormField] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [FormFieldTranslation] CHECK CONSTRAINT [FK_FormFieldTranslation_FormField]
GO
ALTER TABLE [FormFieldTranslation]  WITH CHECK ADD  CONSTRAINT [FK_FormFieldTranslation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [FormFieldTranslation] CHECK CONSTRAINT [FK_FormFieldTranslation_Tenant]
GO
ALTER TABLE [Invitation]  WITH CHECK ADD  CONSTRAINT [FK_Invitation_RegisteredUser] FOREIGN KEY([RegisteredUserId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Invitation] CHECK CONSTRAINT [FK_Invitation_RegisteredUser]
GO
ALTER TABLE [Invitation]  WITH CHECK ADD  CONSTRAINT [FK_Invitation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Invitation] CHECK CONSTRAINT [FK_Invitation_Tenant]
GO
ALTER TABLE [Invitation]  WITH CHECK ADD  CONSTRAINT [FK_Invitation_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Invitation] CHECK CONSTRAINT [FK_Invitation_User]
GO
ALTER TABLE [Language]  WITH CHECK ADD  CONSTRAINT [FK_Language_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Language] CHECK CONSTRAINT [FK_Language_Tenant]
GO
ALTER TABLE [Language]  WITH CHECK ADD  CONSTRAINT [FK_Language_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Language] CHECK CONSTRAINT [FK_Language_User]
GO
ALTER TABLE [Language]  WITH CHECK ADD  CONSTRAINT [FK_Language_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Language] CHECK CONSTRAINT [FK_Language_User1]
GO
ALTER TABLE [Lead]  WITH CHECK ADD  CONSTRAINT [FK_Lead_Asset1] FOREIGN KEY([AssetId])
REFERENCES [Asset] ([Id])
GO
ALTER TABLE [Lead] CHECK CONSTRAINT [FK_Lead_Asset1]
GO
ALTER TABLE [Lead]  WITH CHECK ADD  CONSTRAINT [FK_Lead_Tenant1] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Lead] CHECK CONSTRAINT [FK_Lead_Tenant1]
GO
ALTER TABLE [Lead]  WITH CHECK ADD  CONSTRAINT [FK_Lead_User1] FOREIGN KEY([SubscriberId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Lead] CHECK CONSTRAINT [FK_Lead_User1]
GO
ALTER TABLE [Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_User] FOREIGN KEY([SentBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Message] CHECK CONSTRAINT [FK_Message_User]
GO
ALTER TABLE [Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_User1] FOREIGN KEY([RecipientId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Message] CHECK CONSTRAINT [FK_Message_User1]
GO
ALTER TABLE [PartnerType]  WITH CHECK ADD  CONSTRAINT [FK_PartnerType_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [PartnerType] CHECK CONSTRAINT [FK_PartnerType_Tenant]
GO
ALTER TABLE [PartnerType]  WITH CHECK ADD  CONSTRAINT [FK_PartnerType_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [PartnerType] CHECK CONSTRAINT [FK_PartnerType_User]
GO
ALTER TABLE [PartnerType]  WITH CHECK ADD  CONSTRAINT [FK_PartnerType_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [PartnerType] CHECK CONSTRAINT [FK_PartnerType_User1]
GO
ALTER TABLE [PartnerTypeTranslation]  WITH CHECK ADD  CONSTRAINT [FK_PartnerTypeTranslation_PartnerType] FOREIGN KEY([PartnerTypeId])
REFERENCES [PartnerType] ([Id])
GO
ALTER TABLE [PartnerTypeTranslation] CHECK CONSTRAINT [FK_PartnerTypeTranslation_PartnerType]
GO
ALTER TABLE [PartnerTypeTranslation]  WITH CHECK ADD  CONSTRAINT [FK_PartnerTypeTranslation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [PartnerTypeTranslation] CHECK CONSTRAINT [FK_PartnerTypeTranslation_Tenant]
GO
ALTER TABLE [Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_PartnerType] FOREIGN KEY([PartnerTypeId])
REFERENCES [PartnerType] ([Id])
GO
ALTER TABLE [Subscription] CHECK CONSTRAINT [FK_Subscription_PartnerType]
GO
ALTER TABLE [Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Subscription] CHECK CONSTRAINT [FK_Subscription_Subscriber]
GO
ALTER TABLE [Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [Subscription] CHECK CONSTRAINT [FK_Subscription_Tenant]
GO
ALTER TABLE [Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Subscription] CHECK CONSTRAINT [FK_Subscription_User]
GO
ALTER TABLE [Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_User1] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Subscription] CHECK CONSTRAINT [FK_Subscription_User1]
GO
ALTER TABLE [TemplateEmail]  WITH CHECK ADD  CONSTRAINT [FK_TemplateEmail_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [TemplateEmail] CHECK CONSTRAINT [FK_TemplateEmail_User]
GO
ALTER TABLE [TemplateEmail]  WITH CHECK ADD  CONSTRAINT [FK_TemplateEmail_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [TemplateEmail] CHECK CONSTRAINT [FK_TemplateEmail_User1]
GO
ALTER TABLE [Tenant]  WITH CHECK ADD  CONSTRAINT [FK_Tenant_User] FOREIGN KEY([OwnerId])
REFERENCES [User] ([Id])
GO
ALTER TABLE [Tenant] CHECK CONSTRAINT [FK_Tenant_User]
GO
ALTER TABLE [User]  WITH CHECK ADD  CONSTRAINT [FK_User_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [User] CHECK CONSTRAINT [FK_User_Tenant]
GO
ALTER TABLE [UserClaim]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserClaim_dbo.User_UserId] FOREIGN KEY([UserId])
REFERENCES [User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [UserClaim] CHECK CONSTRAINT [FK_dbo.UserClaim_dbo.User_UserId]
GO
ALTER TABLE [UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_Tenant] FOREIGN KEY([TenantId])
REFERENCES [Tenant] ([Id])
GO
ALTER TABLE [UserLocation] CHECK CONSTRAINT [FK_UserLocation_Tenant]
GO
ALTER TABLE [UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_User] FOREIGN KEY([CreatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [UserLocation] CHECK CONSTRAINT [FK_UserLocation_User]
GO
ALTER TABLE [UserLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserLocation_User1] FOREIGN KEY([UpdatedBy])
REFERENCES [User] ([Id])
GO
ALTER TABLE [UserLocation] CHECK CONSTRAINT [FK_UserLocation_User1]
GO
ALTER TABLE [UserLogin]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogin_dbo.User_UserId] FOREIGN KEY([UserId])
REFERENCES [User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [UserLogin] CHECK CONSTRAINT [FK_dbo.UserLogin_dbo.User_UserId]
GO
ALTER TABLE [UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [UserRole] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.Role_RoleId]
GO
ALTER TABLE [UserRole]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.User_UserId] FOREIGN KEY([UserId])
REFERENCES [User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [UserRole] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.User_UserId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ELMAH_GetErrorsXml]
(
    @Application NVARCHAR(60),
    @PageIndex INT = 0,
    @PageSize INT = 15,
    @TotalCount INT OUTPUT
)
AS 

    SET NOCOUNT ON

    DECLARE @FirstTimeUTC DATETIME
    DECLARE @FirstSequence INT
    DECLARE @StartRow INT
    DECLARE @StartRowIndex INT

    SELECT 
        @TotalCount = COUNT(1) 
    FROM 
        [ELMAH_Error]
    WHERE 
        [Application] = @Application

    -- Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1

    IF @StartRowIndex <= @TotalCount
    BEGIN

        SET ROWCOUNT @StartRowIndex

        SELECT  
            @FirstTimeUTC = [TimeUtc],
            @FirstSequence = [Sequence]
        FROM 
            [ELMAH_Error]
        WHERE   
            [Application] = @Application
        ORDER BY 
            [TimeUtc] DESC, 
            [Sequence] DESC

    END
    ELSE
    BEGIN

        SET @PageSize = 0

    END

    -- Now set the row count to the requested page size and get
    -- all records below it for the pertaining application.

    SET ROWCOUNT @PageSize

    SELECT 
        errorId     = [ErrorId], 
        application = [Application],
        host        = [Host], 
        type        = [Type],
        source      = [Source],
        message     = [Message],
        [user]      = [User],
        statusCode  = [StatusCode], 
        time        = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
    FROM 
        [ELMAH_Error] error
    WHERE
        [Application] = @Application
    AND
        [TimeUtc] <= @FirstTimeUTC
    AND 
        [Sequence] <= @FirstSequence
    ORDER BY
        [TimeUtc] DESC, 
        [Sequence] DESC
    FOR
        XML AUTO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [ELMAH_Error]
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [ELMAH_LogError]
(
    @ErrorId UNIQUEIDENTIFIER,
    @Application NVARCHAR(60),
    @Host NVARCHAR(30),
    @Type NVARCHAR(100),
    @Source NVARCHAR(60),
    @Message NVARCHAR(500),
    @User NVARCHAR(50),
    @AllXml NTEXT,
    @StatusCode INT,
    @TimeUtc DATETIME
)
AS

    SET NOCOUNT ON

    INSERT
    INTO
        [ELMAH_Error]
        (
            [ErrorId],
            [Application],
            [Host],
            [Type],
            [Source],
            [Message],
            [User],
            [AllXml],
            [StatusCode],
            [TimeUtc]
        )
    VALUES
        (
            @ErrorId,
            @Application,
            @Host,
            @Type,
            @Source,
            @Message,
            @User,
            @AllXml,
            @StatusCode,
            @TimeUtc
        )


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [LanguageTrigger] ON [Language] AFTER
INSERT, UPDATE AS 

DECLARE @Id int, @IsDefault bit, @TenantId int, @CreatedBy int;

SELECT @Id = Id,
	   @IsDefault = IsDefault,
       @TenantId = TenantId,
	   @CreatedBy = CreatedBy
FROM Inserted;

IF @IsDefault=0 RETURN; 

BEGIN
	
	IF @TenantId=1
	BEGIN
		UPDATE [Language]
		SET IsDefault = 0
		WHERE TenantId = @TenantId AND Id <> @Id AND CreatedBy = @CreatedBy;
	END

	ELSE 
	BEGIN
		UPDATE [Language]
		SET IsDefault = 0
		WHERE TenantId = @TenantId AND Id <> @Id;
	END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE TRIGGER [TemplateEmailTrigger]  
ON [TemplateEmail] AFTER INSERT, UPDATE AS 

DECLARE @Id int, @Name nvarchar(4000), @IsDefault bit, @CreatedBy int;

SELECT @Id = Id,
	   @Name = Name,
	   @IsDefault = Active,
	   @CreatedBy = CreatedBy
FROM Inserted;

IF @IsDefault=0 RETURN; 

BEGIN
	
	BEGIN
		UPDATE [TemplateEmail]
		SET Active = 0
		WHERE Id <> @Id AND Name = @Name AND CreatedBy = @CreatedBy;
	END
END
GO
