﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Repositories
{
    public interface IRepository<T> : IReadOnlyRepository<T> where T : class
    {
        void Insert(T entity);
        void Insert(IEnumerable<T> entities);
        void Update(T entity);
        void Update(IEnumerable<T> entities);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        void DeleteByUniqueKey(Func<T, bool> predicate);
    }
}