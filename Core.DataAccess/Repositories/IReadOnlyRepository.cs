﻿using Core.DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.DataAccess.Repositories
{
    public interface IReadOnlyRepository<T> where T : class
    {
        /// <summary>
        /// Get entity domain by primary keys
        /// </summary>
        /// <param name="keyValues">Key values by database order</param>
        /// <returns>Entity</returns>
        T Get(params object[] keyValues);

        /// <summary>
        /// Get entity domain by primary keys
        /// </summary>
        /// <param name="keyValues">Key values by database order</param>
        /// <returns>Entity</returns>
        Task<T> GetAsync(params object[] keyValues);

        IList<T> GetAll();
        Task<IList<T>> GetAllAsync();

        IQueryable<T> Queryable();
        IQueryable<T> AsNoFilter();

        T Single(Expression<Func<T, bool>> predicate);
        Task<T> SingleAsync(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate);

        int Count();
        Task<int> CountAsync();
        int Count(Expression<Func<T, bool>> predicate);
        Task<int> CountAsync(Expression<Func<T, bool>> predicate);

        bool Any(Expression<Func<T, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);
    }
}