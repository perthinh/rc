using Core.DataAccess.Context;
using Core.DataAccess.Infrastructure;
using Core.DataAccess.Uow;
using CuttingEdge.Conditions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Core.DataAccess.Repositories
{
    public class ReadOnlyRepository<T> : IReadOnlyRepository<T> where T : class
    {
        protected readonly IUnitOfWork TheUnitOfWork;
        protected IDataContext DbContext { get; }
        protected virtual DbSet<T> DbSet { get; }

        protected ReadOnlyRepository(IUnitOfWork unitOfWork)
        {
            Condition.Requires(unitOfWork, nameof(unitOfWork)).IsNotNull();
            Condition.Requires(unitOfWork.DataContext, nameof(unitOfWork.DataContext)).IsNotNull();

            TheUnitOfWork = unitOfWork;
            DbContext = TheUnitOfWork.DataContext;
            DbSet = DbContext.Set<T>();
        }

        public virtual T Get(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public virtual async Task<T> GetAsync(params object[] keyValues)
        {
            return await DbSet.FindAsync(keyValues);
        }

        public virtual IList<T> GetAll()
        {
            return Queryable().ToList();
        }

        public virtual Task<IList<T>> GetAllAsync()
        {
            return Task.FromResult(GetAll());
        }

        public virtual IQueryable<T> Queryable()
        {
            return DbSet;
        }

        public IQueryable<T> AsNoFilter()
        {
            return DbSet.AsNoFilter();
        }

        public virtual T Single(Expression<Func<T, bool>> predicate)
        {
            return Queryable().Single(predicate);
        }

        public virtual Task<T> SingleAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Single(predicate));
        }

        public virtual T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return Queryable().FirstOrDefault(predicate);
        }

        public virtual Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(FirstOrDefault(predicate));
        }

        public virtual int Count()
        {
            return Queryable().Count();
        }

        public virtual Task<int> CountAsync()
        {
            return Task.FromResult(Count());
        }

        public virtual int Count(Expression<Func<T, bool>> predicate)
        {
            return Queryable().Where(predicate).Count();
        }

        public virtual Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Count(predicate));
        }

        public virtual bool Any(Expression<Func<T, bool>> predicate)
        {
            return Queryable().Any(predicate);
        }

        public virtual Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return Task.FromResult(Any(predicate));
        }
    }
}