using System;
using Core.DataAccess.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Core.DataAccess.Entities;
using Core.DataAccess.Infrastructure;
using Core.Exceptions;
using Core.Extensions;
using Core.Runtime.Session;
using Core.Timing;

namespace Core.DataAccess.Context
{
    public abstract class DataContext<TUser, TRole> : IdentityDbContext<TUser, TRole, int, AspNetUserLogin, AspNetUserRole, AspNetUserClaim>, IDataContext
        where TUser : AspNetUser
        where TRole : AspNetRole
    {
        protected DataContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AspNetUserRole>().ToTable("UserRole");
            modelBuilder.Entity<AspNetUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<AspNetUserClaim>().ToTable("UserClaim");
        }

        public override int SaveChanges()
        {
            PreCommit();
            var result = base.SaveChanges();
            PostCommit();
            return result;
        }

        public override async Task<int> SaveChangesAsync()
        {
            PreCommit();
            var result = await base.SaveChangesAsync();
            PostCommit();
            return result;
        }

        private void PostCommit()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                var entity = entry.Entity as IObjectState;
                if (entity == null) continue;
                entity.ObjectState = StateConverter.ToObjectState(entry.State);
            }
        }

        protected void PreCommit()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                SyncEntityState(entry);
                AuditEntity(entry);
            }
        }

        private void AuditEntity(DbEntityEntry entry)
        {
            if (entry.State == EntityState.Deleted) return;
            switch (entry.State)
            {
                case EntityState.Added:
                    SetCreationProperties(entry);
                    SetMustHaveTenantProperties(entry);
                    break;
                case EntityState.Modified:
                    SetUpdatedProperties(entry);
                    SetMustHaveTenantProperties(entry);
                    break;
            }
        }

        private static void SetCreationProperties(DbEntityEntry entry)
        {
            if (entry.Entity is IHasCreationTime)
            {
                entry.Entity.Cast<IHasCreationTime>().CreatedDate = Clock.Now;
            }

            if (entry.Entity is ICreationAudited)
            {
                var entity = entry.Entity as ICreationAudited;
                entity.CreatedDate = Clock.Now;
                SetAuditedUserProperties(entity, x => x.CreatedBy);
            }

            if (entry.Entity is IAuditedEntity)
            {
                var entity = entry.Entity as IAuditedEntity;
                entity.UpdatedDate = entity.CreatedDate;
                SetAuditedUserProperties(entity, x => x.UpdatedBy, entity.CreatedBy);
            }
        }

        private void SetUpdatedProperties(DbEntityEntry entry)
        {
            if (entry.Entity is IHasAuditedTime)
            {
                var entity = entry.Entity as IHasAuditedTime;
                entity.UpdatedDate = Clock.Now;
                Entry(entity).Property(x => x.CreatedDate).IsModified = false;
            }

            if (entry.Entity is IAuditedEntity)
            {
                var entity = entry.Entity as IAuditedEntity;
                entity.UpdatedDate = Clock.Now;
                SetAuditedUserProperties(entity, x => x.UpdatedBy);

                Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                Entry(entity).Property(x => x.CreatedBy).IsModified = false;
            }
        }

        private void SetMustHaveTenantProperties(DbEntityEntry entry)
        {
            var entity = entry.Entity as IMustHaveTenant;
            if (entity?.TenantId != 0) return;

            var tenantId = UserSessions.TenantId;
            if (!tenantId.HasValue)
            {
                throw new SysException("Can not set TenantId to 0 for IMustHaveTenant entities!");
            }

            entity.TenantId = tenantId.Value;
        }

        private static void SyncEntityState(DbEntityEntry entry)
        {
            var entity = entry.Entity as IObjectState;
            if (entity == null) return;
            entry.State = StateConverter.ToEntityState(entity.ObjectState, entry.State);
        }

        private static void SetAuditedUserProperties<T>(T target, Expression<Func<T, int>> memberLamda, int? value = null)
        {
            var expr = memberLamda.Body as MemberExpression;
            if (expr == null) return;
            var property = expr.Member as PropertyInfo;
            if (property == null) return;

            if (!property.GetValue(target).Equals(0)) return;

            if (!value.HasValue)
            {
                value = UserSessions.UserId.GetValueOrDefault();
            }
            property.SetValue(target, value, null);
        }
    }
}