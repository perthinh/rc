using Core.DataAccess.Context;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Core.DataAccess.Identity
{
    public abstract class AspNetRoleStore<TRole> : RoleStore<TRole, int, AspNetUserRole>
        where TRole : AspNetRole, new()
    {
        protected AspNetRoleStore(IDataContext context)
            : base((DbContext)context)
        {
        }
    }
}