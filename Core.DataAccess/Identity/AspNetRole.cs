﻿using Core.DataAccess.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.DataAccess.Identity
{
    public abstract class AspNetRole : IdentityRole<int, AspNetUserRole>, IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}