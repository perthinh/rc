using Core.DataAccess.Context;
using Core.DataAccess.Identity;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.AspNetIdentity
{
    public class RoleStore : AspNetRoleStore<Role>
    {
        public RoleStore(IDataContext context)
            : base(context)
        {
        }
    }
}