﻿using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.AspNetIdentity
{
    public class UserManager : UserManager<User, int>
    {
        public UserManager(IUserStore<User, int> store)
            : base(store)
        {
        }
    }
}