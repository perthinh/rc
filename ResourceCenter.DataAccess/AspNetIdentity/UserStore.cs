using Core.DataAccess.Context;
using Core.DataAccess.Identity;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.AspNetIdentity
{
    public class UserStore : AspNetUserStore<User, Role>
    {
        public UserStore(IDataContext context)
            : base(context)
        {
        }
    }
}