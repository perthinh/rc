﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.DataAccess.AspNetIdentity.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<IdentityResult> AddToRoleAsync(this UserManager userManager, User user, ERole role)
        {
            return await userManager.AddToRoleAsync(user.Id, role.ToString());
        }
    }
}
