﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.AspNetIdentity
{
    public class RoleManager : RoleManager<Role, int>
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }

        public async Task CreateIfNotExists(string roleName)
        {
            if (!this.RoleExists(roleName))
            {
                await CreateAsync(new Role
                {
                    Name = roleName
                });
            }
        }
    }
}