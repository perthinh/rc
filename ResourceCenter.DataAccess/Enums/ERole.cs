﻿
namespace ResourceCenter.DataAccess.Enums
{
    public enum ERole
    {
        Host,
        Distributor,
        Reseller,
        User
    }
}