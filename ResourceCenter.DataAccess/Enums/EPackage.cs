﻿namespace ResourceCenter.DataAccess.Enums
{
    public enum EPackage
    {
        None, Free, Personal, Business
    }
}
