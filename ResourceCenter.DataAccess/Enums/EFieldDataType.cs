﻿namespace ResourceCenter.DataAccess.Enums
{
    public enum EFieldDataType
    {
        Text,
        MultilineText,
        DropDownList,
        CheckBox
    }
}
