﻿namespace ResourceCenter.DataAccess.Enums
{
    public enum ESubscriptionStatus
    {
        Pending,
        Active,
        Suspended,
        Cancelled
    }
}
