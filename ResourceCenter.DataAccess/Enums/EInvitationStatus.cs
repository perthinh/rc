﻿namespace ResourceCenter.DataAccess.Enums
{
    public enum EInvitationStatus
    {
        Pending,
        Accepted
    }
}