﻿using System.Threading.Tasks;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class FieldTranslationRepository : Repository<FormFieldTranslation>, ITranslationRepository
    {
        public FieldTranslationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Task<int> CountTranslations(int parentId)
        {
            return CountAsync(x => x.FormFieldId == parentId);
        }
    }
}
