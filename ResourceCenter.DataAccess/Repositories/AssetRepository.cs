﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class AssetRepository : Repository<Asset>
    {
        public AssetRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
