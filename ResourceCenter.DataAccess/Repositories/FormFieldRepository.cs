﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class FormFieldRepository : Repository<FormField>
    {
        public FormFieldRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
