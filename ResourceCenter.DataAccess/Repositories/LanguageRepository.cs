﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class LanguageRepository : Repository<Language>
    {
        public LanguageRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
