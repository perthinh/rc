﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using Core.Infrastructure.Objects;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class PartnerTypeRepository : Repository<PartnerType>
    {
        public PartnerTypeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Task<List<PairInt>> GetByTenantId(int tenantId)
        {
            return AsNoFilter()
                   .Where(x => x.TenantId == tenantId)
                   .Select(x => new PairInt
                   {
                       Value = x.Id,
                       Text = x.Name
                   })
                   .OrderBy(x => x.Text)
                   .ToListAsync();
        }

        public IList<PairInt> GetByUserId(int userId)
        {
            return AsNoFilter()
                .Where(x => x.CreatedBy == userId)
                .Select(x => new PairInt
                {
                    Value = x.Id,
                    Text = x.Name
                })
                .OrderBy(x => x.Text)
                .ToList();
        }
    }
}
