﻿using System.Threading.Tasks;

namespace ResourceCenter.DataAccess.Repositories
{
    public interface ITranslationRepository
    {
        Task<int> CountTranslations(int parentId);
    }
}
