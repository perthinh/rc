﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class AssetDownloadRepository : Repository<AssetDownload>
    {
        public AssetDownloadRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

    }
}
