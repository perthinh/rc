﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class InvitationRepository : Repository<Invitation>
    {
        public InvitationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
