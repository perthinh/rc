﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class SubscriptionRepository : Repository<Subscription>
    {
        public SubscriptionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
