﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using Core.Infrastructure.Objects;
using ResourceCenter.DataAccess.Entities;
using Z.EntityFramework.Plus;

namespace ResourceCenter.DataAccess.Repositories
{
    public class TenantRepository : Repository<Tenant>
    {
        public TenantRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Tenant GetDefault()
        {
            return AsNoFilter().SingleOrDefault(x => x.Id == Tenant.DefaultId);
        }

        public async Task UpdateOwner(int tenantId, int ownerId)
        {
            await AsNoFilter()
                .Where(x => x.Id == tenantId)
                .UpdateAsync(x => new Tenant
                {
                    OwnerId = ownerId
                });
        }

        public IList<PairInt> GetDistributors()
        {
            return AsNoFilter().Where(x => x.Id != Tenant.DefaultId)
                .OrderBy(x => x.Name)
                .Select(x => new PairInt
                {
                    Value = x.Id,
                    Text = x.Name
                })
                .ToList();
        }

        public Task<string> GetTenantEmail(int tenantId)
        {
            return AsNoFilter()
                 .Where(x => x.Id == tenantId)
                 .Select(x => x.Owner.Email)
                 .SingleOrDefaultAsync();
        }

    }
}
