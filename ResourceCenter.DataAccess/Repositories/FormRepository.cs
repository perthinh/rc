﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class FormRepository : Repository<Form>
    {
        public FormRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
