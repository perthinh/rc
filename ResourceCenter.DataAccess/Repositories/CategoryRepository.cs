﻿using System.Collections.Generic;
using System.Linq;
using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;
using Z.EntityFramework.Plus;

namespace ResourceCenter.DataAccess.Repositories
{
    public class CategoryRepository : Repository<Category>
    {
        public CategoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IList<Category> GetTenantCategories(int tenantId, string languageCode)
        {
            return DbSet.AsNoFilter()
                .IncludeFilter(x => x.Translations.Where(a => a.LanguageCode == languageCode))
                .Where(x => x.TenantId == tenantId)
                .ToList();
        }
    }
}
