﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class CompanyAttributeValueRepository : Repository<CompanyAttributeValue>
    {
        public CompanyAttributeValueRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

    }
}