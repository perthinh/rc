﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class RoleRepository : Repository<Role>
    {
        public RoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}