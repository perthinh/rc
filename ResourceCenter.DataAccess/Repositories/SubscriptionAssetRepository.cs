﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class SubscriptionAssetRepository : ReadOnlyRepository<SubscriptionAsset>
    {
        public SubscriptionAssetRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
