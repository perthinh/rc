﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class TemplateEmailRepository : Repository<TemplateEmail>
    {
        public TemplateEmailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
