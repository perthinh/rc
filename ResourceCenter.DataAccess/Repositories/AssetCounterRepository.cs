﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class AssetCounterRepository : Repository<AssetCounter>
    {
        public AssetCounterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
