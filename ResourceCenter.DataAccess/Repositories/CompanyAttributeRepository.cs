﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class CompanyAttributeRepository : Repository<CompanyAttribute>
    {
        public CompanyAttributeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
