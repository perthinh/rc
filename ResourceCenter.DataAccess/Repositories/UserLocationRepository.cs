﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using ResourceCenter.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Repositories
{
    public class UserLocationRepository : Repository<UserLocation>
    {
        public UserLocationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}