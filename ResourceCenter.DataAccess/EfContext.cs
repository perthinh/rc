﻿using Core.DataAccess.Context;
using ResourceCenter.DataAccess.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Core.DataAccess.Entities;
using Core.Runtime.Session;
using Z.EntityFramework.Plus;

namespace ResourceCenter.DataAccess
{
    public class EfContext : DataContext<User, Role>
    {
        static EfContext()
        {
            Database.SetInitializer<EfContext>(null);

        }

        public EfContext()
            : this("DefaultConnection")
        {
        }

        public EfContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.Filter<ISoftDelete>(q => q.Where(x => !x.IsDeleted));
            this.Filter<IMustHaveTenant>(q => q.Where(x => x.TenantId == UserSessions.TenantId));
        }

        public IDbSet<Category> Categories { get; set; }
        public IDbSet<CategoryTranslation> CategoryTranslations { get; set; }

        public IDbSet<Asset> Assets { get; set; }
        public IDbSet<AssetTranslation> AssetTranslations { get; set; }
        public IDbSet<AssetCounter> AssetCounters { get; set; }
        public IDbSet<AssetDownload> AssetDownloads { get; set; }

        public IDbSet<Language> Languages { get; set; }
        public IDbSet<PartnerType> PartnerTypes { get; set; }
        public IDbSet<Subscription> Subscriptions { get; set; }
        public IDbSet<SubscriptionAsset> SubscriptionAssets { get; set; }

        public IDbSet<Form> Forms { get; set; }
        public IDbSet<FormField> FormFields { get; set; }
        public IDbSet<FormFieldTranslation> FormFieldTranslations { get; set; }

        public IDbSet<Company> Companies { get; set; }
        public IDbSet<CompanyAttribute> CompanyAttributes { get; set; }
        public IDbSet<CompanyAttributeValue> CompanyAttributeValues { get; set; }

        public IDbSet<Invitation> Invitations { get; set; }

        public IDbSet<TemplateEmail> TemplateEmails { get; set; }
        public IDbSet<UserLocation> UserLocations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Role>().ToTable("Role");

            ModelBuilders.Build(modelBuilder.Entity<User>());
            ModelBuilders.Build(modelBuilder.Entity<Asset>());
            ModelBuilders.Build(modelBuilder.Entity<AssetCounter>());
            ModelBuilders.Build(modelBuilder.Entity<Company>());
        }

        internal static class ModelBuilders
        {
            public static void Build(EntityTypeConfiguration<Company> modelBuilder)
            {
                modelBuilder.Property(x => x.Id).HasColumnName("UserId").IsRequired();
                modelBuilder.HasRequired(a => a.Owner).WithOptional(b => b.Company);
            }

            public static void Build(EntityTypeConfiguration<AssetCounter> modelBuilder)
            {
                modelBuilder.Property(x => x.Id).HasColumnName("AssetId").IsRequired();
                modelBuilder.HasRequired(a => a.Asset).WithOptional(b => b.AssetCounter);
            }

            public static void Build(EntityTypeConfiguration<Asset> modelBuilder)
            {
                modelBuilder
                   .HasMany(t => t.PartnerTypes).WithMany(t => t.Assets).Map(m =>
                   {
                       m.ToTable("AssetPartnerType");
                       m.MapLeftKey("AssetId");
                       m.MapRightKey("PartnerTypeId");
                   });
            }

            public static void Build(EntityTypeConfiguration<User> modelBuilder)
            {
                modelBuilder.ToTable("User")
                   .HasRequired(a => a.Tenant).WithMany(b => b.Users).HasForeignKey(c => c.TenantId);

            }
        }
    }
}