﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.DataAccess.Entities
{
    public class Invitation : CreationAudited<int>, IMustHaveTenant
    {
        public int? RegisteredUserId { get; set; }
        public string EmailTo { get; set; }
        public Guid InvitationCode { get; set; }
        public EInvitationStatus Status { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int TenantId { get; set; }

        public virtual Tenant Tenant { get; set; }

        [ForeignKey("RegisteredUserId")]
        public virtual User RegisteredUser { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }
    }
}
