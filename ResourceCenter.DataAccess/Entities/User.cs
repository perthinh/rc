﻿using Core.DataAccess.Identity;
using Microsoft.AspNet.Identity;
using ResourceCenter.DataAccess.AspNetIdentity;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Runtime.Security;
using Core.Timing;

namespace ResourceCenter.DataAccess.Entities
{
    public class User : AspNetUser
    {
        public bool IsLocked => LockoutEnabled && LockoutEndDateUtc.HasValue && LockoutEndDateUtc.Value.Date > Clock.Now.Date;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string JobTitle { get; set; }
        public string Department { get; set; }
        public string Notes { get; set; }

        public int TenantId { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual Company Company { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager manager, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim(CustomClaimTypes.TenantId, TenantId.ToString()));

            return userIdentity;
        }

        public void SetLockState(bool isLocked)
        {
            if (isLocked)
            {
                LockoutEnabled = true;
                LockoutEndDateUtc = Clock.Now.AddYears(200);
            }
            else
            {
                LockoutEndDateUtc = null;
            }
        }
    }
}