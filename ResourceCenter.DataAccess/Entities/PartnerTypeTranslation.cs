﻿using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class PartnerTypeTranslation : Entity<int>
    {
        public int PartnerTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
        public int TenantId { get; set; }

        public virtual PartnerType PartnerType { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
