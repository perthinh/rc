﻿using Core.DataAccess.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResourceCenter.DataAccess.Entities
{
    public class Asset : AuditedEntity<int>, IMustHaveTenant
    {
        public string Alias { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Thumbnail { get; set; }
        public bool Published { get; set; }
        public bool IsGated { get; set; }
        public bool IsCobranded { get; set; }
        public int TenantId { get; set; }
        public int CategoryId { get; set; }
        public int? FormId { get; set; }
        public virtual Category Category { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual AssetCounter AssetCounter { get; set; }
        public virtual Form Form { get; set; }

        public virtual IList<AssetTranslation> Translations { get; set; }
        public virtual IList<PartnerType> PartnerTypes { get; set; }

        public Asset()
        {
            Translations = new List<AssetTranslation>();
            PartnerTypes = new List<PartnerType>();
        }
    }
}
