﻿using Core.DataAccess.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResourceCenter.DataAccess.Entities
{
    public class Language : AuditedEntity<int>, IMustHaveTenant
    {
        public string LanguageCode { get; set; }
        public bool IsDefault { get; set; }
        public int TenantId { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
