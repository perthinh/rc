﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class UserLocation : AuditedEntity<int>, IMustHaveTenant
    {
        public DbGeography Location { get; set; }
        public string Description { get; set; }
        public int TenantId { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        public virtual Tenant Tenant { get; set; }
    }
}
