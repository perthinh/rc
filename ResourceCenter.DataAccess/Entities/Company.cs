﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class Company : AuditedEntity<int>, IMustHaveTenant
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string WebsiteUrl { get; set; }
        public string ExternalCode { get; set; }
        public int TenantId { get; set; }

        public virtual Tenant Tenant { get; set; }
        public virtual User Owner { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }
    }
}
