using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class FormFieldTranslation : Entity<int>, IMustHaveTenant
    {
        public int FormFieldId { get; set; }
        public string LanguageCode { get; set; }
        public string Label { get; set; }
        public string DataSource { get; set; }
        public string DefaultValue { get; set; }
        public int TenantId { get; set; }

        public virtual FormField FormField { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}