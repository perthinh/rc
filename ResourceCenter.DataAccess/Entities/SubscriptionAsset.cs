﻿using System;
using System.Collections.Generic;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class SubscriptionAsset : Entity<int>
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Thumbnail { get; set; }
        public bool IsGated { get; set; }
        public bool IsCobranded { get; set; }
        public int TenantId { get; set; }
        public int? FormId { get; set; }
        public int CategoryId { get; set; }
        public int? PartnerTypeId { get; set; }
        public int SubscriberId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public virtual IList<AssetTranslation> Translations { get; set; }

        public SubscriptionAsset()
        {
            Translations = new List<AssetTranslation>();
        }
    }
}
