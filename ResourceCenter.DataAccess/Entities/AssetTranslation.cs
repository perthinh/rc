﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class AssetTranslation : Entity<int>, IMustHaveTenant
    {
        public int AssetId { get; set; }
        public string AssetTitle { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string LanguageCode { get; set; }
        public int TenantId { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual Tenant Tenant { get; set; }

        [ForeignKey("AssetId")]
        public virtual SubscriptionAsset SubscriptionAsset { get; set; }
    }
}