﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.DataAccess.Entities
{
    public class Subscription : AuditedEntity<int>
    {
        public int TenantId { get; set; }
        public int SubscriberId { get; set; }
        public int? PartnerTypeId { get; set; }
        public ESubscriptionStatus Status { get; set; }

        public virtual PartnerType PartnerType { get; set; }
        public virtual Tenant Tenant { get; set; }

        [ForeignKey("SubscriberId")]
        public virtual User Subscriber { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }
    }
}
