﻿using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class AssetCounter : Entity<int>
    {
        public int ViewCount { get; set; }
        public int DownloadCount { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
