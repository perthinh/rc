﻿using Core.DataAccess.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResourceCenter.DataAccess.Entities
{
    public class Category : AuditedEntity<int>, IMustHaveTenant
    {
        public string Alias { get; set; }
        public int SortOrder { get; set; }
        public int? ParentId { get; set; }
        public int TenantId { get; set; }

        public virtual Tenant Tenant { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        [ForeignKey("ParentId")]
        public virtual Category Parent { get; set; }

        public virtual IList<CategoryTranslation> Translations { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<Category> Children { get; set; }

        public Category()
        {
            Assets = new List<Asset>();
            Children = new List<Category>();
            Translations = new List<CategoryTranslation>();
        }

    }
}
