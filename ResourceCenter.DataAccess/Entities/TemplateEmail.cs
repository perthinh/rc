﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class TemplateEmail : AuditedEntity<int>
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public bool Active { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }
    }
}
