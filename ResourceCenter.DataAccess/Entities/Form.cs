﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class Form : AuditedEntity<int>, IMustHaveTenant
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int TenantId { get; set; }
   
        public virtual Tenant Tenant { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        public virtual IList<Asset> Assets { get; set; }
        public virtual IList<FormField> FormFields { get; set; }

        public Form()
        {
            Assets = new List<Asset>();
            FormFields = new List<FormField>();
        }
    }
}
