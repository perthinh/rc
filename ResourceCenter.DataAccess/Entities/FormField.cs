﻿using System.Collections.Generic;
using Core.DataAccess.Entities;
using ResourceCenter.DataAccess.Enums;

namespace ResourceCenter.DataAccess.Entities
{
    public class FormField : Entity<int>, IMustHaveTenant
    {
        public int FormId { get; set; }
        public string FieldName { get; set; }
        public EFieldDataType DataType { get; set; }
        public bool IsRequired { get; set; }
        public int SortOrder { get; set; }
        public int TenantId { get; set; }

        public virtual Form Form { get; set; }
        public virtual Tenant Tenant { get; set; }
        public virtual IList<FormFieldTranslation> Translations { get; set; }

        public FormField()
        {
            Translations = new List<FormFieldTranslation>();
        }
    }
}