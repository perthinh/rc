﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class CompanyAttribute : AuditedEntity<int>, IMustHaveTenant
    {
        public string AttributeName { get; set; }
        public bool IsMandatory { get; set; }
        public int SortOrder { get; set; }
        public int TenantId { get; set; }

        public virtual Tenant Tenant { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }
        public virtual IList<CompanyAttributeValue> AttributeValues { get; set; }

        public CompanyAttribute()
        {
            AttributeValues = new List<CompanyAttributeValue>();
        }
    }
}
