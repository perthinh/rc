﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class CompanyAttributeValue : AuditedEntity<int>
    {
        public int AttributeId { get; set; }
        public string Value { get; set; }

        [ForeignKey("AttributeId")]
        public virtual CompanyAttribute CompanyAttribute { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }
    }
}