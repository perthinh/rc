﻿using System.Collections.Generic;
using Core.DataAccess.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResourceCenter.DataAccess.Entities
{
    public class PartnerType : AuditedEntity<int>, IMustHaveTenant
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int TenantId { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedUser { get; set; }

        [ForeignKey("UpdatedBy")]
        public virtual User UpdatedUser { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual IList<Asset> Assets { get; set; }
        public virtual IList<PartnerTypeTranslation> Translations { get; set; }
        public virtual IList<Subscription> Subscriptions { get; set; }
        public PartnerType()
        {
            Assets = new List<Asset>();
            Translations = new List<PartnerTypeTranslation>();
            Subscriptions = new List<Subscription>();
        }
    }
}
