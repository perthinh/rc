﻿using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class CategoryTranslation : Entity<int>, IMustHaveTenant
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LanguageCode { get; set; }
        public int TenantId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}