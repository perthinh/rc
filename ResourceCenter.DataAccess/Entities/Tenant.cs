﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;
using Core.Timing;

namespace ResourceCenter.DataAccess.Entities
{
    public class Tenant : Entity<int>
    {
        public static readonly int DefaultId = 1;

        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public virtual User Owner { get; set; }

        public virtual IList<User> Users { get; set; }
        public virtual IList<Subscription> Subscriptions { get; set; }

        public Tenant()
        {
            Users = new List<User>();
            Subscriptions = new List<Subscription>();
            CreatedDate = Clock.Now;
        }
    }
}
