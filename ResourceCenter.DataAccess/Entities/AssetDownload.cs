﻿using System.ComponentModel.DataAnnotations.Schema;
using Core.DataAccess.Entities;

namespace ResourceCenter.DataAccess.Entities
{
    public class AssetDownload : CreationTimeEntity<int>, IMustHaveTenant
    {
        public int? AssetId { get; set; }
        public int SubscriberId { get; set; }
        public int TenantId { get; set; }
        public string LeadContent { get; set; }
        public string IPAddress { get; set; }
        public bool IsLead { get; set; }

        public virtual Asset Asset { get; set; }

        [ForeignKey("SubscriberId")]
        public virtual User Subscriber { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
